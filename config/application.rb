require File.expand_path("../boot.rb", __FILE__)

require 'yaml'
require 'sequel'
require 'mail'
require 'securerandom'

module EARMMS
  @@root = File.expand_path(File.join('..', '..'), __FILE__)
  require File.join(@@root, 'app', 'version')

  class << self
    attr_reader :controllers
    attr_accessor :database

    attr_accessor :app_config, :app_config_refresh_pending
    attr_accessor :site_dir, :theme_dir
    attr_accessor :email_templates

    attr_accessor :default_language
    attr_reader :languages
  end

  def self.root
    @@root
  end

  def self.filter_strip_chars
    [
      " ",
      "\t",
      "\n",
      "(",
      ")",
      "[",
      "]",
      "{",
      "}",
      '"',
      "'",
      "#",
      /[\u0080-\u00ff]/, # all non-ASCII characters
    ]
  end

  def self.initialize(opts = {})
    # Check whether we can reach keyderiv before allowing the app to initialize
    require File.join(EARMMS.root, 'app', 'crypto')
    unless opts[:no_check_keyderiv]
      begin
        EARMMS::Crypto.get_index_key "test", "test"
      rescue => e
        raise "Couldn't reach keyderiv, dying (inner: #{e.inspect})"
      end
    end

    require File.join(EARMMS.root, 'app', 'config')
    require File.join(EARMMS.root, 'app', 'server_utils')
    require File.join(EARMMS.root, 'app', 'email_templates')
    require File.join(EARMMS.root, 'app', 'helpers')
    require File.join(EARMMS.root, 'app', 'controllers')
    require File.join(EARMMS.root, 'app', 'workers')

    @site_dir = nil
    @app_config_refresh_pending = []
    @app_config = {}
    @email_templates = EARMMS::EmailTemplates.new

    @controllers = YAML.load_file(File.expand_path('../controllers.yml', __FILE__)).map do |c|
      app_env = (ENV["RACK_ENV"] ||= "production").to_sym

      # allow exceptions to controller loading
      if c.key?("only")
        allowed = []

        if c["only"].key?("env")
          [c["only"]["env"]].flatten.each do |okenv|
            ok = (app_env.to_s == okenv.to_s)
            allowed << ok
          end
        end

        next unless allowed.all?
      end

      klass = self.const_get(c["controller"])
      klass.class_eval do
        configure do
          set :environment, app_env

          # The session secret used to be set by individual environment configs
          # using EARMMS.configure, but using the SESSION_SECRET environment
          # variable is standard enough that it can just be set here.
          set :session_secret, ENV.fetch("SESSION_SECRET") {SecureRandom.hex(32)}
        end
      end

      {
        :path => c["path"],
        :controller => klass,
      }
    end.compact

    @database = Sequel.connect(ENV["DATABASE_URL"])
    @database.extension(:pagination)
    self.load_models unless opts[:no_load_models]
    self.load_configs unless opts[:no_load_configs]

    @default_language = 'en'
    self.load_languages
  end

  def self.load_languages
    @languages = Dir.glob(File.join(EARMMS.root, 'config', 'translations', '*.yml')).map do |e|
      name = /(\w+)\.yml$/.match(e)[1]
      strings = YAML.load_file(e)

      [name, strings]
    end.to_h

    # Allow themes to override translation keys
    if @theme_dir
      @languages.keys.each do |tlname|
        override_file = File.join(@theme_dir, 'translations', "#{tlname}.yml")
        if File.exists?(override_file)
          strings = YAML.load_file(override_file)
          next unless strings

          strings.each do |key, value|
            @languages[tlname][key] = value
          end
        end
      end
    end

    # Filter out languages that have their `:meta_description` set to nil or
    # an empty string
    @languages.reject! do |name, strings|
      strings[:meta_description].nil? || strings[:meta_description]&.empty?
    end

    # In development mode, add a "language" that has no translated text, which
    # when t() is called, will display the translation key rather than any text
    if ENV['RACK_ENV'] == 'development'
      @languages["translationkeys"] = {
        :meta_description => "DEBUG: Translation keys"
      }
    end
  end

  def self.load_models
    require File.join(EARMMS.root, 'app', 'models')
  end

  def self.load_configs
    require File.join(File.expand_path('..', __FILE__), 'default_config.rb')
    require File.join(File.expand_path('..', __FILE__), 'environments', "#{ENV["RACK_ENV"]}.rb")

    @site_dir = ENV["SITE_DIR"]
    @site_dir = nil if @site_dir&.strip&.empty?
    unless @site_dir.nil?
      cfgpath = File.join(@site_dir, 'config.rb')
      if File.exist?(cfgpath)
        require cfgpath
      end
    end

    # Allow forcing no theme by environment variable
    @theme_dir = nil if ENV['EARMMS_FORCE_THEME_OFF']

    self.app_config_refresh(:force => true)
  end

  def self.app_config_refresh(opts = {})
    output = []

    EARMMS::APP_CONFIG_ENTRIES.each do |key, desc|
      if opts[:only_keys]
        next unless opts[:only_keys].include?(key)
      end

      cfg = EARMMS::ConfigModel.where(:key => key).first
      if cfg
        value = cfg.value
      else
        value = desc[:default]
        if desc[:type] == :bool
          value = (value ? 'yes' : 'no')
        end

        value = value.to_s
      end

      parsed = value
      warnings = []
      stop = false
      EARMMS::Config.parsers.each do |parser|
        next if stop

        if parser.accept?(key, desc[:type])
          out = parser.parse(parsed)
          warnings << out[:warning] if out[:warning]
          parsed = out[:data]

          if !opts[:dry]
            parser.process(parsed) if parser.respond_to?(:process)
          end

          stop = out[:stop_processing_here]
        end
      end

      if !opts[:dry]
        @app_config[key] = parsed
      end

      output << {:key => key, :warnings => warnings}
    end

    @app_config_refresh_pending.clear if !opts[:dry]
    output
  end

  def self.email_subject(text)
    out = ""
    case EARMMS.app_config['email-subject-prefix']
    when "none"
      out = text
    when "org-name"
      out = "#{EARMMS.app_config['org-name']}: #{text}"
    when "org-name-brackets"
      out = "[#{EARMMS.app_config['org-name']}] #{text}"
    when "site-name"
      out = "#{EARMMS.app_config['site-name']}: #{text}"
    else # site-name-brackets
      out = "[#{EARMMS.app_config['site-name']}] #{text}"
    end

    return out
  end
end
