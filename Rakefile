require File.expand_path("../config/application.rb", __FILE__)
EARMMS.initialize :no_load_models => true, :no_load_configs => true, :no_check_keyderiv => true

def do_setup
  EARMMS.load_models
  EARMMS.load_configs
end

desc "Run scheduled tasks"
task :work_queue do |t|
  do_setup
  EARMMS::Workers.perform_queue
end

namespace :site do
  desc "Toggle maintenance mode"
  task :maintenance do
    do_setup

    c = EARMMS::ConfigModel.where(key: 'maintenance').find_or_create do |c|
      c.type = 'bool'
      c.value = 'no'
    end

    c.value = (c.value == 'yes' ? 'no' : 'yes')
    c.save

    puts "Maintenance enabled: #{c.value}"
  end

  desc "Generate an invite code with system privileges"
  task :invite_admin do |t|
    do_setup

    roles = ["system:*"]
    invite = EARMMS::Token.generate_invite(:roles => roles)
    invite.save

    puts "Invite code: #{invite.token}"
  end
end

namespace :db do
  desc "Run database migrations"
  task :migrate, [:version] do |t, args|
    Sequel.extension(:migration)

    migration_dir = File.expand_path("../migrations", __FILE__)
    version = nil
    version = args[:version].to_i if args[:version]

    Sequel::Migrator.run(EARMMS.database, migration_dir, :target => version)
  end
end

namespace :cfg do
  desc "Set default values for configuration keys that are not already set"
  task :defaults do |t|
    do_setup

    puts "Pass one: creating non-existent entries with default values"
    EARMMS::APP_CONFIG_ENTRIES.each do |key, desc|
      cfg = EARMMS::ConfigModel.find_or_create(key: key) do |a|
        puts "  - Creating #{key.inspect}"

        a.type = desc[:type].to_s
        a.value = desc[:default].to_s
        if desc[:type] == :bool && !desc[:default].is_a?(String)
          a.value = (desc[:default] ? 'yes' : 'no')
        end
      end
    end

    puts "Pass two: correcting configuration entry types"
    EARMMS::APP_CONFIG_ENTRIES.each do |key, desc|
      cfg = EARMMS::ConfigModel.where(key: key).map do |a|
        if a.type != desc[:type].to_s
          puts "  - Correcting #{key.inspect}: #{a.type.inspect} -> #{desc[:type].to_s.inspect}"

          a.type = desc[:type].to_s
          a.save
        end
      end
    end
    
    puts "Pass three: correcting invalid boolean values"
    EARMMS::APP_CONFIG_ENTRIES.each do |key, desc|
      cfg = EARMMS::ConfigModel.where(key: key).map do |a|
        next unless a.type == 'bool'
        if a.value == 'true'
          puts "  - Correcting #{key.inspect}: true -> yes"
          a.value = 'yes'
          a.save
        elsif a.value == 'false'
          puts "  - Correcting #{key.inspect}: false -> no"
          a.value = 'no'
          a.save
        end
      end
    end
  end

  desc "Find configuration key duplicates"
  task :duplicates do |t|
    do_setup

    keys = {}
    EARMMS::ConfigModel.all.each do |cfg|
      keys[cfg.key] ||= []
      keys[cfg.key] << [cfg.id, cfg.value]
    end

    keys.each do |key, values|
      if values.count > 1
        puts "Key #{key.inspect} has #{values.count} entries:"
        values.each do |v|
          puts "\tID #{v.first}: #{v.last.inspect}"
        end
      end
    end
  end
end
