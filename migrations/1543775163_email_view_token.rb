Sequel.migration do
  change do
    add_column :mass_emails, :view_token, String
  end
end
