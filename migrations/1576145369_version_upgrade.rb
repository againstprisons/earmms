Sequel.migration do
  change do
    create_table :earmms_upgrades do
      primary_key :id
      Integer :upgrade_id

      String :mode
      String :status
      String :data
    end
  end
end
