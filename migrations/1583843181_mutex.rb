Sequel.migration do
  change do
    create_table :mutexes do
      primary_key :id
      
      foreign_key :user, :users, null: false
      String :type, null: false
      String :data

      DateTime :creation, null: false, default: Sequel.function('NOW')
    end
  end
end
