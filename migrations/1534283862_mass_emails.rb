Sequel.migration do
  change do
    create_table :mass_emails do
      primary_key :id
      foreign_key :user, :users

      Time :creation
      String :target
      String :subject
      String :content
    end
  end
end
