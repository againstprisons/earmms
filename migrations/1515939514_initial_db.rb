Sequel.migration do
  change do
    create_table :users do
      primary_key :id

      String :email
      String :password_hash

      Time :creation
      Time :last_login
    end

    create_table :tokens do
      primary_key :id
      foreign_key :user, :users

      String :token
      TrueClass :valid

      Time :created
      Time :last_used
    end
  end
end
