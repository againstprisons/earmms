Sequel.migration do
  change do
    create_table :profiles do
      primary_key :id
      foreign_key :user, :users

      String :full_name
      String :birthday
      String :address
      String :phone_home
      String :phone_mobile
      String :membership_status
      String :branch
      String :prisoner_number
    end
  end
end
