Sequel.migration do
  change do
    create_table :user_has_role do
      primary_key :id
      foreign_key :user, :users

      String :role
    end
  end
end
