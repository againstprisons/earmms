Sequel.migration do
  change do
    add_column :groups, :user_agreement, String
  end
end
