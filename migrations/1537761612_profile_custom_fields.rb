Sequel.migration do
  up do
    add_column :profiles, :custom_fields, String
  end

  down do
    drop_column :profiles, :custom_fields
  end
end
