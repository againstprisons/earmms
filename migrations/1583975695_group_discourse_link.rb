Sequel.migration do
  change do
    alter_table :groups do
      add_column :discourse_link, String, null: true
    end
  end
end
