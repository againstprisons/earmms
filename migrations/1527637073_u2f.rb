Sequel.migration do
  change do
    create_table :u2f_registrations do
      primary_key :id
      foreign_key :user, :users

      String :name
      String :key_handle
      String :public_key
      String :certificate
      Integer :counter
    end
  end
end
