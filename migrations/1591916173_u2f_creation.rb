Sequel.migration do
  change do
    alter_table :u2f_registrations do
      add_column :creation, DateTime, null: false, default: Sequel.function(:NOW)
    end
  end
end
