Sequel.migration do
  change do
    add_column :tokens, :extra_data, String
  end
end
