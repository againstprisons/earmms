Sequel.migration do
  up do
    from(:groups).where(restrict_to_member: nil).update(restrict_to_member: false)
    from(:groups).where(is_joinable: nil).update(is_joinable: true)

    alter_table :groups do
      set_column_default :restrict_to_member, false
      set_column_not_null :restrict_to_member
      set_column_default :is_joinable, true
      set_column_not_null :is_joinable
    end
  end

  down do
    alter_table :groups do
      set_column_allow_null :restrict_to_member
      set_column_default :restrict_to_member, nil
      set_column_allow_null :is_joinable
      set_column_default :is_joinable, nil
    end
  end
end
