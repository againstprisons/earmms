Sequel.migration do
  up do
    add_column :tokens, :use, String
    from(:tokens).update(:use => 'session')
  end

  down do
    drop_column :tokens, :use
  end
end
