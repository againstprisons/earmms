Sequel.migration do
  up do
    add_column :work_queue, :status, String
    add_column :work_queue, :log, String
    add_column :work_queue, :created, String
    add_column :work_queue, :run_at, String
    add_column :work_queue, :started, String
    add_column :work_queue, :finished, String

    t = Time.at(0)

    from(:work_queue).update({
      :status => 'complete',
      :log => nil,
      :created => t,
      :run_at => t,
      :started => t,
      :finished => t,
    })
  end

  down do
    drop_column :work_queue, :status
    drop_column :work_queue, :log
    drop_column :work_queue, :created
    drop_column :work_queue, :run_at
    drop_column :work_queue, :started
    drop_column :work_queue, :finished
  end
end
