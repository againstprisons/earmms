Sequel.migration do
  change do
    create_table :membership_stats do
      primary_key :id
      DateTime :date

      Integer :branch
      Integer :members
      Integer :supporters
    end
  end
end
