Sequel.migration do
  change do
    create_table :work_queue do
      primary_key :id

      String :task
      String :data
    end
  end
end
