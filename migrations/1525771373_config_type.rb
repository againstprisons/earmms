Sequel.migration do
  up do
    add_column :config, :type, String
    from(:config).update(:type => 'text')
  end

  down do
    drop_column :config, :type
  end
end
