Sequel.migration do
  change do
    create_table :groups do
      primary_key :id

      String :type

      String :name
      String :description
      TrueClass :is_joinable
    end

    create_table :group_members do
      primary_key :id
      foreign_key :group, :groups

      String :user
      String :roles
    end

    create_table :group_member_filters do
      primary_key :id
      foreign_key :group_member, :group_members

      String :filter_label
      String :filter_value
    end
  end
end
