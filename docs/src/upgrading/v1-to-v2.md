# Version 1 to Version 2

The `v1-config-migrate` tool and the Initial Setup flow in EARMMS work together
to migrate the configuration from an EARMMS version 1 installation to the
upgraded version 2 installation.

EARMMS version 1 uses a Ruby configuration file to define the core settings of
the installation. EARMMS version 2 instead stores these values in the database,
and the `v1-config-migrate` tool exists to convert an EARMMS version 1 config
file to a configuration format that EARMMS version 2 can use.

If you are interested in the structure of the output of the `v1-config-migrate`
tool, please see the [Upgrade v1 to v2][] document in the "For developers"
section of this documentation.

[Upgrade v1 to v2]: ../for-developers/upgrade-v1-to-v2.md

## Performing the upgrade

### Step one: `v1-config-migrate`

The `v1-config-migrate` tool is designed to make migrating an EARMMS version 1
configuration file to the new format as easy as possible.

Running the tool will output something like the following:

```
$ cd /path/to/earmms
$ bundle exec ./bin/v1-config-migrate ~/earmms-config.rb ~/config.txt
[..] Configuration file: /Users/c/earmms-config.rb
[..] Loaded configuration file, starting conversion
[..] Saving output to /Users/c/config.txt
[..] Complete! Please read the output file for instructions.
```

Once you see the "Complete!" message, open the output file (in this example,
`~/config.txt`) in a text editor. You will see a blob of configuration data -
make sure you have access to this when starting the upgrade, as this contains
all of your configuration.

### Step two: initial setup wizard

To continue, open your browser to your EARMMS instance. Your EARMMS instance
will be in an upgrade lock - the site will not be functional until you have
completed the upgrade path.

If you were not logged in to your EARMMS instance, you may see a "service
unavailable" page. This page has a link at the bottom to log in, which you
should do.

Once you are logged in, you should see the EARMMS initial setup wizard, an
example of which is shown below.

[![](../img/initial-setup-splash.png)](../img/initial-setup-splash.png)

To start the upgrade process, click the "Start upgrade process" button. You will
be prompted to paste in your configuration blob - this is the blob that was in
the output file you specified when calling `v1-config-migrate`.

[![](../img/initial-setup-blob.png)](../img/initial-setup-blob.png)

Paste in your configuration blob and click "Continue". You will now be presented
with a page that details all of the configuration changes that will be performed
when the upgrade is applied.

[![](../img/initial-setup-verify.png)](../img/initial-setup-verify.png)

You should review these changes, and if you are satisfied, click "Continue".

The next step is a parse test of the new configuration values. A list of all
of the configuration keys will be presented, and beside each one will be either
a check mark or a warning sign. If a check mark is present, the parse test
was successful for that key. If a warning sign is present, the parse test had
warnings for that key. If there are any warnings, you should review these and
correct them.

It is not recommended to continue if there are parse errors - if these errors
are in the imported configuration, you should fix these in your EARMMS version
1 configuration file, re-run `v1-config-migrate`, and click the button at the
top of the page to discard the current configuration and start over.

[![](../img/initial-setup-parse.png)](../img/initial-setup-parse.png)

If your configuration contained custom fields, you will now be presented with a
page that previews what those custom fields will look like in the membership
administration panel. You should confirm that these match your configuration,
and if you're satisfied, click "Continue."

[![](../img/initial-setup-custom-fields.png)](../img/initial-setup-custom-fields.png)

The next and final step is to apply the configuration changes. This page also
has a summary of the configuration changes that will be made. When you are ready
to continue, enter the given verification code into the field, and click the
"Apply this configuration" button.

[![](../img/initial-setup-apply.png)](../img/initial-setup-apply.png)

You're now at the end, congratulations! Clicking the "Complete upgrade" button
will release the upgrade lock, refresh the application configuration (including
performing a phased restart if the application is running with multiple
workers), and turn on maintenance mode.

[![](../img/initial-setup-complete.png)](../img/initial-setup-complete.png)

You should use the opportunity of the site being in maintenance mode to double
check all of the configuration of your EARMMS instance, and familiarise yourself
with the changes in EARMMS version 2.
