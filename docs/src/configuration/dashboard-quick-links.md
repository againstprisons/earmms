# Dashboard quick links

EARMMS provides the ability to define a set of "quick lines", visible on
the dashboard to logged in users. These are defined with a set of
configuration entries, the details of which are below.

## `dashboard-quick-links-enabled`

The `dashboard-quick-links-enabled` configuration entry is a boolean,
which controls the visibility of the defined quick links, if any.

## `dashboard-quick-links-attributes`

The `dashboard-quick-links-attributes` configuration entry is a JSON hash
containing attributes which define how the quick links are shown. The valid
keys of this hash are:

* `style` - The style of the displayed links (explained below).
* `title` - The title of the section of the dashboard that contains the
    quick links. If this is set to `null`, the default title ("Quick links")
	is shown.
  
### The `style` attribute

The `style` attribute determines how to display the defined quick links. The
two styles that are currently available are `"buttons"`, and `"tiles"`.

The `"buttons"` style shows the defined quick links as a list of buttons,
spanning the full width of the page, one below the other. This looks like the
following:

[![](../img/quick-links-buttons.png)](../img/quick-links-buttons.png)

The `"tiles"` style shows the defined quick links as rows of tiles. The tiles
are a fixed width and height. This looks like the following:

[![](../img/quick-links-tiles.png)](../img/quick-links-tiles.png)

## `dashboard-quick-links-content`

The `dashboard-quick-links-content` configuration entry is a JSON array of
hashes, each hash containing the data for a single link.

As an example, this is the content of the `dashboard-quick-links-content` 
configuration entry for the above style examples:

```json
[
  {
    "colour": "primary",
    "href": "http://example.com",
    "text": "Example link one",
    "icon": "fa-link"
  },
  {
    "colour": "#aa00aa",
    "href": "http://example.com",
    "text": "Example link two",
    "icon": "fa-info"
  },
  {
    "colour": "#3399aa",
    "href": "http://example.com",
    "text": "Example link three",
    "icon": "fa-file-o"
  }
]
```

The available keys are as follows:

* `href` - The link URL
* `text` - The text to display for the link
* `icon` (optional) - The icon to display for the link
  * If the value of the `icon` field starts with `fa-`, it is treated as a
    Font Awesome icon name
* `colour` (optional) - A name of a defined button style, a CSS colour name,
    or a CSS colour code
  * The defined button styles are `primary`, `success`, `warning`, and `error`
* `classes` (optional) - Additional CSS classes to apply to the link element
* `only_statuses` (optional) - An array of membership status values, where the
    link is only shown to a user if that user's membership status is in the
    list
