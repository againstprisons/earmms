# Environment variables

## `RACK_ENV`

Set the `RACK_ENV` environment variable to the current application
environment.

Allowed values are:

- `production`
- `development`

## `DATABASE_URL`

The `DATABASE_URL` environment variable must be set to the URL to the
application database. In production, it is recommended to use PostgreSQL, and
as such this URL would begin with `postgres://`.

## `SESSION_SECRET`

The contents of the `SESSION_SECRET` environment variable are used to protect
the contents of the session cookie sent to a user's browser. This should be
set to a sufficiently random string. The value must remain the same across
application restarts, as changing the value of this environment variable makes
all previous sessions invalid (therefore requiring users to sign in again).

## `SITE_DIR`

The `SITE_DIR` environment variable should be set to the path to the site
directory for this EARMMS installation. The site directory should contain the
theme, if one is being used.

If a file named `config.rb` exists in the site directory, it is loaded when
the application is started.

## `EARMMS_FORCE_THEME_OFF`

Set the `EARMMS_FORCE_THEME_OFF` environment variable to any value to stop
EARMMS from using a theme, even if one is specified in the configuration file.

## Key derivation service variables

The `KEYDERIV_URL` variable must be set, pointing to an `earmms_keyderiv`
instance. 

If using `earmms_keyderiv` v2.x:

- The `KEYDERIV_URL` variable must contain the named target used for this
    installation (for example, `http://localhost:8080/?target=earmms`),
- The `KEYDERIV_USE_2X` variable must be set to `true`,
- The `KEYDERIV_SECRET` variable must be set to the shared secret that is
    also in the `earmms_keyderiv` configuration for the named target used
    for this installation (the one also given in the `KEYDERIV_URL` variable).
