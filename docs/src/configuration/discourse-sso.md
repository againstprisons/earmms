# Discourse SSO

## `discourse-sso-enabled`

This boolean value determines whether Discourse SSO is enabled.

## `discourse-sso-data`

This JSON value contains all of the configuration data for the Discourse SSO
provider.

An example Discourse SSO configuration looks like this:

```json
{
  "secret": "0123456789abcdef",
  "admin-roles": [
    "discourse_sso:admin"
  ],
  "allowed-hosts": [
    "forum.example.com"
  ],
  "limit-membership-status": [
    "member"
  ],
  "groups": [
    {
      "name": "members",
      "grant-on": [
        {
          "mode": "membership-status",
          "statuses": [
            "member"
          ]
        }
      ]
    },
    {
      "name": "admins",
      "grant-on": [
        {
          "mode": "role",
          "roles": [
            "discourse_sso:admin"
          ]
        }
      ]
    }
  ]
}
```

The keys in this configuration JSON are as follows:

* `secret` is the HMAC secret used for signing messages
* `admin-roles` is a list of EARMMS roles, users authenticating to Discourse
    who have any of these roles will be given admin privileges on Discourse
* `allowed-hosts` is a list of hostnames of Discourse installations that are
    allowed to authenticate with this EARMMS instance
* `limit-membership-status` is a list of membership statuses, only users who
    have these membership statuses will be allowed to authenticate with
    Discourse
* `groups` is a list of Discourse groups, see below.

### Groups

The keys for each group are as follows:

* `name` is the name of the group in Discourse
* `grant-on` is a list of conditions, of which all must be met for the
    authenticating user to be granted that group. See below for details.

#### Granting access to groups

Each group has a list of `grant-on` conditions, and each condition can
check a different attribute of the authenticating user. The available
checks are `role` and `membership-status`. Multiple conditions can be
specified, and the group will only be granted to the authenticating
user if *all* of the conditions are met.

If a `grant-on` condition's mode is set to `membership-status`, the
`statuses` key is checked. If the authenticating user belongs to any
one of the membership statuses given in the `statuses` list, this
check passes.

If a `grant-on` condition's mode is set to `role`, the `roles` key is
checked. If the authenticating user has any one of the roles given in
the `roles` list granted to them, this check passes. Note that if you
wish to assert that an authenticating user has multiple different roles,
you must specify each role in it's own `grant-on` check.

For example, if you wish to only grant a group to a user who has permission
to access both the membership admin panel and the system admin panel, the
following `grant-on` list would work:

```json
{
  ...

  "grant-on": [
    {
      "mode": "role",
      "roles": ["system:access"]
    },
    {
      "mode": "role",
      "roles": ["admin:access"]
    }
  ],

  ...
}
```
