# Upgrade v1 to v2

The `v1-config-migrate` tool takes an EARMMS v1.x configuration file, does
some processing on it, and outputs a "configuration blob". Configuration
blobs are base64-encoded JSON hashes, where the root hash contains at least
the two keys `generator` and `config`.

The value of the `generator` key must always be a two-entry array, where the
first value of the array is the string `"v1-config-migrate"` and the second
value of the array is the configuration blob version number (see the
["Version information"](#version-information) section below for the details of
the configuration blob versions).

## Version information

Below is a list of all of the configuration blob versions, with the newest at
the top. Each version lists it's changes from the previous version, as well as
a summary.

* **Version 1** - 2019-12-13:

  Version 1 is the base configuration blob format.

  In version 1, the value of the `config` key must always be a hash, the keys
  and values of which directly correspond to configuration entry keys and values
  that are stored in the database upon a successful upgrade.
