# Home

Welcome to the EARMMS documentation! EARMMS is an Encrypted At Rest Membership
Management System. EARMMS is one of many open source projects developed by and
for [People Against Prisons Aotearoa (PAPA)][papa]. More of PAPA's open source
projects can be found at [againstprisons.gitlab.io][papa-oss].

[papa]: https://papa.org.nz
[papa-oss]: https://againstprisons.gitlab.io
