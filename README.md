# EARMMS

An encrypted-at-rest membership management system designed for
[People Against Prisons Aotearoa](https://papa.org.nz).

The `main` branch of this repository - which you are currently viewing -
contains EARMMS `v2.x`, which is currently in beta. The current beta
release of EARMMS `v2.0.0` (which, as of this writing, is `v2.0.0-beta.6`)
is considered stable enough to be used for new installations - using EARMMS
`v1.x` is not recommended if you are performing a clean install of EARMMS.

Until `v2.0.0` reaches proper "stable" status - that is, when the `v2.0.0`
release is tagged - EARMMS `v1.x` can be found in the `stable` branch.

## Installation

To install dependencies and build the static assets:

```
$ bundle install
$ npm install
$ npm run build
```

For more information on configuring EARMMS, see [INSTALL.md](./INSTALL.md).

## Contributors

We wish to thank the many contributors to EARMMS, including those who
contribute in ways other than writing code. You can see a list of
contributors to this project in the [CONTRIBUTORS.md](./CONTRIBUTORS.md)
file in this repository.

## Code of conduct

Please note that this project is released with a Contributor Code of Conduct.
By participating in this project you agree to abide by its terms, which can
be found in the [CODE-OF-CONDUCT.md](./CODE-OF-CONDUCT.md) file in this
repository.

## License

EARMMS is released under the permissive MIT license. For license details,
please see the [LICENSE](./LICENSE) file in this repository.
