export const enableSignupInvite = () => {
  let container = document.getElementById('auth-signup')
  if (container) {
    let content = document.getElementById('auth-signup-content')
    let toggle = document.getElementById('auth-signup-invite-toggle')
    let form = document.getElementById('auth-signup-invite-entry')

    form.style.display = 'none'
    toggle.style.display = 'inline-block'

    toggle.addEventListener('click', () => {
      form.style.display = 'block'
      content.style.display = toggle.style.display = 'none'
    })
  }
}

enableSignupInvite()
