export const enableFieldDisplayOnlyIf = () => {
  let els = document.querySelectorAll("[data-display-only-if]")
  Array.from(els).forEach((el) => {
    let label = document.querySelector("label[for = '" + el.id + "']")

    Array.from(JSON.parse(el.getAttribute('data-display-only-if'))).forEach((data) => {
      let target_els = document.querySelectorAll("[name = '" + data["field"] + "']")
      let values = data["values"]
      console.log("values", values)

      Array.from(target_els).forEach((target_el) => {
        let doChange = () => {
          var target_value = target_el.value
          if (target_el.type === "checkbox") {
            target_value = (target_el.checked ? "on" : "off")
          }

          console.log("target_value", target_value)
          if (values.indexOf(target_value) != -1) {
            el.style.display = 'block'
            if (label) label.style.display = 'block'
          } else {
            el.style.display = 'none'
            if (label) label.style.display = 'none'
          }
        }

        target_el.addEventListener('change', () => doChange())
        target_el.addEventListener('input', () => doChange())
        doChange()
      })
    })
  })
}

enableFieldDisplayOnlyIf()
