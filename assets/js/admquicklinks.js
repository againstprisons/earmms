const defaultIconText = "fa-link"
const defaultButtonText = "Example link text"
const defaultCustomColour = "#00aa66"
const allowedButtonClasses = ["primary", "success", "warning", "error"]

export const enableAdminQuickLinkEditor = () => {
  let exampleButton = document.getElementById("create-btn-example")
  if (exampleButton !== undefined) {
    let exampleButtonStyle = document.getElementById("create-btn-example-style")
    let exampleButtonIcon = document.getElementById("create-btn-example-icon")
    let exampleButtonText = document.getElementById("create-btn-example-text")

    let fieldText = document.getElementById("create-btn-text")
    let fieldColourType = document.getElementById("create-btn-colour-type")
    let fieldColourCustom = document.getElementById("create-btn-colour-custom")
    let fieldIcon = document.getElementById("create-btn-icon")

    let updateCustomColour = (c) => {
      exampleButtonStyle.innerText = ""

      if (c !== "" && c !== undefined) {
        var style = `#create-btn-example { background: ${c} }`
        exampleButtonStyle.appendChild(document.createTextNode(style))
      }
    }

    let updateIcon = (i) => {
      Array.from(exampleButtonIcon.children).forEach(c => c.remove())

      if (i.startsWith("fa-")) {
        var iconEl = document.createElement('i')
        iconEl.classList.add("fa", i)
        exampleButtonIcon.appendChild(iconEl)
      }
    }

    // Icon field
    fieldIcon.addEventListener("input", ev => updateIcon(ev.target.value))

    // Text field
    fieldText.addEventListener("input", (ev) => {
      var text = ev.target.value
      if (text === "") text = defaultButtonText
      exampleButtonText.innerText = text
    })

    // Custom colour field
    fieldColourCustom.addEventListener("input", (ev) => {
      let customColour = ev.target.value
      if (customColour === "") customColour = defaultCustomColour
      updateCustomColour(customColour)
    })

    // Colour type field
    fieldColourType.addEventListener("input", (ev) => {
      exampleButton.classList.remove("button-primary", "button-success", "button-warning", "button-error")

      var type = ev.target.value
      if (allowedButtonClasses.includes(type)) {
        updateCustomColour(undefined)
        exampleButton.classList.add(`button-${type}`)
      } else if (type === 'custom') {
        updateCustomColour(fieldColourCustom.value)
      }
    })

    // Set colour from field values
    if (allowedButtonClasses.includes(fieldColourType.value)) {
      updateCustomColour(undefined)
      exampleButton.classList.add(`button-${fieldColourType.value}`)
    } else {
      updateCustomColour(fieldColourCustom.value)
    }

    if (exampleButton.getAttribute('data-defaults') === 'no') {
      // Set text and icon from field values
      updateIcon(fieldIcon.value)
      exampleButtonText.innerText = fieldText.value
    } else {
      // Set default icon
      fieldIcon.value = defaultIconText
      updateIcon(defaultIconText)

      // Set default text
      fieldText.value = defaultButtonText
      exampleButtonText.innerText = defaultButtonText
    }

    document.getElementById("create-btn-example-loading").remove()
  }
}

window.earmms = window.earmms || {}
window.earmms.enableAdminQuickLinkEditor = enableAdminQuickLinkEditor
enableAdminQuickLinkEditor()
