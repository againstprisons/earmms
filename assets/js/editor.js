import ClassicEditor from '@ckeditor/ckeditor5-build-classic'

let editorMaxTries = 10
let editorConfig = {
  removePlugins: ['ImagePlugin'],
  toolbar: [
    'heading',
    '|',
    'bold',
    'italic',
    'link',
    'bulletedList',
    'numberedList',
    'blockQuote',
    '|',
    'undo',
    'redo',
  ],
}

export const enableEditor = (editorElement) => {
  window.earmms.rich_editors = window.earmms.rich_editors || {}

  if (typeof(editorElement) !== "object") return
  let editorId = editorElement.id
    
  if (typeof(window.earmms.rich_editors[editorId]) === "undefined") {
    let p = editorElement.parentElement
    window.earmms.rich_editors[editorId] = {
      parentElement: p,
      loadingElement: p.querySelector('.editor-message-loading'),
      errorElement: p.querySelector('.editor-message-error'),
      editorTries: 1,
    }
  }

  console.log("Trying to load editor on element id", editorId, "try number", window.earmms.rich_editors[editorId].editorTries)

  if (typeof(ClassicEditor) === "undefined") {
    window.earmms.rich_editors[editorId].editorTries = window.earmms.rich_editors[editorId].editorTries + 1
    if (window.earmms.rich_editors[editorId].editorTries > editorMaxTries) {
      console.error("Timed out trying to load editor on element id", editorId)
      window.earmms.rich_editors[editorId].loadingElement.style.display = 'none'
      window.earmms.rich_editors[editorId].errorElement.style.display = 'block'
      return
    }

    setTimeout(() => enableEditor(editorElement), 1000)
    return
  }

  console.log("ClassicEditor loaded, creating on element id", editorId)
  ClassicEditor.create(editorElement, editorConfig)
    .then((editor) => {
      window.earmms.rich_editors[editorId].editorObject = editor

      console.log("Editor created on element id", editorId)
      window.earmms.rich_editors[editorId].loadingElement.style.display = 'none'
      window.earmms.rich_editors[editorId].errorElement.style.display = 'none'
    }).catch((e) => {
      console.error("Error while creating editor on element id", editorId, "error", e)

      window.earmms.rich_editors[editorId].loadingElement.style.display = 'none'
      window.earmms.rich_editors[editorId].errorElement.style.display = 'block'
    })
}

export const enableAllEditors = () => {
  Array.from(document.querySelectorAll('.rich-editor')).forEach((el) => {
    enableEditor(el)
  })
}

window.earmms = window.earmms || {}
enableAllEditors()
