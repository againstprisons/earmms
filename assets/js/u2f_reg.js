import _u2f from 'u2f-api-polyfill'

export const doU2FReg = () => {
  let data_el = document.querySelector('#u2f-data')
  if (!data_el) { return }
  let data = JSON.parse(data_el.innerText)
  if (!data || data.mode !== "register") { return }

  let form_el = document.querySelector("#u2f-form")
  let message_el = document.querySelector("#u2f-message")
  message_el.innerHTML = data.messages.insert

  try {
    u2f.register(data.app_id, data.reg_requests, data.sign_requests, function(resp) {
      if (resp.errorCode) {
        message_el.innerHTML = data.messages.error + ` (${resp.errorCode})`
        return
      }

      let response_el = document.querySelector("#u2f-response")
      response_el.value = JSON.stringify(resp)

      message_el.innerHTML = data.messages.success
      form_el.submit()
    })
  } catch (e) {
    message_el.innerHTML = data.messages.error + " (exception)"
    throw e
  }
}

doU2FReg()
