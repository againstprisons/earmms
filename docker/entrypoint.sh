#!/bin/sh

echo "==> Running database migrations..."
bundle exec rake db:migrate || exit 1

echo "==> Configuring..."
bundle exec rake cfg:defaults || exit 1

cat <<-ENDHELP
================================================================================
Welcome to EARMMS! Signups are disabled by default, so in order to get started
you'll need to generate an invite code. To generate an invite code that grants
administration privileges to the user who signs up using it, run the following
command (substituting CONTAINER_NAME with the name of this Docker container):

$ docker exec CONTAINER_NAME bundle exec rake site:invite_admin

Once you have signed up, you will be shown the initial setup wizard. Follow the
instructions in this wizard to set up your EARMMS instance!
================================================================================
ENDHELP

echo "==> Starting supervisord"
exec supervisord -c /etc/supervisor/supervisord.conf
