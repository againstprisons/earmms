class EARMMS::User < Sequel::Model
  def self.by_token(token)
    token = EARMMS::Token.where(token: token).first
    return nil unless token
    return nil unless token.valid
    return nil unless token.use == 'session'

    user = self[token.user]
    token.last_used = Time.now
    token.save

    user
  end

  def get_roles
    EARMMS::UserHasRole.where(user: self.id).map do |uhr|
      {
        :role => uhr.role.downcase,
        :id => uhr.id,
      }
    end
  end

  def get_tag
    t = self.decrypt(:tag)
    if t.nil? || t&.empty?
      t = EARMMS::Token.generate_short.token
      self.encrypt(:tag, t)
      self.save
    end

    t
  end

  def verify_password(password)
    return EARMMS::Crypto.password_verify(self.password_hash, password)
  end

  def email=(e)
    e = e&.strip&.downcase
    super
  end

  def password=(password)
    self.password_hash = EARMMS::Crypto.password_hash(password)
  end

  def invalidate_tokens(current_token)
    EARMMS::Token.where(user: self.id, use: 'session').each do |token|
      next if token.token == current_token
      token.invalidate!
      token.save
    end
  end

  def send_email_verification!
    token = EARMMS::Token.generate_email_verify(self)
    token.save

    text = EARMMS.email_templates.email_verify(email, token.token)
    qm = EARMMS::EmailQueue.new(creation: Time.now)
    qm.save
    qm.queue_status = 'queued'
    qm.encrypt(:recipients, JSON.generate({"type" => "list", "list" => [self.email]}))
    qm.encrypt(:subject, EARMMS.email_subject("Verify your email address"))
    qm.encrypt(:content, text.content_text)
    qm.encrypt(:content_html, text.content_html)
    qm.save
  end

  def send_password_reset!
    token = EARMMS::Token.generate_password_reset(self)
    token.save

    text = EARMMS.email_templates.password_reset(email, token.token)
    qm = EARMMS::EmailQueue.new(creation: Time.now)
    qm.save
    qm.queue_status = 'queued'
    qm.encrypt(:recipients, JSON.generate({"type" => "list", "list" => [self.email]}))
    qm.encrypt(:subject, EARMMS.email_subject("Password reset for #{EARMMS.app_config['site-name']}"))
    qm.encrypt(:content, text.content_text)
    qm.encrypt(:content_html, text.content_html)
    qm.save
  end

  def send_delete_confirm_email!
    token = EARMMS::Token.generate_delete_confirm(self)
    token.save

    text = EARMMS.email_templates.delete_confirm(email, token.token)
    qm = EARMMS::EmailQueue.new(creation: Time.now)
    qm.save
    qm.queue_status = 'queued'
    qm.encrypt(:recipients, JSON.generate({"type" => "list", "list" => [self.email]}))
    qm.encrypt(:subject, EARMMS.email_subject("Confirmation: Account deletion"))
    qm.encrypt(:content, text.content_text)
    qm.encrypt(:content_html, text.content_html)
    qm.save
  end

  def delete_account!(opts = {})
    profile = EARMMS::Profile.for_user(self)

    unless opts[:suppress_email]
      text = EARMMS.email_templates.user_deletion(self, profile)
      qm = EARMMS::EmailQueue.new(creation: Time.now)
      qm.save
      qm.queue_status = 'queued'
      qm.encrypt(:recipients, JSON.generate({"type" => "roles", "roles" => ['admin:alert_emails']}))
      qm.encrypt(:subject, EARMMS.email_subject("User deletion summary"))
      qm.encrypt(:content, text.content_text)
      qm.encrypt(:content_html, text.content_html)
      qm.save
    end

    self.convert_attendance_to_guest!

    EARMMS::UserFilter.where(user: self.id).delete
    EARMMS::ProfileFilter.where(profile: profile.id).delete
    EARMMS::Token.where(user: self.id).delete
    EARMMS::UserHasRole.where(user: self.id).delete
    EARMMS::MassEmail.where(user: self.id).update(user: nil)
    EARMMS::ProfileChangelog.where(profile: profile.id).delete
    EARMMS::U2FRegistration.where(user: self.id).delete

    EARMMS::GroupMemberFilter.perform_filter(:user, self.id.to_s).map do |x|
      gm = x.group_member
      x.delete
      gm
    end.uniq.map do |x|
      EARMMS::GroupMember[x].delete
    end

    EARMMS::BranchMeetingAttendanceRecordFilter.perform_filter(:profile, profile.id.to_s).each do |x|
      at = EARMMS::BranchMeetingAttendanceRecord[x.attendance_record]
      x.delete
      at.delete
    end

    profile.delete
    self.delete
  end

  def convert_attendance_to_guest!
    profile = EARMMS::Profile.for_user(self)

    meetings = []
    EARMMS::BranchMeetingAttendanceRecordFilter.perform_filter(:profile, profile.id.to_s).each do |x|
      at = EARMMS::BranchMeetingAttendanceRecord[x.attendance_record]
      meetings << at.meeting if at.decrypt(:attendance_status) == "present"
      x.delete
      at.delete
    end

    # add this user to the guests for each meeting they were present at
    meetings.uniq.each do |meetingid|
      m = EARMMS::BranchMeeting[meetingid]
      next nil unless m

      guests = m.decrypt(:guests)
      guests = guests.lines.map(&:strip)
      guests << profile.decrypt(:full_name)
      guests = guests.join("\n")
      m.encrypt(:guests, guests)
      m.save
    end
  end
end

class EARMMS::UserFilter < Sequel::Model
  def self.clear_filters_for(user)
    user = user.id if user.respond_to? :id
    self.where(user: user).delete
  end

  def self.perform_filter(column, search)
    e = EARMMS::Crypto.index("User", column.to_s.downcase, search)
    return self.where(filter_label: column.to_s.downcase, filter_value: e)
  end
end
