class EARMMS::MutexModel < Sequel::Model(:mutexes)
  def self.type_data
    {
      'initial_setup' => {
        type: 'initial_setup',
        friendly_tl: :'initial_setup/title',
        path_prefix: '/initialsetup',        
      },
      'membership-dashboard-quick-links' => {
        type: 'membership-dashboard-quick-links',
        friendly_tl: :'membership/configuration/quick_links/title',
        path_prefix: '/admin/config/quick-links',
      }
    }
  end

  def self.acquire!(user, type, opts = {})
    user = user.id if user.respond_to?(:id)

    # If we already have a mutex for this +type+, return that if the user
    # matches, else return `nil` (to indicate a lock was unsuccessful)
    self.where(type: type).each do |e|
      if e.user == user
        return {mine: true, mutex: e, new: :existing}
      end

      return {mine: false, mutex: e, new: :existing}
    end

    # If we get here, there is no lock.
    # If the `:no_new` option is set, return a failure.
    if opts[:no_new]
      return {mine: false, mutex: nil, new: :no_new}
    end
    
    # If `:no_new` is not set, create a new mutex.
    mutex = self.new(user: user, type: type, creation: Time.now)
    mutex.data = opts[:data] if opts[:data]
    mutex.save

    {mine: true, mutex: mutex, new: :new}
  end

  def release!
    self.delete
  end
end
