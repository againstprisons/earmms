class EARMMS::Group < Sequel::Model(:groups)
  def self.hashes_for_user(user)
    out = []
    user = user.id if user.respond_to?(:id)

    EARMMS::GroupMemberFilter.perform_filter(:user, user.to_s).map do |gmf|
      gm = EARMMS::GroupMember[gmf.group_member]
      g = EARMMS::Group[gm.group]

      h = g.to_h
      h[:user_is_member] = true
      h[:user_group_member] = gm
      h[:user_group_roles] = gm.decrypt(:roles)&.split(',')&.map(&:strip)&.map(&:downcase) || []

      out << h
    end

    out
  end

  def to_h
    h = {
      :group_id => self.id,
      :group_obj => self,
      :name => self.decrypt(:name),
      :type => self.type,
      :creation => self.creation,
      :description => self.decrypt(:description),
      :agreement => self.decrypt(:user_agreement),
      :secret => !self.is_joinable,
      :member_only => self.restrict_to_member,
      :discourse_link => self.decrypt(:discourse_link),
      :member_count => EARMMS::GroupMember.where(group: self.id).count,
      :user_is_member => nil,
      :user_group_member => nil,
      :user_group_roles => [],
    }

    h[:description] = nil if h[:description]&.empty?
    h[:agreement] = nil if h[:agreement]&.empty?
    h[:discourse_link] = nil if h[:discourse_link]&.empty?

    h
  end

  def admins(opts = {})
    EARMMS::GroupMember.where(group: self.id).map do |gm|
      roles = gm.decrypt(:roles)&.split(',')&.map(&:strip)&.map(&:downcase)
      next nil unless roles

      if roles.include?('admin') \
          || (opts[:include_notify_users] && roles.include?('notify')) \
          || (opts[:include_email_users] && roles.include('email'))

        user = EARMMS::User[gm.decrypt(:user).to_i]
        next nil unless user

        next user
      end
    end.compact
  end

  def summary
    g = self.to_h

    [
      {
        :name => :group_id,
        :data => g[:group_id],
      },
      {
        :name => :group_type,
        :data => g[:type],
      },
      {
        :name => :member_count,
        :data => g[:member_count],
      },
      {
        :name => :has_agreement,
        :data => !(g[:agreement].nil? || g[:agreement].empty?),
      },
      {
        :name => :is_secret,
        :data => g[:secret],
      },
      {
        :name => :member_only,
        :data => g[:member_only],
      },
    ]
  end
  
  def delete!
    EARMMS::GroupMember.where(group: self.id).each do |gm|
      EARMMS::GroupMemberFilter.clear_filters_for(gm)
      gm.delete
    end
    
    self.delete
  end
end

class EARMMS::GroupMember < Sequel::Model(:group_members)
end

class EARMMS::GroupMemberFilter < Sequel::Model(:group_member_filters)
  def self.clear_filters_for(cm)
    cm = cm.id if cm.respond_to?(:id)
    self.where(:group_member => cm).delete
  end

  def self.create_filters_for(cm)
    fields = [
      :user,
      :roles
    ]

    filters = []

    # whole field
    fields.each do |f|
      s = cm.decrypt(f).strip.downcase
      s = s.encode(Encoding::UTF_8, :invalid => :replace, :undef => :replace, :replace => "")
      EARMMS.filter_strip_chars.each do |x|
        s = s.gsub(x, "")
      end

      e = EARMMS::Crypto.index("CaucusMember", f.to_s.downcase, s)
      f = self.new(group_member: cm.id, filter_label: f.to_s, filter_value: e)
      f.save

      filters << f
    end

    # individual roles
    cm.decrypt(:roles).strip.downcase.split(",").each do |partial|
      partial = partial.encode(Encoding::UTF_8, :invalid => :replace, :undef => :replace, :replace => "")
      EARMMS.filter_strip_chars.each do |x|
        partial = partial.gsub(x, "")
      end

      e = EARMMS::Crypto.index("CaucusMember", "roles", partial)
      f = self.new(group_member: cm.id, filter_label: "roles", filter_value: e)
      f.save

      filters << f
    end

    filters
  end

  def self.perform_filter(column, search)
    s = search.strip.downcase
    s = s.encode(Encoding::UTF_8, :invalid => :replace, :undef => :replace, :replace => "")
    EARMMS.filter_strip_chars.each do |x|
      s = s.gsub(x, "")
    end

    e = EARMMS::Crypto.index("CaucusMember", column.to_s.downcase, s)
    return self.where(filter_label: column.to_s.downcase, filter_value: e)
  end
end
