class EARMMS::BranchMeetingAttendanceRecord < Sequel::Model(:branch_meeting_attendance_records)
end

class EARMMS::BranchMeetingAttendanceRecordFilter < Sequel::Model(:branch_meeting_attendance_record_filters)
  def self.clear_filters_for(at)
    at = at.id if at.respond_to? :id
    self.where(attendance_record: at).delete
  end

  def self.create_filters_for(at)
    fields = [
      :profile,
    ]

    filters = []
    fields.each do |f|
      s = at.decrypt(f).strip.downcase
      s = s.encode(Encoding::UTF_8, :invalid => :replace, :undef => :replace, :replace => "")
      EARMMS.filter_strip_chars.each do |x|
        s = s.gsub(x, "")
      end

      e = EARMMS::Crypto.index("BranchMeetingAttendanceRecord", f.to_s.downcase, s)
      f = self.new(attendance_record: at.id, filter_label: f.to_s, filter_value: e)
      f.save

      filters << f
    end

    filters
  end

  def self.perform_filter(column, search)
    s = search.strip.downcase
    s = s.encode(Encoding::UTF_8, :invalid => :replace, :undef => :replace, :replace => "")
    EARMMS.filter_strip_chars.each do |x|
      s = s.gsub(x, "")
    end

    e = EARMMS::Crypto.index("BranchMeetingAttendanceRecord", column.to_s.downcase, s)
    return self.where(filter_label: column.to_s.downcase, filter_value: e)
  end
end
