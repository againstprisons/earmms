module EARMMS::Config
  Dir.glob(File.join(EARMMS.root, 'app', 'config', '*.rb')).each do |f|
    require f
  end

  module_function
  def parsers
    list = EARMMS::Config.constants.map do |cname|
      p = EARMMS::Config.const_get(cname)
      next unless p.respond_to?(:accept?)

      p
    end

    list.compact.sort{|a, b| a.order <=> b.order}
  end
end
