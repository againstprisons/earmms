require 'erb'
require 'ostruct'
require 'addressable'
require 'sanitize'

class EARMMS::EmailTemplates
  attr_accessor :template_root

  def initialize(config = {})
    @template_root = config[:template_root] || File.join(EARMMS.root, "app", "views", "email_templates")
  end

  def base_data
    OpenStruct.new({
      :site_name => EARMMS.app_config['site-name'],
      :org_name => EARMMS.app_config['org-name'],
      :base_url => EARMMS.app_config['base-url'],
    })
  end

  def password_reset(email, token)
    text_template = ERB.new(File.read(File.join(@template_root, "password_reset.text.erb")))
    html_template = ERB.new(File.read(File.join(@template_root, "password_reset.html.erb")))

    data = base_data()
    data.email = email

    link = Addressable::URI.parse(data.base_url)
    link += "/auth/reset/#{token}"
    data.reset_link = link.to_s

    b = data.instance_eval do
      binding
    end

    out = OpenStruct.new()
    out.content_text = text_template.result(b)
    out.content_html = html_template.result(b)

    out
  end

  def email_verify(email, token)
    text_template = ERB.new(File.read(File.join(@template_root, "email_verify.text.erb")))
    html_template = ERB.new(File.read(File.join(@template_root, "email_verify.html.erb")))

    data = base_data()
    data.email = email

    link = Addressable::URI.parse(data.base_url)
    link += "/auth/email-verify/#{token}"
    data.verify_link = link.to_s

    b = data.instance_eval do
      binding
    end

    out = OpenStruct.new()
    out.content_text = text_template.result(b)
    out.content_html = html_template.result(b)

    out
  end

  def delete_confirm(email, token)
    text_template = ERB.new(File.read(File.join(@template_root, "delete_confirm.text.erb")))
    html_template = ERB.new(File.read(File.join(@template_root, "delete_confirm.html.erb")))

    data = base_data()
    data.email = email

    link = Addressable::URI.parse(data.base_url)
    link += "/auth/account-delete/confirm"
    link.query_values = {token: token}
    data.confirm_link = link.to_s

    b = data.instance_eval do
      binding
    end

    out = OpenStruct.new()
    out.content_text = text_template.result(b)
    out.content_html = html_template.result(b)

    out
  end

  def alert(alert, role)
    text_template = ERB.new(File.read(File.join(@template_root, "alert.text.erb")))
    html_template = ERB.new(File.read(File.join(@template_root, "alert.html.erb")))

    data = base_data()
    data.role = role
    data.alert = alert

    alert_link = Addressable::URI.parse(data.base_url)
    if alert[:section].to_s.downcase == "membership"
      alert_link += '/admin/alerts'
    else
      alert_link += '/system/alerts'
    end
    data.view_alert_link = alert_link

    user_link = Addressable::URI.parse(data.base_url)
    user_link += "/admin/user/edit/#{alert[:user] ? alert[:user].id.to_s : '0'}"
    data.view_user_link = user_link

    b = data.instance_eval do
      binding
    end

    out = OpenStruct.new()
    out.content_text = text_template.result(b)
    out.content_html = html_template.result(b)

    out
  end

  def user_deletion(user, profile)
    text_template = ERB.new(File.read(File.join(@template_root, "user_deletion.text.erb")))
    html_template = ERB.new(File.read(File.join(@template_root, "user_deletion.html.erb")))

    data = base_data()
    data.user = user
    data.profile = profile

    b = data.instance_eval do
      binding
    end

    out = OpenStruct.new()
    out.content_text = text_template.result(b)
    out.content_html = html_template.result(b)

    out
  end

  def branch_meeting(branch, meeting)
    text_template = ERB.new(File.read(File.join(@template_root, "branch_meeting.text.erb")))
    html_template = ERB.new(File.read(File.join(@template_root, "branch_meeting.html.erb")))

    data = base_data()
    data.branch = branch
    data.meeting = meeting
    data.datetime = DateTime.parse(meeting.decrypt(:datetime))
    data.meeting_notes_html = meeting.decrypt(:notes)
    data.meeting_notes_text = Sanitize.fragment(
      data.meeting_notes_html,
      Sanitize::Config.merge(
        Sanitize::Config::DEFAULT,
        {
          :whitespace_elements => Sanitize::Config::DEFAULT[:whitespace_elements].merge({
            "p" => { :before => "\n", :after => "\n" },
          }),
        }
      )
    )

    link = Addressable::URI.parse(data.base_url)
    link += "/dashboard/meetings/#{branch.id}/#{meeting.id}"
    data.meeting_link = link

    b = data.instance_eval do
      binding
    end

    out = OpenStruct.new()
    out.content_text = text_template.result(b)
    out.content_html = html_template.result(b)

    out
  end

  def supporter_meeting_reminder(meetings)
    text_template = ERB.new(File.read(File.join(@template_root, "supporter_meeting_reminder.text.erb")))
    html_template = ERB.new(File.read(File.join(@template_root, "supporter_meeting_reminder.html.erb")))

    data = base_data()
    data.meetings = meetings

    b = data.instance_eval do
      binding
    end

    out = OpenStruct.new()
    out.content_text = text_template.result(b)
    out.content_html = html_template.result(b)

    out
  end

  def group_new_user(input = {})
    text_template = ERB.new(File.read(File.join(@template_root, "group_new_user.text.erb")))
    html_template = ERB.new(File.read(File.join(@template_root, "group_new_user.html.erb")))

    data = base_data()
    input.each do |k, v|
      data[k] = v
    end

    b = data.instance_eval do
      binding
    end

    out = OpenStruct.new()
    out.content_text = text_template.result(b)
    out.content_html = html_template.result(b)

    out
  end

  def user_changed_branch(user, branch, type)
    text_template = ERB.new(File.read(File.join(@template_root, "user_changed_branch.text.erb")))
    html_template = ERB.new(File.read(File.join(@template_root, "user_changed_branch.html.erb")))

    data = base_data()
    data.user = user
    data.profile = EARMMS::Profile.for_user(user)
    data.branch = branch
    data.type = type

    b = data.instance_eval do
      binding
    end

    out = OpenStruct.new()
    out.content_text = text_template.result(b)
    out.content_html = html_template.result(b)

    out
  end
end
