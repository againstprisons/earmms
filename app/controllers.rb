require File.join(EARMMS.root, 'app', 'controllers', 'application_controller')
require File.join(EARMMS.root, 'app', 'controllers', 'membership_admin_controller')
require File.join(EARMMS.root, 'app', 'controllers', 'system_controller')
require File.join(EARMMS.root, 'app', 'controllers', 'initial_setup_controller')
require File.join(EARMMS.root, 'app', 'controllers', 'api_controller')
Dir.glob(File.join(EARMMS.root, 'app', 'controllers', '*.rb')).each do |f|
  require f
end
