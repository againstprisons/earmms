require File.join(EARMMS.root, 'app', 'helpers', 'application_helpers')
require File.join(EARMMS.root, 'app', 'helpers', 'api_helpers')
Dir.glob(File.join(EARMMS.root, 'app', 'helpers', '*.rb')).each do |f|
  require f
end
