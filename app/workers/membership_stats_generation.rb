module EARMMS::Workers::MembershipStatsGeneration
  def self.queue(data)
    self.queue_at(Time.now, data)
  end

  def self.queue_at(time, data)
    w = EARMMS::WorkQueue.new(:status => 'queued')
    w.save
    w.encrypt(:task, "MembershipStatsGeneration")
    w.encrypt(:data, JSON.dump(data))
    w.encrypt(:created, Time.now)
    w.encrypt(:run_at, time)
    w.save

    w
  end

  def self.perform(log, entryid, data)
    start = Time.now
    log << "MembershipStatsGeneration: starting at #{start.iso8601}"

    today = DateTime.civil(start.year, start.month, start.day, 0, 0, 0)

    overall_members = EARMMS::ProfileFilter.perform_filter(:membership_status, "member").count
    overall_supporters = EARMMS::ProfileFilter.perform_filter(:membership_status, "supporter").count
    log << "MembershipStatsGeneration: overall counts: #{overall_members} member(s), #{overall_supporters} supporter(s)"

    overall = EARMMS::MembershipStats.new(date: today, branch: 0, members: overall_members, supporters: overall_supporters)
    overall.save

    EARMMS::Branch.all.each do |branch|
      bid = branch.id
      members = EARMMS::ProfileFilter.perform_filter(:branch, "#{branch.id.to_s}:member").count
      supporters = EARMMS::ProfileFilter.perform_filter(:branch, "#{branch.id.to_s}:supporter").count

      log << "MembershipStatsGeneration: branch #{bid}: #{members} member(s), #{supporters} supporter(s)"

      brstats = EARMMS::MembershipStats.new(date: today, branch: bid, members: members, supporters: supporters)
      brstats.save
    end

    log << "MembershipStatsGeneration: queueing next run"
    self.queue_at(Time.parse((today + 1).to_s), {})

    finish = Time.now
    log << "MembershipStatsGeneration: done at #{finish.iso8601}, run time #{((finish - start) * 1000).round(3)}ms"
  end
end
