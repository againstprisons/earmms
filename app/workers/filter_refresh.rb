module EARMMS::Workers::FilterRefresh
  class << self
    include EARMMS::FieldHelpers
  end

  def self.queue(data)
    self.queue_at(Time.now, data)
  end

  def self.queue_at(time, data)
    w = EARMMS::WorkQueue.new(:status => 'queued')
    w.save
    w.encrypt(:task, "FilterRefresh")
    w.encrypt(:data, JSON.dump(data))
    w.encrypt(:created, Time.now)
    w.encrypt(:run_at, time)
    w.save

    w
  end

  def self.perform(log, entryid, data)
    start = Time.now
    log << "FilterRefresh: starting at #{start.iso8601}"

    # enable maintenance mode
    log << "FilterRefresh: enabling maintenance mode"
    was_maint = EARMMS::Workers.worker_maintenance_on

    custom_fields = custom_field_desc()

    log << "FilterRefresh: starting refresh of profile filters"
    profile_count = {:started => 0, :ok => 0, :failure => 0}
    EARMMS::Profile.each do |p|
      profile_count[:started] += 1
      log << "FilterRefresh: started profile #{profile_count[:started]}" if (profile_count[:started] % 10) == 0

      begin
        # change branch if nil
        default_branch = EARMMS.app_config['membership-default-branch'].to_i
        unless default_branch.nil? || default_branch.zero?
          if p.branch.nil? || p.decrypt(:branch).empty?
            p.encrypt(:branch, default_branch.to_s)
          end
        end

        # change membership status to supporter if nil
        # XXX: change this for multiple membership statuses?
        membership_status = p.decrypt(:membership_status)&.strip&.downcase
        unless %w[member supporter].include?(membership_status)
          p.encrypt(:membership_status, "supporter")
        end

        # update custom fields
        p_custom_fields = {}
        p_custom_json = p.decrypt(:custom_fields)
        unless p_custom_json.nil? || p_custom_json.empty?
          p_custom_fields = JSON.parse(p_custom_json)
        end

        # set custom fields to defaults if they don't have a value
        custom_fields.each do |cf|
          v = p_custom_fields[cf[:name]]
          if v.nil? || v.empty?
            if cf[:default]
              v = cf[:default]
            end
          end

          p_custom_fields[cf[:name]] = v
        end

        # save custom fields
        p.encrypt(:custom_fields, JSON.generate(p_custom_fields))

        # save and refresh filters
        p.save
        EARMMS::ProfileFilter.clear_filters_for(p)
        EARMMS::ProfileFilter.create_filters_for(p)

        profile_count[:ok] += 1
      rescue => e
        log << "FilterRefresh: profile #{p.id} failed: #{e.inspect}"
        profile_count[:failure] += 1
      end
    end

    log << "FilterRefresh: updated #{profile_count[:ok]} profiles, #{profile_count[:failure]} failed"

    # disable maintenance mode if not enabled at worker start
    if was_maint
      log << "FilterRefresh: Not disabling maintenance mode as it was enabled when worker started"
    else
      log << "FilterRefresh: Disabling maintenance mode"
      EARMMS::Workers.worker_maintenance_off
    end

    finish = Time.now
    log << "FilterRefresh: finished at #{start.iso8601} (run time #{((finish - start) * 1000).round(3)}ms)"
  end
end
