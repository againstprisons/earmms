require 'addressable'
require 'haml'
require 'mail'

module EARMMS::Workers::MembershipChangeReport
  def self.queue(data)
    self.queue_at(Time.now, data)
  end

  def self.queue_at(time, data)
    w = EARMMS::WorkQueue.new(:status => 'queued')
    w.save
    w.encrypt(:task, "MembershipChangeReport")
    w.encrypt(:data, JSON.dump(data))
    w.encrypt(:created, Time.now)
    w.encrypt(:run_at, time)
    w.save

    w
  end

  def self.perform(log, entryid, data)
    start = Time.now
    log << "MembershipChangeReport: starting at #{start.iso8601}"

    requesting_user = EARMMS::User[data['requesting_user'].to_i]
    unless requesting_user
      log << "MembershipChangeReport: invalid requesting user, dying at #{Time.now.iso8601}"
      return
    end

    since = Chronic.parse(data['since'], guess: true)
    unless since
      log << "MembershipChangeReport: can't parse datetime, dying at #{Time.now.iso8601}"
      return
    end

    # Gather users created since the given date
    new_users = EARMMS::User.where{creation >= since}.map do |u|
      p = EARMMS::Profile.for_user(u)
      next nil unless p

      branch = EARMMS::Branch[p.decrypt(:branch).to_i]
      if branch
        branch = {
          branch: branch,
          name: branch.decrypt(:name),
        }
      end

      changelogs = EARMMS::ProfileChangelog.where(profile: p.id).map do |ce|
        begin
          data = JSON.parse(ce.decrypt(:data) || '{}').map{|k, v| [k.to_sym, v]}.to_h
        rescue
          next nil
        end
      
        # Data patches, for entries where they're needed
        case data[:type]
        when 'membership_status'
          data[:from] = t(:'unknown') if data[:from].empty?
          data[:to] = t(:'unknown') if data[:to].empty?
        when 'branch_change'
          data[:from_name] = EARMMS::Branch[data[:from].to_i]&.decrypt(:name) || '(unknown)'
          data[:to_name] = EARMMS::Branch[data[:to].to_i]&.decrypt(:name) || '(unknown)'
        end

        {
          entry: ce,
          type: data[:type],
          creation: ce.created,
          data: data,
        }
      end.compact.sort{|a, b| b[:creation] <=> a[:creation]}

      changelogs_first_to_member = changelogs.select do |cl|
        next false unless cl[:type] == 'membership_status'
        next false unless %w[supporter].include?(cl[:data][:from])
        next false unless %w[member].include?(cl[:data][:to])

        true
      end.first

      edit_link = Addressable::URI.parse(EARMMS.app_config['base-url'])
      edit_link += "/admin/user/edit/#{u.id}"

      {
        user: u,
        profile: p, 
        creation: u.creation,
        name: p.decrypt(:full_name),
        branch: branch,
        status: p.decrypt(:membership_status)&.strip&.downcase,
        changelogs: {
          all: changelogs,
          first_to_member: changelogs_first_to_member,
        },
        edit_link: edit_link.to_s,
      }
    end.compact.sort{|a, b| b[:creation] <=> a[:creation]}

    # Gather changelog entries from the given date
    changelogs = {}
    EARMMS::ProfileChangelog.where{created >= since}.each do |ce|
      p = EARMMS::Profile[ce.profile]
      u = EARMMS::User[p.user]

      begin
        data = JSON.parse(ce.decrypt(:data) || '{}').map{|k, v| [k.to_sym, v]}.to_h
      rescue
        next nil
      end
    
      # Data patches, for entries where they're needed
      case data[:type]
      when 'membership_status'
        data[:from] = t(:'unknown') if data[:from].empty?
        data[:to] = t(:'unknown') if data[:to].empty?
      when 'branch_change'
        data[:from_name] = EARMMS::Branch[data[:from].to_i]&.decrypt(:name) || '(unknown)'
        data[:to_name] = EARMMS::Branch[data[:to].to_i]&.decrypt(:name) || '(unknown)'
      end
      
      edit_link = Addressable::URI.parse(EARMMS.app_config['base-url'])
      edit_link += "/admin/user/edit/#{u.id}"

      changelogs[data[:type]] ||= []
      changelogs[data[:type]] << {
        entry: ce,
        type: data[:type],
        creation: ce.created,
        data: data,

        # User info
        user: {
          user: u,
          profile: p,
          name: p.decrypt(:full_name) || '(unknown)',
          edit_link: edit_link.to_s,
          branch: {
            branch: EARMMS::Branch[p.decrypt(:branch).to_i],
            name: EARMMS::Branch[p.decrypt(:branch).to_i]&.decrypt(:name) || '(unknown)',
          },
        }
      }
    end
    changelogs.keys.each do |type|
      changelogs[type] = changelogs[type].sort{|a, b| b[:creation] <=> a[:creation]}
    end


    # Gather membership stats
    membership_stats = EARMMS::MembershipStats.where{date >= since}.map do |st|
      branch = EARMMS::Branch[st.branch.to_i]

      {
        stat: st,
        creation: st.date,
        mode: (branch.nil?() ? :overall : :branch),
        branch: branch,
        branch_name: branch&.decrypt(:name),
        members: st.members,
        supporters: st.supporters,
      }
    end.sort{|a, b| b[:creation] <=> a[:creation]}

    # Collate membership stats by day
    membership_stats_by_day = {}
    membership_stats.each do |sh|
      membership_stats_by_day[sh[:creation].strftime("%Y-%m-%d")] ||= {branches: [], overall: nil}

      if sh[:mode] == :overall
        membership_stats_by_day[sh[:creation].strftime("%Y-%m-%d")][:overall] = sh

      else
        membership_stats_by_day[sh[:creation].strftime("%Y-%m-%d")][:branches] << sh
      end
    end
    membership_stats_by_day = membership_stats_by_day.map do |day, data|
      {
        day: data[:overall][:creation],
        overall: data[:overall],
        branches: data[:branches].sort{|a, b| a[:branch].id <=> b[:branch].id},
      }
    end.sort{|a, b| a[:day] <=> b[:day]}
    
    # Calculate membership gain/loss over time period
    members_diff = membership_stats_by_day.last[:overall][:members] - membership_stats_by_day.first[:overall][:members]
    supporters_diff = membership_stats_by_day.last[:overall][:supporters] - membership_stats_by_day.first[:overall][:supporters]

    # Get ready to render template
    template_data = {
      # Misc things
      site_name: EARMMS.app_config['site-name'],
      org_name: EARMMS.app_config['org-name'],
      title: "Membership change report for #{EARMMS.app_config['org-name']} - #{since.strftime("%Y-%m-%d")} to #{Time.now.strftime("%Y-%m-%d")}",

      # Timestamps
      generated: Time.now,
      since: since,

      # Membership gain/loss
      membership_diff: {
        days: ((Time.now - since) / 60 / 60 / 24).to_i,
        members: members_diff,
        supporters: supporters_diff,
      },
      
      # Membership stats
      membership_stats: {
        all: membership_stats,
        by_day: membership_stats_by_day,
      },

      # New users
      new_users: new_users,

      # Changelogs
      changelogs: changelogs,
    }

    html_template = Haml::Engine.new(File.read(File.join(EARMMS.root, "app", "views", "workers", "membership_change_report.haml")), {:format => :html5})
    exported_data = html_template.render(Object.new, template_data)

    message_body = (
      "The requested membership report for changes is attached.\n\n" +
      "This report covers the time period from #{ERB::Util.html_escape(since.strftime("%Y-%m-%d"))} " +
      "to #{ERB::Util.html_escape(Time.now.strftime("%Y-%m-%d"))}.\n\n" +
      "--\n#{ERB::Util.html_escape(EARMMS.app_config['site-name'])}"
    )

    qm = EARMMS::EmailQueue.new(creation: Time.now)
    qm.save
    qm.queue_status = 'queued'
    qm.encrypt(:recipients, JSON.generate({"type" => "list", "list" => [requesting_user.email]}))
    qm.encrypt(:subject, EARMMS.email_subject("Membership report"))
    qm.encrypt(:content, message_body)
    qm.encrypt(:attachments, JSON.generate([{"filename" => "membership_change_report_#{start.strftime("%Y-%m-%d_%H-%M-%S")}.html", "content" => exported_data}]))
    qm.save
    log << "MembershipChangeReport: email queued with id #{qm.id}"

    finish = Time.now
    log << "MembershipChangeReport: done at #{finish.iso8601}, run time #{((finish - start) * 1000).round(3)}ms"
  end
end
