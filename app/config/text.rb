module EARMMS::Config::Text
  module_function

  def order
    -10000
  end

  def accept?(_key, type)
    type == :text
  end

  def parse(value)
    value = '' if value == nil
    value = value.dup
    value = value.force_encoding(Encoding::UTF_8)
    value.gsub!("@SITEDIR@", EARMMS.site_dir) if EARMMS.site_dir

    {
      :data => value
    }
  end
end
