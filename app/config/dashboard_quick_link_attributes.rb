module EARMMS::Config::DashboardQuickLinkAttributes
  VALID_STYLES = %w[buttons tiles]

  module_function

  def order
    10000
  end

  def accept?(key, _type)
    key == 'dashboard-quick-links-attributes'
  end

  def parse(value)
    default = EARMMS::APP_CONFIG_ENTRIES["dashboard-quick-links-attributes"][:default]

    unless value.is_a?(Hash)
      return {
        :warning => "Value is not a JSON hash, using default values",
        :data => default,
      }
    end
    
    unless VALID_STYLES.include?(value['style'])
      return {
        :warning => "Invalid link style #{value['style'].inspect}, using default values",
        :data => default,
      }
    end
    
    {
      :data => value,
      :stop_processing_here => true,
    }
  end
end
