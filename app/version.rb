require 'memoist'

module EARMMS
  VERSION = "2.2.0"
  VERSION_UPGRADE_ID = 1

  class << self
    extend Memoist

    def version
      gitrev = `sh -c 'command -v git >/dev/null && git describe --always --tags --abbrev --dirty'`.strip
      if gitrev.nil? || gitrev.empty?
        "v#{VERSION}"
      else
        "v#{VERSION} rev #{gitrev}"
      end
    end

    memoize :version
  end
end
