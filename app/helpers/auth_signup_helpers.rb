module EARMMS::AuthSignupHelpers
  def get_invite(token)
    return nil if token.nil? || token.strip.empty?
    token = token.strip.split(' ').map{|x| x.split('-')}.flatten.join('')

    invite = EARMMS::Token.where(token: token.downcase, use: 'invite').first
    return false unless invite&.is_valid?
    return false unless invite.user.nil?

    invite
  end

  def extended_signup_steps
    [
      {
        status: 'membership',
        url: '/auth/signup/ex/membership',
      },
      {
        status: 'branch',
        url: '/auth/signup/ex/branch',
      },
      {
        status: 'profile',
        url: '/auth/signup/ex/profile',
      },
      {
        status: 'membership-info',
        url: '/auth/signup/ex/membership-info',
      },
      {
        status: 'apply',
        url: '/auth/signup/ex/apply',
      },
      {
        status: 'complete',
        url: '/auth/signup/ex/complete',
      },
    ]
  end
  
  def extended_signup_current_step_url
    extended_signup_steps.each do |step|
      return step[:url] if @user.signup_extended_status == step[:status]
    end

    nil
  end

  def extended_signup_verify!
    ex_status = @user.signup_extended_status
    if ex_status != @current_step
      step = extended_signup_steps().select{|x| x[:status] == ex_status}.first

      unless step
        flash :error, t(:'auth/signup_extended/errors/invalid_step', uid: @user.id, step: ex_status)

        @title = t(:'auth/signup/title')
        @auth_header_hide_home = @auth_header_hide_links = true
        halt haml(:'auth/layout', layout: :layout_minimal) { "" }
      end

      halt redirect step[:url]
    end
  end

  def extended_signup_get_data
    return {} if @user.signup_extended_status_data.nil? || @user.signup_extended_status_data.empty?
    JSON.parse(@user.decrypt(:signup_extended_status_data))
  end
end
