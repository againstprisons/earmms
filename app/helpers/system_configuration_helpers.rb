module EARMMS::SystemConfigurationHelpers
  def validate_cfgkey(key)
    return nil if key.nil? || key&.empty?

    out = /^([a-z0-9-]{5,})$/.match(key)
    return nil unless out
    return nil unless key == out[1]

    key
  end
  
  def get_deprecated(configentries)
    configentries = configentries.map do |e|
      next e.key if e.respond_to?(:key)
      next e if e.is_a?(String)
      nil
    end.compact

    EARMMS::APP_CONFIG_DEPRECATED_ENTRIES.map do |key, info|
      next nil unless configentries.include?(key)
      [key, info]
    end.compact.to_h
  end
end
