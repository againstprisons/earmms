require_relative './branch_helpers'

module EARMMS::MembershipAdminUserEditRolesHelpers
  include EARMMS::BranchHelpers

  def get_available_roles
    roles = [
      {
        :name => t(:'membership/user/roles/role_group/mass_email/title'),
        :disable_cat_if_role => [
          "email:*",
        ],
        :roles => [
          {
            :key => "mass_email_members",
            :multiple => true,
            :role => [
              "email:access",
              "email:members",
            ],
            :desc => t(:'membership/user/roles/role_group/mass_email/members'),
          },
          {
            :key => "mass_email_all_groups",
            :multiple => true,
            :role => [
              "email:access",
              "email:all_groups",
            ],
            :desc => t(:'membership/user/roles/role_group/mass_email/groups'),
          },
        ],
      },
      {
        :name => t(:'membership/user/roles/role_group/membership/title'),
        :warning => t(:'membership/user/roles/role_group/membership/warning'),
        :roles => [
          {
            :key => "membership_full_access",
            :multiple => true,
            :role => [
              "admin:*",
              "email:*",
              "branch:*",
            ],
            :desc => t(:'membership/user/roles/role_group/membership/full_access'),
          },
          {
            :key => "membership_alert_emails",
            :role => "admin:alert_emails",
            :desc => t(:'membership/user/roles/role_group/membership/alert_emails'),
          },
        ],
      },
    ]

    get_branches_data(:ignore_roles => true).reverse.each do |branch|
      desc = {
        :name => t(:'membership/user/roles/role_group/branch/title', :branch => branch[:name]),
        :disable_cat_if_role => [
          "branch:*",
        ],
        :roles => [
          {
            :key => "branch_#{branch[:id]}_full_access",
            :multiple => true,
            :role => [
              "branch:access",
              "branch:#{branch[:id]}:*",
            ],
            :desc => t(:'membership/user/roles/role_group/branch/full_access'),
          },
          {
            :key => "branch_#{branch[:id]}_mass_email",
            :multiple => true,
            :role => [
              "email:access",
              "email:branch:#{branch[:id]}",
            ],
            :desc => t(:'membership/user/roles/role_group/branch/mass_email'),
          },
          {
            :key => "branch_#{branch[:id]}_alert_emails",
            :role => "branch:#{branch[:id]}:alert_emails",
            :desc => t(:'membership/user/roles/role_group/branch/alert_emails'),
          },
        ],
      }

      roles.unshift(desc)
    end

    roles
  end
  
  def filter_cats_by_user_roles(cats, roles)
    cats.map do |cat|
      disable = false
      if cat.key?(:disable_cat_if_role)
        cat[:disable_cat_if_role].each do |dr|
          if roles.include?(dr)
            disable = true
          end
        end
      end
      next nil if disable
      
      cat[:roles] = cat[:roles].map do |roledesc|
        if roledesc[:multiple]
          has = roledesc[:role].map do |r|
            roles.include?(r)
          end
          roledesc[:has] = has.all?
        else
          roledesc[:has] = roles.include?(roledesc[:role])
        end
        
        roledesc
      end
      
      cat
    end.compact
  end
end
