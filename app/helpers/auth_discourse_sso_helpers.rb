require 'openssl'
require 'base64'
require 'cgi'
require 'uri'

module EARMMS::AuthDiscourseSSOHelpers
  require_relative './group_helpers'
  include EARMMS::GroupHelpers

  def discourse_sso_data
    return {'secret' => nil} unless EARMMS.app_config['discourse-sso-enabled']
    EARMMS.app_config['discourse-sso-data']
  end

  def discourse_sso_enabled?
    return false unless EARMMS.app_config['discourse-sso-enabled']
    return false if discourse_sso_data()['secret'].nil?
    return false if discourse_sso_data()['secret'].empty?

    true
  end

  def discourse_secret
    return nil unless discourse_sso_enabled?
    discourse_sso_data()['secret']
  end

  def discourse_allowed_hosts
    return [] unless discourse_sso_enabled?
    discourse_sso_data()['allowed-hosts']
  end

  def discourse_hmac_sign(data)
    return nil unless discourse_sso_enabled?
    OpenSSL::HMAC.hexdigest('SHA256', discourse_secret, data).strip.downcase
  end

  def discourse_hmac_valid?(data, sig)
    return false unless discourse_sso_enabled?

    calculated_hmac = discourse_hmac_sign(data)
    Rack::Utils.secure_compare(calculated_hmac, sig)
  end

  # Checks whether the user has the correct permissions to authenticate to
  # Discourse. The flow is:
  #
  #
  # - if user has role `discourse_sso:login`, immediately return true
  #
  # - if the `:limit_membership_status` config option is set, check whether the
  #   user has one of the permitted membership statuses, if not immediately
  #   return false
  #
  # - return true
  def discourse_user_can_log_in?(opts = {})
    user = current_user
    if opts[:user]
      user = opts[:user]
    else
      return false unless logged_in?
    end

    # immediately allow if user has the right role
    if has_role?('discourse_sso:login', :user => user)
      return true
    end

    # get user's membership status
    user_profile = EARMMS::Profile.for_user(user)
    return false unless user_profile

    # check if the user's membership status is in the allowed list
    # XXX: change this for multiple membership statuses?
    user_membership_status = user_profile.decrypt(:membership_status)&.strip&.downcase
    user_membership_status = nil unless %w[member supporter].include?(user_membership_status.to_s)
    if discourse_sso_data().key?('limit-membership-status')
      allowed_membership_statuses = discourse_sso_data()['limit-membership-status']
      if !(allowed_membership_statuses.include?(user_membership_status))
        return false
      end
    end

    true
  end

  # Gets the list of groups the given user belongs to.
  def discourse_groups_for_user(user)
    return [] unless user
    profile = EARMMS::Profile.for_user(user)
    return [] unless profile

    user_membership_status = profile.decrypt(:membership_status).strip.downcase

    out = []
    discourse_sso_data()['groups'].each do |groupdesc|
      grant = groupdesc['grant-on'].map do |go|
        ok = false

        if go['mode'] == 'membership-status'
          if go['statuses'].include?(user_membership_status)
            ok = true
          end
        elsif go['mode'] == 'role'
          go['roles'].each do |r|
            if has_role?(r, :user => user)
              ok = true
            end
          end
        elsif go['mode'] == 'group'
          group_ids = get_group_ids_user_is_member(user)
          go['groups'].each do |r|
            g = EARMMS::Group[r.to_i]
            if group_ids.include?(g.id)
              ok = true
            end
          end
        end

        ok
      end

      if grant.all?
        out << groupdesc
      end
    end

    out
  end

  # Parses SSO data from the session, returning nil if the data is invalid.
  #
  # This function expects the data to be a hash with keys `:payload` and
  # `:signature`.
  #
  # Returns a hash with keys :nonce and :return_url.
  def discourse_sso_parse_data(data)
    return nil unless data.respond_to?(:[])
    return nil unless data[:payload]
    return nil unless data[:signature]

    payload = data[:payload]
    signature = data[:signature]

    # check hmac
    return nil unless discourse_hmac_valid?(payload, signature)

    # un-base64 the payload
    payload = Base64.decode64(payload)

    # decode query string
    payload = CGI.parse(payload)

    nonce = payload["nonce"].first
    return_url = payload["return_sso_url"].first

    {
      :nonce => nonce,
      :return_url => return_url,
    }
  end

  # Generates an SSO response containing the given user's information.
  #
  # This function returns a hash with keys `:payload` and `:signature`, ready
  # to be added to the callback URL.
  def discourse_sso_generate_response(nonce, opts = {})
    return nil unless nonce

    user = current_user
    if opts[:user]
      user = opts[:user]
    end

    return nil unless user
    profile = EARMMS::Profile.for_user(user)
    return nil unless profile

    is_admin = false
    if discourse_sso_data()['admin-roles']
      discourse_sso_data()['admin-roles'].each do |r|
        if has_role?(r, :user => user)
          is_admin = true
        end
      end
    end

    groups = discourse_sso_data()['groups']
    user_groups = discourse_groups_for_user(user)

    payload = {
      "nonce" => nonce,
      "email" => user.email,
      "external_id" => user.id.to_s,
      "name" => profile.decrypt(:full_name),
      "admin" => is_admin,
      "add_groups" => user_groups.map{|x| x['name']}.join(','),
      "remove_groups" => groups.reject{|x| user_groups.include?(x)}.map{|x| x['name']}.join(','),
    }

    payload_encoded = URI.encode_www_form(payload)
    payload_encoded = Base64.encode64(payload_encoded)

    # generate hmac
    signature = discourse_hmac_sign(payload_encoded)

    {
      :payload => payload_encoded,
      :signature => signature,
    }
  end
end
