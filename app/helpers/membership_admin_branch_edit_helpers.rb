module EARMMS::MembershipAdminBranchEditHelpers
  def error_page(msg)
    flash :error, msg

    @title = "Branch #{@branch.decrypt(:name)} (id #{@branch.id})"
    haml :'membership_admin/branch/edit'
  end
end
