module EARMMS::UserHelpers
  def current_token
    EARMMS::Token.where(token: session[:token], use: 'session').first
  end

  def logged_in?
    current_token()&.is_valid?() || false
  end

  def current_user
    return nil unless logged_in?
    EARMMS::User[current_token().user]
  end

  def has_role?(role, opts = {})
    user = current_user
    if opts[:user]
      user = opts[:user]
    else
      return false unless logged_in?
    end

    parts = role.split(':')
    roleparts = user.get_roles.map do |r|
      r[:role].split(':')
    end

    roleparts.each do |rp|
      skip = false
      oksofar = true

      rp.each_index do |rpi|
        next if skip

        if oksofar && rp[rpi] == '*'
          return true
        elsif rp[rpi] != parts[rpi]
          oksofar = false
          skip = true
        end
      end

      return true if oksofar
    end

    false
  end

  def is_group_admin?(opts = {})
    user = current_user
    if opts[:user]
      user = opts[:user]
    else
      return false unless logged_in?
    end

    user = user.id if user.respond_to?(:id)

    EARMMS::GroupMemberFilter.perform_filter(:user, user.to_s).each do |cmf|
      cm = EARMMS::GroupMember[cmf.group_member]
      next unless cm

      roles = cm.decrypt(:roles).split(",")

      if roles.include?("admin")
        return true
      end

      if opts[:include_email_privs] && roles.include?("email")
        return true
      end
    end

    false
  end
end
