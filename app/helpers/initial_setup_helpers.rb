module EARMMS::InitialSetupHelpers
  require_relative './initial_setup_upgrade_helpers'
  include EARMMS::InitialSetupUpgradeHelpers
  require_relative './initial_setup_new_install_helpers'
  include EARMMS::InitialSetupNewInstallHelpers

  def get_config_value(key)
    cfg = EARMMS::ConfigModel.where(key: key).first
    return nil unless cfg
    cfg.value || ''
  end

  def this_upgrade_steps
    case @this_upgrade.mode
    when 'upgrade'
      upgrade_steps
    when 'newinstall'
      newinstall_steps
    else
      []
    end
  end

  def current_step_can_discard
    this_upgrade_steps.each do |step|
      if @this_upgrade.status == step[:status]
        return step[:can_discard]
      end
    end

    false
  end

  def current_step_url
    this_upgrade_steps.each do |step|
      if @this_upgrade.status == step[:status]
        return step[:path]
      end
    end

    nil
  end
end
