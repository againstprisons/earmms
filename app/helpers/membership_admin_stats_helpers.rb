module EARMMS::MembershipAdminStatsHelpers
  def stats_from_date(since)
    membership_stats = EARMMS::MembershipStats.where{date >= since}.map do |st|
      branch = EARMMS::Branch[st.branch.to_i]

      {
        stat: st,
        creation: st.date,
        mode: (branch.nil?() ? :overall : :branch),
        branch: branch,
        branch_name: branch&.decrypt(:name),
        members: st.members,
        supporters: st.supporters,
      }
    end.sort{|a, b| b[:creation] <=> a[:creation]}

    # Collate membership stats by day
    membership_stats_by_day = {}
    membership_stats.each do |sh|
      membership_stats_by_day[sh[:creation].strftime("%Y-%m-%d")] ||= {branches: [], overall: nil}

      if sh[:mode] == :overall
        membership_stats_by_day[sh[:creation].strftime("%Y-%m-%d")][:overall] = sh

      else
        membership_stats_by_day[sh[:creation].strftime("%Y-%m-%d")][:branches] << sh
      end
    end
    membership_stats_by_day = membership_stats_by_day.map do |day, data|
      branches = data[:branches].map do |br|
        [br[:branch].id.to_s, br]
      end.to_h

      {
        day: data[:overall][:creation],
        overall: data[:overall],
        branches: branches,
      }
    end.sort{|a, b| a[:day] <=> b[:day]}

    # Calculate membership gain/loss over time period
    members_diff = membership_stats_by_day.last[:overall][:members] - membership_stats_by_day.first[:overall][:members]
    supporters_diff = membership_stats_by_day.last[:overall][:supporters] - membership_stats_by_day.first[:overall][:supporters]

    {
      since: since,
      by_day: membership_stats_by_day,
      diff: {
        members: members_diff,
        supporters: supporters_diff,
      }
    }
  end
end