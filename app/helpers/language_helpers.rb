require 'erb'

module EARMMS::LanguageHelpers
  # This class exists to let translated text call t()
  class LanguageData < OpenStruct
    include EARMMS::LanguageHelpers

    def e(text)
      ERB::Util.html_escape(text)
    end
  end

  def languages
    EARMMS.languages
  end

  def current_language
    if defined?(session)
      return session[:lang] if session.key?(:lang)
    end

    EARMMS.default_language
  end

  def t(translation_key, values = {})
    language = current_language
    language = values.delete(:force_language) if values[:force_language]
    return translation_key.to_s  if language == "translationkeys"

    text = EARMMS.languages[language]&.fetch(translation_key, nil)
    text = EARMMS.languages[EARMMS.default_language].fetch(translation_key, nil) unless text

    unless text
      return "##MISSING(#{translation_key.to_s})##"
    end

    data = LanguageData.new(values)
    data.site_name = EARMMS.app_config['site-name']
    data.org_name = EARMMS.app_config['org-name']
    b = data.instance_eval do
      binding
    end

    erb = ERB.new(text)
    erb.result(b)
  end
end
