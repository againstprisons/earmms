module EARMMS::FieldHelpers
  include EARMMS::LanguageHelpers

  def field_desc(opts = {})
    fields = [
      {
        :name => "full_name",
        :friendly => t(:full_name),
        :type => "text",
        :required => true,
        :is_custom => false,
      },
      {
        :name => "birthday",
        :friendly => t(:birthday),
        :type => "date",
        :required => false,
        :is_custom => false,
      },
      {
        :name => "phone_home",
        :friendly => t(:phone_home),
        :type => "text",
        :required => false,
        :is_custom => false,
      },
      {
        :name => "phone_mobile",
        :friendly => t(:phone_mobile),
        :type => "text",
        :required => false,
        :is_custom => false,
      },
      {
        :name => "address",
        :friendly => t(:address),
        :type => "textarea",
        :required => false,
        :is_custom => false,
      },
      {
        :name => "prisoner_number",
        :friendly => t(:prisoner_number),
        :type => "text",
        :required => false,
        :display_in_extended_signup => false,
        :display_in_profile_edit => false,
        :is_custom => false,
      },

      # Include custom fields
      custom_field_desc(:data => opts[:custom_blob]),
    ].flatten

    names_in_order = fields.map{|x| x[:name]}

    adjust_blob = EARMMS.app_config['field-adjustments']
    adjust_blob = opts[:adjust_blob] if opts.key?(:adjust_blob)
    adjust_blob.each do |adj|
      to_adj = fields.compact.select{|x| x[:name].to_s == adj['name'].to_s}
      to_adj.each do |desc|
        descidx = names_in_order.find_index(desc[:name])
        next unless descidx

        desc[:adjusted] = true

        newfriendly = adj['friendly']
        desc[:friendly] = newfriendly if newfriendly

        newrequired = adj['required']
        desc[:required] = newrequired unless newrequired.nil?

        doi = adj['display-only-if']
        if doi.respond_to?(:key?)
          desc[:display_only_if] = []
          if doi.key?('other-fields')
            desc[:display_only_if] = doi['other-fields']
          end

          desc[:display_only_if_membership_status] = []
          if doi.key?('membership-status')
            desc[:display_only_if_membership_status] = doi['membership-status']
          end

          desc[:display_in_profile_edit] = true
          if doi.key?('profile-edit')
            desc[:display_in_profile_edit] = doi['profile-edit']
          end

          desc[:display_in_extended_signup] = true
          if doi.key?('extended-signup')
            desc[:display_in_extended_signup] = doi['extended-signup']
          end
        end

        # Allow completely removing the field
        if adj['remove']
          desc = nil
        end

        fields[descidx] = desc
      end
    end

    fields.compact
  end

  def custom_field_desc(opts = {})
    field_blob = EARMMS.app_config['field-custom-entries']
    field_blob = opts[:data] if opts[:data]

    return [] if field_blob.nil?
    return [] if field_blob.empty?

    field_blob.map do |field|
      desc = {
        :name => field['name'],
        :friendly => field['friendly'],
        :type => (field['type'] || 'text'),
        :required => field['required'] || false,
        :default => field['default'] || nil,
        :is_custom => true,
      }

      if desc[:type] == 'select'
        desc[:select_options] = field['select-options'].map do |opt|
          {
            :name => opt['name'],
            :friendly => opt['friendly'],
          }
        end
      end

      doi = field['display-only-if']
      if doi.respond_to?(:key?)
        desc[:display_only_if] = []
        if doi.key?('other-fields')
          desc[:display_only_if] = doi['other-fields']
        end

        desc[:display_only_if_membership_status] = []
        if doi.key?('membership-status')
          desc[:display_only_if_membership_status] = doi['membership-status']
        end

        desc[:display_in_profile_edit] = true
        if doi.key?('profile-edit')
          desc[:display_in_profile_edit] = doi['profile-edit']
        end

        desc[:display_in_extended_signup] = true
        if doi.key?('extended-signup')
          desc[:display_in_extended_signup] = doi['extended-signup']
        end
      end

      desc
    end
  end

  def custom_field_default_json
    data = custom_field_desc.map do |field|
      [field[:name], field[:default] || nil]
    end.to_h

    JSON.generate(data)
  end
end
