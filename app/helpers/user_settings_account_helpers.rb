module EARMMS::UserSettingsAccountHelpers
  def error_page(message)
    flash :error, message
    return haml :'user_settings/account'
  end
end
