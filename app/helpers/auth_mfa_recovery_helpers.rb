module EARMMS::AuthMfaRecoveryHelpers
  def user_has_mfa_recovery?(user)
    user = user.id if user.respond_to?(:id)
    EARMMS::Token.where(user: user, use: 'mfa_recovery').count.positive?
  end

  def generate_mfa_recovery!(user)
    user = user.id if user.respond_to?(:id)
    EARMMS::Token.where(user: user, use: 'mfa_recovery').delete

    codes = 8.times.map do
      t = EARMMS::Token.generate_short
      t.user = user
      t.use = 'mfa_recovery'
      t.save
    end
  end
end
