# frozen_string_literal: true

module EARMMS::IdentHelpers
  ALPHABET = (('A'..'Z').to_a + ('2'..'7').to_a)

  def to_ident(input)
    output = []
    input = input.to_i

    while input > 0
      output << ALPHABET[input % ALPHABET.length]
      input = input / ALPHABET.length
    end
    
    output.reverse!
    output.unshift('0') while output.length < 2
    output.join
  end
  
  def from_ident(input)
    output = 0
    input = input
      .strip
      .upcase
      .split('')
      .reject { |e| ['0'].include?(e) }
      .map { |e| ALPHABET.index(e) }

    input.each do |alpha_idx|
      output = (output * ALPHABET.length) + alpha_idx
    end
    
    output
  end
end
