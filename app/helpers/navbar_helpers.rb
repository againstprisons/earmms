module EARMMS::NavbarHelpers
  def site_navbar_entries
    out = []

    unless logged_in?
      # If not logged in, we don't have anything to display in the navbar, as
      # the authentication options are now in `auth/header.haml`

      return []
    end
    
    # Dashboard
    out << {
      link: "/dashboard",
      friendly: t(:'dashboard/title'),
      selected: current_prefix?("/dashboard"),
    }

    # User profile
    out << {
      link: "/user",
      friendly: t(:'userprofile/title'),
      selected: current_prefix?("/user") || (EARMMS.app_config['group-user-panel-user-settings-integration'] && current_prefix?("/ugroups")),
    }

    # Groups
    unless EARMMS.app_config['group-user-panel-user-settings-integration']
      out << {
        link: "/ugroups",
        friendly: t(:'usergroups/title'),
        selected: current_prefix?("/ugroups"),
      }
    end

    # Mass email
    if has_role?("email:access") || is_group_admin?(include_email_privs: true)
      out << {
        link: "/massemail",
        friendly: t(:'massemail/title'),
        selected: current_prefix?("/massemail"),
      }
    end

    # Group admin
    if is_group_admin?
      out << {
        link: "/group",
        friendly: t(:'groupadmin/title'),
        selected: current_prefix?("/group"),
      }
    end

    # Branch admin
    if has_role?("branch:access")
      out << {
        link: "/branch",
        friendly: t(:'branch/title'),
        selected: current_prefix?("/branch"),
      }
    end

    # Membership admin
    if has_role?("admin:access")
      out << {
        link: "/admin",
        friendly: t(:'membership/title'),
        selected: current_prefix?("/admin"),
      }
    end

    # System admin
    if has_role?("system:access")
      out << {
        link: "/system",
        friendly: t(:'system/title'),
        selected: current_prefix?("/system"),
      }
    end

    out
  end
end
