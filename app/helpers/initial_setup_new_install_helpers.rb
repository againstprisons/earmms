require 'json'

module EARMMS::InitialSetupNewInstallHelpers
  def newinstall_steps
    [
      {
        :status => 'not-started',
        :path => '/initialsetup/newinstall',
        :friendly => t(:'initial_setup/newinstall/steps/basecfg'),
        :can_discard => true,
      },
      {
        :status => 'base-config-ok',
        :path => '/initialsetup/newinstall/create-branch',
        :friendly => t(:'initial_setup/newinstall/steps/create_branch'),
        :can_discard => true,
      },
      {
        :status => 'branch-ok',
        :path => '/initialsetup/newinstall/kaupapa',
        :friendly => t(:'initial_setup/newinstall/steps/kaupapa'),
        :can_discard => true,
      },
      {
        :status => 'kaupapa-ok',
        :path => '/initialsetup/newinstall/meeting-reminders',
        :friendly => t(:'initial_setup/newinstall/steps/meeting_reminders'),
        :can_discard => true,
      },
      {
        :status => 'meeting-reminders-ok',
        :path => '/initialsetup/newinstall/background-tasks',
        :friendly => t(:'initial_setup/newinstall/steps/background_tasks'),
        :can_discard => true,
      },
      {
        :status => 'background-tasks-ok',
        :path => '/initialsetup/newinstall/two-factor',
        :friendly => t(:'initial_setup/newinstall/steps/two_factor'),
        :can_discard => true,
      },
      {
        :status => 'two-factor-ok',
        :path => '/initialsetup/newinstall/roles',
        :friendly => t(:'initial_setup/newinstall/steps/roles'),
        :can_discard => true,
      },
      {
        :status => 'roles-ok',
        :path => '/initialsetup/newinstall/verify',
        :friendly => t(:'initial_setup/newinstall/steps/verify'),
        :can_discard => true,
      },
      {
        :status => 'verify-ok',
        :path => '/initialsetup/newinstall/apply',
        :friendly => t(:'initial_setup/newinstall/steps/apply'),
        :can_discard => true,
      },
      {
        :status => 'apply-ok',
        :path => '/initialsetup/newinstall/complete',
        :friendly => t(:'initial_setup/newinstall/steps/complete'),
        :can_discard => false,
      },
    ]
  end

  def newinstall_cfg_entries(inputentries)
    out = EARMMS::APP_CONFIG_ENTRIES.map do |key, desc|
      value = desc[:default]
      source = :default

      if inputentries.key?(key)
        value = inputentries[key]
        source = :wizard
      end

      if desc[:type] == :bool
        value = (value ? 'yes' : 'no')
      end
      
      {
        :key => key,
        :value => value,
        :type => desc[:type],
        :source => source,
        :change => :new,
      }
    end

    {
      :from_wizard => out.select{|e| e[:source] == :wizard},
      :defaults => out.reject{|e| e[:source] == :wizard},
    }
  end
end
