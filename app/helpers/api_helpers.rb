module EARMMS::ApiHelpers
  def api_token?
    token = request.params['token']&.strip&.downcase
    token = EARMMS::Token.where(use: 'apitoken', token: token, valid: true).first

    return nil unless token && token.is_valid?
    token
  end
end
