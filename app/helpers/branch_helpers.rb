module EARMMS::BranchHelpers
  def get_branches_data(opts = {})
    branches = EARMMS::Branch.all.map do |b|
      unless opts[:ignore_roles] || has_role?("branch:#{b.id}")
        next nil
      end

      # XXX: change this for multiple membership statuses?
      counts = %w[member supporter].map do |status|
        c = EARMMS::ProfileFilter.perform_filter(:branch, "#{b.id.to_s}:#{status}").count
        [status, c]
      end.to_h

      [
        b.id.to_s,
        {
          :id => b.id,
          :name => b.decrypt(:name),
          :counts => counts,
        }
      ]
    end.compact.to_h

    default = nil
    if EARMMS.app_config['membership-default-branch']
      default = branches.delete(EARMMS.app_config['membership-default-branch'].to_s)
      if default
        default[:is_default] = true
      end
    end

    [default, branches.values.sort{|a, b| a[:id] <=> b[:id]}].flatten.compact
  end
end
