module EARMMS::MembershipAdminUserEditHelpers
  def alert_branch_admin_user_change(profile, oldbranchid, newbranchid)
    return unless profile
    user = EARMMS::User[profile.user]
    return unless user

    # Send alert to admins of old branch saying user is leaving
    if !oldbranchid.zero?
      oldbranch = EARMMS::Branch[oldbranchid]
      if oldbranch
        text = EARMMS.email_templates.user_changed_branch(user, oldbranch, :leave)
        alert_branch_admin_send_email(oldbranch, text)
      end
    end

    # Send alert to admins of new branch saying user is joining
    if !newbranchid.zero?
      newbranch = EARMMS::Branch[newbranchid]
      if newbranch
        text = EARMMS.email_templates.user_changed_branch(user, newbranch, :join)
        alert_branch_admin_send_email(newbranch, text)
      end
    end
  end

  def alert_branch_admin_send_email(branch, text)
    if branch.respond_to?(:id)
      branch = branch.id
    end

    to_addresses = EARMMS::UserHasRole.where(role: "branch:#{branch}:alert_emails").map do |uhr|
      user = EARMMS::User[uhr.user]
      next nil unless user

      user.email
    end.compact

    if !to_addresses.empty?
      qm = EARMMS::EmailQueue.new(creation: Time.now)
      qm.save
      qm.queue_status = 'queued'
      qm.encrypt(:recipients, JSON.generate({"type" => "list", "list" => to_addresses}))
      qm.encrypt(:subject, EARMMS.email_subject("User changed branch"))
      qm.encrypt(:content, text.content_text)
      qm.encrypt(:content_html, text.content_html)
      qm.save
    end
  end
end
