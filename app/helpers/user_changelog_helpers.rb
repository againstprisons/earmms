module EARMMS::UserChangelogHelpers
  def changelog_entries_for_profile(profile)
    profile = profile.id if profile.respond_to?(:id)

    EARMMS::ProfileChangelog.where(profile: profile).map do |entry|
      data = entry.decrypt(:data)
      next nil unless data
      data = JSON.parse(data).map{|k, v| [k.to_sym, v]}.to_h
        
      out = {
        :id => entry.id,
        :type => data["type"],
        :created => entry.created,
        :data => data,
        :friendly => {
          :type_tl => "changelog/#{data[:type]}".to_sym,
          :message_tl => "changelog/#{data[:type]}/message".to_sym,
        },
      }

      # Data patches, for entries where they're needed
      case data[:type]
      when 'membership_status'
        out[:data][:from] = t(:'unknown') if out[:data][:from]&.empty?
        out[:data][:to] = t(:'unknown') if out[:data][:to]&.empty?
      when 'branch_change'
        out[:data][:from_name] = EARMMS::Branch[data[:from].to_i]&.decrypt(:name) || t(:'unknown')
        out[:data][:to_name] = EARMMS::Branch[data[:to].to_i]&.decrypt(:name) || t(:'unknown')
      end

      out
    end.compact.sort{|a, b| b[:created] <=> a[:created]}
  end
end
