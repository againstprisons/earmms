require 'oauth2'

DISCORD_API_BASE = "https://discord.com/api/v6"
DISCORD_OAUTH_SCOPES = "identify guilds.join"

class EARMMS::AuthDiscordGuildJoinController < EARMMS::ApplicationController
  before do
    @auth_layout_classes = %w[auth-layout-small auth-layout-centered]
    next halt 404 unless EARMMS.app_config['discord-enabled']
    
    unless logged_in?
      flash(:warning, t(:'auth/login/must_log_in'))
      session[:after_login] = '/auth/discord/join'
      next redirect '/auth'
    end

    @user = current_user()
    @profile = EARMMS::Profile.for_user(@user)
    @status = @profile.decrypt(:membership_status)&.strip&.downcase
    @user_name = @profile.decrypt(:full_name) || '(unknown)'
    @branch_name = EARMMS::Branch[@profile.decrypt(:branch).to_i]&.decrypt(:name) || '(unknown)'

    @discord_guild_link = "https://discord.com/channels/#{EARMMS.app_config['discord-guild-id']}"
    @back_to_us = (
      Addressable::URI.parse(EARMMS.app_config['base-url']) +
        '/auth/discord/join/callback'
    )

    @oauth = OAuth2::Client.new(
      EARMMS.app_config['discord-client-id'],
      EARMMS.app_config['discord-client-secret'],
      site: 'https://discord.com',
      authorize_url: 'https://discord.com/api/oauth2/authorize',
      token_url: 'https://discord.com/api/oauth2/token',
    )
  end

  get '/' do
    authorize_uri = @oauth.auth_code.authorize_url(
      redirect_uri: @back_to_us,
      'scope' => DISCORD_OAUTH_SCOPES,
      'prompt' => 'consent',
    )

    next redirect authorize_uri
  end

  get '/callback' do
    code = request.params['code']&.strip
    code = nil if code&.empty?
    next halt 404 unless code

    # get access token
    @token = @oauth.auth_code.get_token(code, redirect_uri: @back_to_us, 'scope' => DISCORD_OAUTH_SCOPES)
    next halt 404 unless @token

    # collect roles to assign
    @role_ids = EARMMS.app_config['discord-assign-roles'].map do |rt|
      case rt["on"]
      when 'membership_status'
        statuses = [rt['status']].flatten
        next nil unless statuses.include?(@status)
      else
        next nil
      end

      rt['role_id']
    end.compact.uniq

    # request discord user information (to get the user id)
    begin
      user_uri = DISCORD_API_BASE + "/users/@me"
      @user_response = @token.get(user_uri)&.parsed
      next halt 404 unless @user_response
      @discord_user_id = @user_response["id"]
      @discord_user = "#{@user_response["username"]}##{@user_response["discriminator"]}"

    rescue => e
      $stderr.puts "===== Discord user get failure ===="
      $stderr.puts e.inspect
      $stderr.puts e.response.inspect if e.respond_to?(:response)
      $stderr.flush

      @title = t(:'auth/discord_join/failure/title')
      next haml :'auth/layout', layout: :layout_minimal do
        haml :'auth/discord_join/failure', layout: false
      end
    end

    # join the guild
    begin
      join_uri = (
        DISCORD_API_BASE +
          "/guilds/#{EARMMS.app_config['discord-guild-id']}" +
          "/members/#{@discord_user_id}"
      )

      join_body = JSON.generate({
        access_token: @token.token,
        roles: @role_ids,
      })

      headers = { 'Content-Type' => 'application/json' }
      @join_response = @token.put(join_uri, body: join_body, headers: headers) do |req|
        req.headers['Authorization'] = "Bot #{EARMMS.app_config['discord-bot-token']}"
      end

    rescue => e
      $stderr.puts "===== Discord guild join failure ===="
      $stderr.puts e.inspect
      $stderr.puts e.response.inspect if e.respond_to?(:response)
      $stderr.flush

      @title = t(:'auth/discord_join/failure/title')
      next haml :'auth/layout', layout: :layout_minimal do
        haml :'auth/discord_join/failure', layout: false
      end
    end
    
    # if notify is enabled, send a notify message
    if EARMMS.app_config['discord-notify-enabled']
      message_content = (
        "Discord user **#{@discord_user}** (ID `#{@discord_user_id}`) " +
          "verified as **#{@user_name}** (EARMMS ID `#{@user.id}`) - " +
          "membership status: _#{@status}_, branch: _#{@branch_name}_"
      )

      begin
        message_uri = (
          DISCORD_API_BASE +
            "/channels/#{EARMMS.app_config['discord-notify-channel-id']}" +
            "/messages"
        )

        message_body = JSON.generate({
          content: message_content,
        })

        headers = { 'Content-Type' => 'application/json' }
        @message_response = @token.post(message_uri, body: message_body, headers: headers) do |req|
          req.headers['Authorization'] = "Bot #{EARMMS.app_config['discord-bot-token']}"
        end

      rescue => e
        $stderr.puts "===== Discord notify failure ===="
        $stderr.puts e.inspect
        $stderr.puts e.response.inspect if e.respond_to?(:response)
        $stderr.flush
      end
    end

    @title = t(:'auth/discord_join/success/title')
    haml :'auth/layout', layout: :layout_minimal do
      haml :'auth/discord_join/success', layout: false
    end
  end
end
