require 'rotp'
require 'rqrcode'
require 'base64'

class EARMMS::UserSettingsMfaTotpController < EARMMS::ApplicationController
  before do
    @auth_header_hide_home = @auth_header_hide_links = true
    @auth_layout_classes = %w[auth-layout-large auth-layout-centered]

    unless logged_in?
      flash(:error, t(:'auth/login/must_log_in'))
      session[:after_login] = request.path
      next redirect '/auth'
    end

    @user = current_user
    @profile = EARMMS::Profile.for_user(@user)
    @has_totp = @user.totp_enabled

    if @enhanced = @user.enhanced_security
      flash :error, t(:'userprofile/mfa/totp/enhanced')
      next redirect '/user/mfa'
    end

    @secret = session[:totp_secret] = (session[:totp_secret] || ROTP::Base32.random_base32)
    @totp = ROTP::TOTP.new(@secret, issuer: EARMMS.app_config['site-name'])
  end

  helpers do
    def generate_qr
      url = @totp.provisioning_uri(@user.email)
      qr = RQRCode::QRCode.new(url)
      svg = qr.as_svg({
        offset: 0,
        color: '000',
        shape_rendering: 'crispEdges',
        module_size: 6,
        standalone: true,
      })

      "data:image/svg+xml;base64,#{Base64.encode64(svg)}"
    end
  end

  get '/' do
    @title = t(:'userprofile/mfa/totp/setup/title')
    @qrcode = generate_qr()

    haml :'auth/layout', :layout => :layout_minimal do
      haml :'user_settings/mfa/totp_setup', :layout => false
    end
  end

  post '/' do
    # Allow cancelling
    if request.params['cancel'].to_i == 1
      session.delete(:totp_secret)
      next redirect '/user/mfa'
    end

    code = request.params['code']&.strip&.downcase
    unless code
      flash :error, t(:'userprofile/mfa/totp/setup/errors/invalid_code')
      next redirect request.path
    end

    unless @totp.verify(code, :drift_behind => 15)
      flash :error, t(:'userprofile/mfa/totp/setup/errors/invalid_code')
      next redirect request.path
    end

    # Delete secret from session
    session.delete(:totp_secret)

    # Save secret
    @user.encrypt(:totp_secret, @secret)
    @user.totp_enabled = true
    @user.save

    # Invalidate tokens
    @user.invalidate_tokens(session[:token])

    flash :success, t(:'userprofile/mfa/totp/setup/success')
    next redirect '/user/mfa'
  end
end
