class EARMMS::MembershipAdminBranchMeetingsController < EARMMS::MembershipAdminController
  before do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:branch'
  end

  get '/:branchid' do |branchid|
    @branch = EARMMS::Branch[branchid.to_i]
    next halt 404 unless @branch
    @branch_name = @branch.decrypt(:name)

    @meetings = EARMMS::BranchMeeting.where(branch: @branch.id).map do |m|
      {
        :id => m.id,
        :datetime => Time.parse(m.decrypt(:datetime)),
        :location => m.decrypt(:location),
        :notes => m.decrypt(:notes),
      }
    end.sort{|a, b| b[:datetime] <=> a[:datetime]}

    @title = t(:'membership/branch/meetings/title', :branch => @branch_name)
    haml :'membership_admin/branch/meetings/list'
  end

  get '/:branchid/:meetingid' do |branchid, meetingid|
    @branch = EARMMS::Branch[branchid.to_i]
    next halt 404 unless @branch
    @branch_name = @branch.decrypt(:name)

    @meeting = EARMMS::BranchMeeting[meetingid.to_i]
    next halt 404 unless @meeting
    next halt 404 unless @meeting.branch == @branch.id

    @meeting = {
      :meeting => @meeting,
      :id => @meeting.id,
      :datetime => Time.parse(@meeting.decrypt(:datetime)),
      :location => @meeting.decrypt(:location),
      :notes => @meeting.decrypt(:notes),
    }

    @title = t(:'membership/branch/meetings/view/title', :branch => @branch_name, :datetime => @meeting[:datetime])
    haml :'membership_admin/branch/meetings/meeting'
  end
end
