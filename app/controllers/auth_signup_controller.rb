require 'email_validator'

class EARMMS::AuthSignupController < EARMMS::ApplicationController
  helpers EARMMS::AuthSignupHelpers

  before do
    next redirect "/" if logged_in?

    @auth_layout_classes = %w[auth-layout-small auth-layout-centered]
    @title = t(:'auth/signup/title')
  end

  get '/' do
    @invite = get_invite(request.params['invite'])
    unless @invite || EARMMS.app_config['signups']
      if @invite == false
        flash :error, t(:'auth/signup/invite/errors/invalid_invite')
      end

      @auth_layout_classes = %w[auth-layout-small auth-layout-centered]
      next haml :'auth/layout', :layout => :layout_minimal do
        haml :'auth/signup/disabled', :layout => false
      end
    end

    haml :'auth/layout', :layout => :layout_minimal do
      haml :'auth/signup/index', :layout => false
    end
  end

  post '/' do
    next redirect "/" if logged_in?

    @invite = get_invite(request.params["invite"])
    unless @invite
      next haml :'auth/signup/disabled' unless EARMMS.app_config['signups']
    end

    email = request.params["email"]&.strip&.downcase
    if email.nil? || email.empty? || !(EmailValidator.valid?(email))
      flash :error, t(:'auth/signup/errors/invalid_email')
      next redirect request.path
    end

    pw = request.params['password']&.strip
    pw_rep = request.params['password-repeat']&.strip
    if pw.nil? || pw.empty? || pw_rep.nil? || pw_rep.empty?
      flash :error, t(:'auth/signup/errors/no_password')
      next redirect request.path
    end

    unless Rack::Utils.secure_compare(pw, pw_rep)
      flash :error, t(:'auth/signup/errors/passwords_dont_match')
      next redirect request.path
    end

    if EARMMS::User.where(email: email).count.positive?
      flash :error, t(:'auth/signup/errors/already_exists')
      next redirect request.path
    end

    user = EARMMS::User.new
    user.email = request.params["email"].downcase
    user.password = request.params["password"]
    user.creation = Time.now
    user.last_login = Time.now
    
    # Extended status
    user.signup_extended_status = "complete"
    if EARMMS.app_config['extended-signup-enabled']
      user.signup_extended_status = extended_signup_steps().first[:status]
    end

    user.save

    if @invite
      @invite.user = user.id
      @invite.invalidate!
      @invite.save
      # roles
      data = JSON.parse(@invite.extra_data || '{}')
      if data.key?("roles") && data["roles"] != nil
        data["roles"].each do |r|
          uhr = EARMMS::UserHasRole.new(user: user.id, role: r)
          uhr.save
        end
      end
    end

    profile = EARMMS::Profile.new(user: user.id)
    profile.save
    profile.encrypt(:branch, EARMMS.app_config['membership-default-branch'].to_s)
    profile.encrypt(:membership_status, "supporter")
    profile.encrypt(:custom_fields, custom_field_default_json)
    profile.save

    token = EARMMS::Token.generate_session(user)
    session[:token] = token.token
    token.save

    EARMMS::ProfileFilter.clear_filters_for(profile)
    EARMMS::ProfileFilter.create_filters_for(profile)

    # Send verification email
    user.send_email_verification!

    if EARMMS.app_config['extended-signup-enabled']
      next redirect "/auth/signup/ex/#{extended_signup_steps().first[:status]}"
    end

    flash(:success, t(:'auth/signup/success'))
    next redirect '/'
  end
end
