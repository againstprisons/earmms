class EARMMS::InitialSetupNewInstallKaupapaController < EARMMS::InitialSetupController
  get '/' do
    haml :'initial_setup/layout', :layout => :layout_minimal do
      haml :'initial_setup/newinstall/layout', :layout => false do
        haml :'initial_setup/newinstall/kaupapa', :layout => false
      end
    end
  end

  post '/' do
    @text = nil
    present = request.params['present']&.strip&.downcase == 'yes'
    if present
      @text = request.params['text']&.strip
      @text = Sanitize.fragment(@text, Sanitize::Config::RELAXED) if @text
      fragment = Sanitize.fragment(@text, Sanitize::Config::RESTRICTED).gsub("&nbsp;", "").strip
      if @text.nil? || @text&.empty? || fragment.empty?
        flash :error, t(:'initial_setup/newinstall/kaupapa/errors/no_text')
        return redirect request.path
      end
    end

    # Parse upgrade data, add kaupapa prompt to it
    @data = JSON.parse(@this_upgrade.decrypt(:data))
    @data['config-entries'] ||= {}
    @data['config-entries']['membership-kaupapa-prompt'] = @text

    # Save upgrade data
    @this_upgrade.encrypt(:data, JSON.generate(@data))
    @this_upgrade.status = 'kaupapa-ok'
    @this_upgrade.save

    next redirect '/initialsetup/newinstall/kaupapa'
  end
end
