class EARMMS::DashboardController < EARMMS::ApplicationController
  helpers EARMMS::DashboardHelpers
  helpers EARMMS::QuickLinkHelpers

  before do
    redirect '/' unless logged_in?
    @user = current_user
    @profile = EARMMS::Profile.for_user(@user)
    @user_name = @profile.decrypt(:full_name)
    @has_set_name = !(@user_name.nil? || @user_name.empty?)
    @mybranch = @profile.decrypt(:branch).strip.to_i
    @custom_content = custom_content_data()
    @quick_links = quick_links_data(profile: @profile)
    @quick_links = nil if @quick_links.nil? || @quick_links[:links].empty?
  end

  get '/' do
    @branches = branch_meetings
    @mymeetings = @branches[@mybranch][:upcoming] if @branches[@mybranch]
    @othermeetings = @branches.keys.map do |id|
      next nil if id == @mybranch
      @branches[id][:upcoming]
    end.flatten.compact

    @title = t(:'dashboard/title')
    haml :'dashboard/index'
  end
end
