require 'chronic'

class EARMMS::MembershipAdminUserController < EARMMS::MembershipAdminController
  before do  
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:user_change'

    @custom_fields = custom_field_desc()
    @branches = EARMMS::Branch.all.map do |b|
      {
        :id => b.id,
        :name => b.decrypt(:name),
      }
    end
  end

  get '/' do
    @title = t(:'membership/user/title')
    haml :'membership_admin/user/index'
  end

  post '/report' do
    date = request.params['date']&.strip&.downcase
    if date.nil? || date.empty? || (date = Chronic.parse(date, guess: true)).nil?
      flash :error, t(:'membership/user/report/errors/invalid_date')
      next redirect '/admin/user'
    end

    EARMMS::Workers::MembershipChangeReport.queue({requesting_user: current_user.id, since: date.strftime("%Y-%m-%d 00:00:00")})

    flash :success, t(:'membership/user/report/success')
    next redirect '/admin/user'
  end

  post '/generate-invite' do
    invite = EARMMS::Token.generate_invite
    invite.expiry = Chronic.parse("in 1 week", guess: true)
    display = invite.token.split('').each_slice(4).to_a.map(&:join).join('-')

    flash :success, t(:'membership/user/actions/generate_invite/success', code: display, expiry: invite.expiry)
    next redirect '/admin/user'
  end
end
