class EARMMS::UserSettingsController < EARMMS::ApplicationController
  before do
    unless logged_in?
      session[:after_login] = request.path
      flash :warning, t(:'auth/login/must_log_in')
      next redirect '/auth'
    end

    @user = current_user
    @profile = EARMMS::Profile.for_user(@user)
    @status = @profile.decrypt(:membership_status)&.strip&.downcase
    @fields = field_desc().map do |f|
      next nil if f[:display_in_profile_edit] == false
      if f[:display_only_if_membership_status]&.count&.positive?
        next nil unless f[:display_only_if_membership_status].include?(@status)
      end

      f
    end.compact
  end

  get '/' do
    @title = t(:'userprofile/edit/title')
    @values = @profile.field_hash

    haml :'user_settings/index'
  end

  post '/' do
    required = @fields.reject{|f| !f[:required]}
    has_all_required = required.map do |f|
      next false unless request.params.key?(f[:name])
      next false unless request.params[f[:name]]
      true
    end.all?

    unless has_all_required
      flash(:error, t(:'userprofile/edit/errors/required_field'))
      next redirect request.path
    end

    cjson = @profile.decrypt(:custom_fields)
    if cjson.nil? || cjson.empty?
      cjson = {}
    else
      cjson = JSON.parse(cjson)
    end

    @fields.each do |field|
      next unless request.params.key?(field[:name])
      if field[:is_custom]
        cjson[field[:name]] = request.params[field[:name]]
      else
        @profile.encrypt(field[:name], request.params[field[:name]])
      end
    end

    @profile.encrypt(:custom_fields, JSON.dump(cjson))
    @profile.save

    EARMMS::ProfileFilter.clear_filters_for(@profile)
    EARMMS::ProfileFilter.create_filters_for(@profile)

    flash(:success, t(:'userprofile/edit/success'))
    next redirect request.path
  end

  get '/email-verify' do
    if @user.email_verified
      flash :warning, t(:'userprofile/email_verify/errors/already_verified')
    else
      @user.send_email_verification!
      flash :success, t(:'userprofile/email_verify/success')
    end

    next redirect '/user'
  end
end
