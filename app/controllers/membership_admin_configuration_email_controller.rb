class EARMMS::MembershipAdminConfigurationEmailController < EARMMS::MembershipAdminController
  before do
    @title = t(:'membership/configuration/email/title')
  end

  get '/' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:config'

    @header_entry = EARMMS::ConfigModel.where(key: 'mass-email-header').first
    @footer_entry = EARMMS::ConfigModel.where(key: 'mass-email-footer').first

    haml :'membership_admin/config/email'
  end

  post '/' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:config'

    @header_entry = EARMMS::ConfigModel.find_or_create(key: 'mass-email-header') do |e|
      e.type = 'text'
      e.value = nil
    end

    @footer_entry = EARMMS::ConfigModel.find_or_create(key: 'mass-email-footer') do |e|
      e.type = 'text'
      e.value = nil
    end

    # change header
    header_changed = false
    header = request.params["header"]&.strip
    header_fragment = Sanitize.fragment(header, Sanitize::Config::RESTRICTED).gsub("&nbsp;", "").strip
    if header.nil? || header&.empty? || header_fragment.empty?
      header_changed = true if @header_entry.value != ''
      @header_entry.value = ''
    else
      header_changed = true if @header_entry.value != header
      @header_entry.value = header
    end

    if header_changed
      @header_entry.save
      gen_alert(:system, :config_change, t(:'system/configuration/edit/change/alert', {
        :key => @header_entry.key,
        :value => @header_entry.value,
        :force_language => true,
      }))
    end

    # change footer
    footer_changed = false
    footer = request.params["footer"]&.strip
    footer_fragment = Sanitize.fragment(footer, Sanitize::Config::RESTRICTED).gsub("&nbsp;", "").strip
    if footer.nil? || footer&.empty? || footer_fragment.empty?
      footer_changed = true if @footer_entry.value != ''
      @footer_entry.value = ''
    else
      footer_changed = true if @footer_entry.value != footer
      @footer_entry.value = footer
    end

    if footer_changed
      @footer_entry.save
      gen_alert(:system, :config_change, t(:'system/configuration/edit/change/alert', {
        :key => @footer_entry.key,
        :value => @footer_entry.value,
        :force_language => true,
      }))
    end

    flash :success, t(:'membership/configuration/email/success')
    next redirect request.path
  end
end
