class EARMMS::DashDevToolsController < EARMMS::ApplicationController
  before do
    next halt 404 unless settings.development?
  end

  get '/reload-languages' do
    if EARMMS::ServerUtils.app_server_has_multiple_workers?
      flash :warning, "Can't reload languages as the app server is running with multiple workers. Trigger a restart instead."
      next redirect back
    end

    EARMMS.load_languages
    flash :success, "Reloaded languages."
    redirect back
  end

  get '/restart' do
    if EARMMS::ServerUtils.app_server_has_multiple_workers?
      flash :warning, "App server running with multiple workers, this will trigger a phased restart."
    end

    EARMMS::ServerUtils.app_server_restart!
    flash :success, "Issued app server restart request."

    redirect back
  end
end
