require 'chronic'

class EARMMS::InitialSetupNewInstallMeetingRemindersController < EARMMS::InitialSetupController
  get '/' do
    haml :'initial_setup/layout', :layout => :layout_minimal do
      haml :'initial_setup/newinstall/layout', :layout => false do
        haml :'initial_setup/newinstall/meeting_reminders', :layout => false
      end
    end
  end

  post '/' do
    @data = JSON.parse(@this_upgrade.decrypt(:data))
    @data['config-entries'] ||= {}

    if request.params['enabled'].to_i.positive?
      datetime = request.params['datetime']&.strip&.downcase

      if datetime.nil? || datetime&.empty?
        flash :error, t(:'initial_setup/newinstall/meeting_reminders/errors/invalid_datetime')
        return redirect request.path
      end

      ts = Chronic.parse(datetime)
      if ts.nil?
        flash :error, t(:'initial_setup/newinstall/meeting_reminders/errors/invalid_datetime')
        return redirect request.path
      end

      @data['config-entries']['worker-meeting-reminder-next'] = datetime 
      @data['job-queue'] ||= []
      @data['job-queue'] << {
        'name' => 'SupporterMeetingReminder',
        'at' => ts.strftime("%Y-%m-%d %H:%M:%S"),
      }
    else
      @data['config-entries']['worker-meeting-reminder-secs'] = 0
    end

    # Save upgrade data
    @this_upgrade.encrypt(:data, JSON.generate(@data))
    @this_upgrade.status = 'meeting-reminders-ok'
    @this_upgrade.save
    next redirect '/initialsetup/newinstall/background-tasks'
  end
end
