require 'sinatra/base'
require 'json'

class EARMMS::ApiController < Sinatra::Base
  helpers EARMMS::ApplicationHelpers
  helpers EARMMS::ApiHelpers

  set :default_encoding, "UTF-8"
  set :views, File.expand_path('../../views', __FILE__)

  before do
    # Check if we're in upgrade flow
    vup = EARMMS::VersionUpgradeModel.where(upgrade_id: EARMMS::VERSION_UPGRADE_ID).first
    if vup.nil? || vup.status != 'complete'
      next halt 503
    end
  end

  error 404 do
    content_type 'application/json'
    JSON.generate({success: false, code: 404})
  end

  error 503 do
    content_type 'application/json'
    JSON.generate({success: false, code: 503})
  end

  error 400 do
    content_type 'application/json'
    JSON.generate({success: false, code: 400})
  end
end
