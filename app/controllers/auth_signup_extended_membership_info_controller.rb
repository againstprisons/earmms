class EARMMS::AuthSignupExtendedMembershipInfoController < EARMMS::ApplicationController
  helpers EARMMS::AuthSignupHelpers

  before do
    unless logged_in?
      flash(:error, t(:'auth/login/must_log_in'))
      session[:after_login] = request.path
      next redirect('/auth')
    end

    @current_step = "membership-info"
    @user = current_user
    @profile = EARMMS::Profile.for_user(@user)

    extended_signup_verify!()
    @signup_data = extended_signup_get_data()
    
    # Skip branch selection for supporters
    if %w[supporter].include?(@signup_data["status"])
      @user.signup_extended_status = "apply"
      @user.save

      redirect extended_signup_current_step_url()
    end

    @membership_info = EARMMS.app_config['membership-information']

    @auth_header_hide_home = @auth_header_hide_links = true
    @auth_layout_classes = %w[auth-layout-large auth-layout-centered]
    @title = t(:'auth/signup_extended/membership_info/title')
  end

  get '/' do
    haml :'auth/layout', :layout => :layout_minimal do
      haml :'auth/signup/extended/membership_info', :layout => false
    end
  end
  
  post '/' do
    @user.signup_extended_status = "apply"
    @user.save

    redirect extended_signup_current_step_url()
  end
end

