class EARMMS::MembershipAdminBranchMeetingAttendanceController < EARMMS::MembershipAdminController
  before do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:branch'
  end

  get '/:branchid/:meetingid' do |branchid, meetingid|
    @branch = EARMMS::Branch[branchid.to_i]
    next halt 404 unless @branch
    @branch_name = @branch.decrypt(:name)
    meeting = EARMMS::BranchMeeting[meetingid.to_i]
    next halt 404 unless meeting
    next halt 404 unless meeting.branch == @branch.id

    @meeting = {
      :meeting => meeting,
      :id => meeting.id,
      :datetime => Time.parse(meeting.decrypt(:datetime)),
      :location => meeting.decrypt(:location),
      :notes => meeting.decrypt(:notes),
    }

    @attendance = EARMMS::BranchMeetingAttendanceRecord.where(meeting: meeting.id).map do |at|
      profile = EARMMS::Profile[at.decrypt(:profile).to_i]
      next nil unless profile
      user = EARMMS::User[profile.user]
      next nil unless user

      status = at.decrypt(:attendance_status)
      status_friendly = t(:'meeting/attendance/unknown')
      if %w[present apologies].include?(status)
        status_friendly = t("meeting/attendance/#{status}".to_sym)
      end

      {
        :at => at,
        :user => user,
        :profile => profile,
        :user_name => profile.decrypt(:full_name),
        :status => {
          :type => status,
          :friendly => status_friendly,
        },
      }
    end.compact

    @title = t(:'membership/branch/meetings/attendance/title', :branch => @branch_name, :datetime => @meeting[:datetime])
    haml :'membership_admin/branch/meetings/attendance'
  end

  post '/:branchid/:meetingid/export' do |branchid, meetingid|
    branch = EARMMS::Branch[branchid.to_i]
    next halt 404 unless branch
    meeting = EARMMS::BranchMeeting[meetingid.to_i]
    next halt 404 unless meeting
    next halt 404 unless meeting.branch == branch.id

    EARMMS::Workers::AttendanceExport.queue :type => "meeting", :meeting => meeting.id, :requesting_user => current_user.id
    flash :success, t(:'membership/branch/meetings/attendance/export/success')

    next redirect "/admin/branch/meetings/attendance/#{branchid}/#{meetingid}"
  end
end
