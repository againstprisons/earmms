require 'chronic'

class EARMMS::SystemWorkQueueController < EARMMS::SystemController
  helpers EARMMS::SystemWorkQueueHelpers

  before do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'system:work_queue'
    
    @available_tasks = EARMMS::Workers.constants.map do |sym|
      m = EARMMS::Workers.const_get(sym)
      next nil unless m.respond_to?(:perform)
      [sym, m]
    end.compact.to_h
  end
  
  get '/' do
    @status = 'queued'
    if request.params.key?("status")
      @status = request.params["status"].downcase
    end

    page = request.params.key?("page") ? request.params["page"].to_i : 1

    @ds = EARMMS::WorkQueue
      .where(status: @status)
      .order(Sequel.desc(:id))
      .paginate(page, 25)

    @current_page = @ds.current_page
    @total_pages = @ds.page_count
    @tasks = @ds.map{|m| work_queue_data(m)}

    @new_uri = Addressable::URI.parse("/system/workqueue/queue")
    @new_uri.query_values = {
      :back => "/system/workqueue/?page=#{@current_page}&status=#{@status}"
    }

    @title = t(:'system/work_queue/title')    
    haml :'system/work_queue/index'
  end

  get '/queue' do
    @back = '/system/workqueue'
    if request.params.key?("back")
      @back = request.params["back"]
    end
    
    @title = t(:'system/work_queue/queue/title')
    haml :'system/work_queue/queue'
  end

  post '/queue' do
    @back = '/system/workqueue'
    if request.params.key?("back")
      @back = request.params["back"]
    end

    type = request.params["type"]&.strip&.to_sym
    if type.nil? || type.empty? || !(@available_tasks.key?(type))
      flash :error, t(:'system/work_queue/queue/errors/invalid_type')
      next redirect request.path
    end

    data = request.params['data']&.strip
    data = '{}' if data.nil? || data.empty?
    begin
      data = JSON.parse(data)
    rescue => e
      flash :error, t(:'system/work_queue/queue/errors/invalid_json', :error => e)
      next redirect request.path
    end

    date = request.params['date']&.strip
    date = Time.now.strftime("%Y-%m-%d") if date.nil? || date.empty?

    time = request.params['time']&.strip
    time = Time.now.strftime("%H:%M") if time.nil? || time.empty?

    begin
      run_at = Chronic.parse("#{date} #{time}")
    rescue => e
      flash :error, t(:'system/work_queue/queue/errors/invalid_time', :datetime => "#{date} #{time}")
      next redirect request.path
    end

    begin
      @available_tasks[type].queue_at(run_at, data)
    rescue => e
      flash :error, t(:'system/work_queue/queue/errors/queue_at_error', :error => e)
    end

    flash :success, t(:'system/work_queue/queue/success', :job_type => type.to_s, :run_at => run_at.iso8601, :in_secs => (run_at - Time.now).round.to_s)
    redirect request.path
  end

  get '/log/:id' do |id|
    entry = EARMMS::WorkQueue[id.to_i]
    next halt 404 unless entry
    @entry = work_queue_data(entry)
    @log = entry.decrypt(:log)

    @back = '/system/workqueue'
    if request.params.key?("back")
      @back = request.params["back"]
    end

    @title = t(:'system/work_queue/log/title', :id => entry.id)
    haml :'system/work_queue/log'
  end
end
