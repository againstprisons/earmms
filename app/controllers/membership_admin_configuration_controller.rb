class EARMMS::MembershipAdminConfigurationController < EARMMS::MembershipAdminController
  get '/' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:config'
    @title = "Configuration"

    @maintenance_enabled = is_maintenance?()
    @signups_enabled = !(EARMMS::ConfigModel.where(key: 'signups').first&.value == 'no')
    @signups_will_req_refresh = @signups_enabled == EARMMS.app_config['signups']

    haml :'membership_admin/config/index'
  end

  post '/toggle' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:config'

    @key = request.params['key']&.strip&.downcase
    next halt 404 unless %w[signups maintenance].include?(@key)

    @entry = EARMMS::ConfigModel.find_or_create(key: @key) do |e|
      e.type = 'bool'
      e.value = 'yes'
    end

    @entry.value = (@entry.value == 'yes' ? 'no' : 'yes')
    @entry.save

    gen_alert(:system, :config_change, t(:'system/configuration/edit/change/alert', {
      :key => @entry.key,
      :value => @entry.value,
      :force_language => true,
    }))

    flash(:success, t(:'membership/configuration/toggle/success', {
      :key => @key,
      :state => t("membership/configuration/toggle/current/#{@entry.value == 'yes' ? 'enabled' : 'disabled'}".to_sym)
    }))

    redirect '/admin/config'
  end
end
