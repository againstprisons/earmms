class EARMMS::BranchAdminMembersController < EARMMS::ApplicationController
  get '/:branchid' do |branchid|
    branchid = branchid.to_i
    next halt 404 unless logged_in?
    next halt 404 unless has_role? "branch:#{branchid}:list"

    @branch = EARMMS::Branch[branchid]
    next halt 404 unless @branch
    @branchname = @branch.decrypt(:name)

    @members = EARMMS::ProfileFilter.perform_filter(:branch, "#{@branch.id.to_s}:member").map do |f|
      p = EARMMS::Profile[f.profile]
      u = EARMMS::User[p.user]

      {
        :user => u,
        :profile => p,
        :user_name => p.decrypt(:full_name),
      }
    end.sort{|a, b| a[:user].id <=> b[:user].id}

    @supporters_on = request.params['supporters'].to_i == 1
    @supporters = []
    if @supporters_on
      @supporters = EARMMS::ProfileFilter.perform_filter(:branch, "#{@branch.id.to_s}:supporter").map do |f|
        p = EARMMS::Profile[f.profile]
        u = EARMMS::User[p.user]

        {
         :user => u,
         :profile => p,
         :user_name => p.decrypt(:full_name),
        }
      end.sort{|a, b| a[:user].id <=> b[:user].id}
    end

    if request.params['text'].to_i == 1
      content_type "text/plain"
      next [@members, @supporters].flatten.map do |m|
        "#{ERB::Util.html_escape(m[:user_name])} <#{m[:user].email}>"
      end.join(', ')
    end

    @title = t(:'branch/members/title', :branch => @branchname)
    haml :"branch_admin/members"
  end
end
