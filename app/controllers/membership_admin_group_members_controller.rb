class EARMMS::MembershipAdminGroupMembersController < EARMMS::MembershipAdminController
  helpers EARMMS::GroupHelpers

  before do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:group:access'
  end

  get '/:id' do |id|
    @group = EARMMS::Group[id.to_i]
    next halt 404 unless @group
    @group_h = @group.to_h
    @summary = @group.summary
    @members = get_group_members(@group)

    @title = t(:'membership/groups/members/title', :group => @group_h[:name])
    haml :'membership_admin/group/members'
  end
  
  post '/:id' do |id|
    @group = EARMMS::Group[id.to_i]
    next halt 404 unless @group
    @group_h = @group.to_h
    @summary = @group.summary
    @members = get_group_members(@group)
    
    case request.params['action']&.strip&.downcase
    when 'remove'
      user = EARMMS::User[request.params['uid'].to_i]
      next halt 400 unless user
      next halt 400 unless @members.keys.include?(user.id)
      
      if @members[user.id][:admin]
        flash :error, t(:'membership/groups/members/remove/errors/is_admin')
        next redirect request.path
      end
      
      gm = @members[user.id][:group_member]
      EARMMS::GroupMemberFilter.clear_filters_for(gm)
      gm.delete
      
      flash :success, t(:'membership/groups/members/remove/success')

    when 'toggle-admin'
      user = EARMMS::User[request.params['uid'].to_i]
      next halt 400 unless user
      next halt 400 unless @members.keys.include?(user.id)
      
      gm = @members[user.id][:group_member]
      if @members[user.id][:admin]
        gm.encrypt(:roles, '')
        flash :success, t(:'membership/groups/members/demote/success')
      else
        gm.encrypt(:roles, 'admin,email')
        flash :success, t(:'membership/groups/members/promote/success')
      end
      gm.save

      EARMMS::GroupMemberFilter.clear_filters_for(gm)
      EARMMS::GroupMemberFilter.create_filters_for(gm)
      
    else
      flash :error, t(:'membership/groups/members/errors/invalid_action')
    end
    
    next redirect request.path
  end
end
