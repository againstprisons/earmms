class EARMMS::SystemConfigurationRefreshController < EARMMS::SystemController
  helpers EARMMS::SystemConfigurationHelpers

  get '/' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'system:change_config'
    @title = t(:'system/configuration/refresh/title')

    # Do dry run of configuration refresh
    @output = EARMMS.app_config_refresh(:dry => true)
    @has_warnings = @output.map{|x| x[:warnings].count.positive?}.any?
    @dry_run = true

    haml :'system/config/refresh'
  end

  post '/' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'system:change_config'
    @title = t(:'system/configuration/refresh/title')

    # Enable maintenance mode
    maint_cfg = EARMMS::ConfigModel.find_or_create(key: 'maintenance') do |e|
      e.type = 'bool'
      e.value = 'no'
    end
    maint_enabled = maint_cfg.value == 'yes'
    maint_cfg.value = 'yes'
    maint_cfg.save

    # Do configuration refresh
    @output = EARMMS.app_config_refresh()
    @has_warnings = @output.map{|x| x[:warnings].count.positive?}.any?

    # Disable maintenance mode
    unless maint_enabled
      maint_cfg.value = 'no'
      maint_cfg.save
    end

    # Restart if we're running with multiple workers
    if EARMMS::ServerUtils.app_server_has_multiple_workers?
      EARMMS::ServerUtils.app_server_restart!
    end

    @dry_run = false
    haml :'system/config/refresh'
  end
end
