require 'base64'
require 'json'
require 'addressable'

class EARMMS::InitialSetupUpgradeApplyChangesController < EARMMS::InitialSetupController
  get '/' do
    begin
      @blob = JSON.parse(@this_upgrade.decrypt(:data))
    rescue
      # This should never happen!
      next halt 500
    end

    # Gather changes
    @changes = upgrade_construct_changes(@blob)

    # Collate the overview data
    @converted_keys = @blob['config'].keys.count || 0
    @custom_fields = @blob['config']['custom-user-fields']&.count || 0
    @rename_count = @changes.select{|x| x[:type] == :rename}.count || 0
    @delete_count = @changes.select{|x| x[:type] == :remove}.count || 0
    @default_count = @changes.select{|x| x[:source] == :default}.count || 0
    @email_from = @changes.select{|x| x[:new_key] == 'email-from'}.first[:new_value]
    @email_host = @changes.select{|x| x[:new_key] == 'email-smtp-host'}.first[:new_value]
    @email_host = Addressable::URI.parse(@email_host)
    @email_host = "#{@email_host.host}:#{@email_host.port}"

    # Generate a verification code and store it in the session if one
    # doesn't already exist there
    if session.key?(:initial_setup_upgrade_apply_code)
      @verify_code = session[:initial_setup_upgrade_apply_code]
    else
      @verify_code = Random.new.rand(100000000 .. 999999999).to_s
      session[:initial_setup_upgrade_apply_code] = @verify_code
    end

    haml :'initial_setup/layout', :layout => :layout_minimal do
      haml :'initial_setup/upgrade/layout', :layout => false do
        haml :'initial_setup/upgrade/apply', :layout => false
      end
    end
  end

  post '/' do
    begin
      @blob = JSON.parse(@this_upgrade.decrypt(:data))
    rescue
      # This should never happen!
      next halt 500
    end

    # Check verification code
    form_verify = request.params['verify']&.strip
    if form_verify
      form_verify = form_verify.split(' ').map{|x| x.split('-')}.flatten.join('')
    end
    if form_verify != session[:initial_setup_upgrade_apply_code]
      flash :error, t(:'initial_setup/upgrade/apply/errors/invalid_code')
      next redirect request.path
    end
    session.delete(:initial_setup_upgrade_apply_code)

    # Gather changes
    @changes = upgrade_construct_changes(@blob)

    # Apply changes
    @changes.each do |change|
      if change[:type] == :add || change[:type] == :modify
        cfg = EARMMS::ConfigModel.find_or_create(key: change[:new_key])
        cfg.type = EARMMS::APP_CONFIG_ENTRIES[change[:new_key]][:type].to_s
        cfg.value = change[:new_value]
        cfg.save
      elsif change[:type] == :rename
        cfg = EARMMS::ConfigModel.where(key: change[:old_key]).first
        if cfg
          cfg.key = change[:new_key]
        else
          cfg = EARMMS::ConfigModel.new(key: change[:new_key])
        end

        cfg.type = EARMMS::APP_CONFIG_ENTRIES[change[:new_key]][:type].to_s
        cfg.value = change[:new_value]
        cfg.save

      elsif change[:type] == :remove
        EARMMS::ConfigModel.where(key: change[:old_key]).delete
      end
    end

    # Save status as 'applied' and redirect to the "hey, you did it!" page
    @this_upgrade.status = 'applied'
    @this_upgrade.save
    next redirect current_step_url
  end
end
