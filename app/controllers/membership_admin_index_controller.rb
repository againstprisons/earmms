class EARMMS::MembershipAdminIndexController < EARMMS::MembershipAdminController
  helpers EARMMS::BranchHelpers

  get '/' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:access'

    @membership_alerts = EARMMS::Alert.where(section: "membership", actioned: false).count

    # XXX: change this for multiple membership statuses?
    @counts = {status: {}, branches: get_branches_data(ignore_roles: true)}
    %w[member supporter].each do |status|
      @counts[:status][status] = EARMMS::ProfileFilter.perform_filter(:membership_status, status).count
    end

    @title = t(:'membership/title')
    haml :'membership_admin/index'
  end
end
