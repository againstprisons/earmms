class EARMMS::InitialSetupNewInstallCreateBranchController < EARMMS::InitialSetupController
  get '/' do
    @data = JSON.parse(@this_upgrade.decrypt(:data))
    @data['first-branch'] ||= {}

    if EARMMS::Branch.count.positive?
      @data['first-branch']['create'] = false
      @this_upgrade.encrypt(:data, JSON.generate(@data))
      @this_upgrade.status = 'branch-ok'
      @this_upgrade.save

      flash :warning, t(:'initial_setup/newinstall/create_branch/skipping')
      next redirect '/initialsetup/newinstall/kaupapa'
    end

    haml :'initial_setup/layout', :layout => :layout_minimal do
      haml :'initial_setup/newinstall/layout', :layout => false do
        haml :'initial_setup/newinstall/create_branch', :layout => false
      end
    end
  end

  post '/' do
    @data = JSON.parse(@this_upgrade.decrypt(:data))
    @data['first-branch'] ||= {}

    if EARMMS::Branch.count.positive?
      @data['first-branch']['create'] = false
      @this_upgrade.encrypt(:data, JSON.generate(@data))
      @this_upgrade.status = 'branch-ok'
      @this_upgrade.save

      flash :warning, t(:'initial_setup/newinstall/create_branch/skipping')
      next redirect '/initialsetup/newinstall/kaupapa'
    end

    @bname = request.params['branch_name']&.strip
    if @bname.nil? || @bname&.empty?
      flash :error, t(:'initial_setup/newinstall/create_branch/errors/no_name')
      next redirect request.path
    end

    # Parse upgrade data and add data to create new branch
    @data['first-branch']['create'] = true
    @data['first-branch']['name'] = @bname

    # Save upgrade data
    @this_upgrade.encrypt(:data, JSON.generate(@data))
    @this_upgrade.status = 'branch-ok'
    @this_upgrade.save
    next redirect '/initialsetup/newinstall/kaupapa'
  end
end
