class EARMMS::SystemDebuggingController < EARMMS::SystemController
  before do
    @title = t(:'system/debugging/title')
    @mutexes = EARMMS::MutexModel.map do |m|
      user = EARMMS::User[m.user]
      profile = EARMMS::Profile.for_user(user.id)

      {
        type: m.type,
        data: m.data,
        creation: m.creation,
        user: {
          id: m.user,
          name: profile&.decrypt(:full_name),
        },
      }
    end
  end

  get '/' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'system:debugging'

    haml :'system/debug/index'
  end

  get '/flashes' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'system:debugging'
    
    %i[success warning error].each do |ftype|
      flash(ftype, t(:'system/debugging/flashes/flash', :type => ftype))
    end
    
    @title = t(:'system/debugging/flashes/title')
    haml :'system/debug/flashes'
  end
  
  post '/alert' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'system:debugging'

    valid_sections = %i[system membership]
    section = request.params['section']&.strip&.downcase&.to_sym
    unless valid_sections.include?(section)
      flash :error, t(:'system/debugging/alerts/errors/invalid_section')
      next redirect '/system/debug'
    end

    is_empty = request.params['empty']&.strip&.downcase == 'on'
    if is_empty
      alert = EARMMS::Alert.new(section: section.to_s, actioned: false, created: Time.now)
    else
      alert = gen_alert(section, :test, t(:'system/debugging/alerts/alert', :force_language => true))
    end
    alert.save

    flash :success, t(:'system/debugging/alerts/success', :empty => is_empty, :section => section.to_s, :alert_id => alert.id)
    next redirect '/system/debug'
  end
end
