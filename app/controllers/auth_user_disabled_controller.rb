class EARMMS::AuthUserDisabledController < EARMMS::ApplicationController
  before do
    @auth_header_hide_home = @auth_header_hide_links = true
    @auth_layout_classes = %w[auth-layout-small auth-layout-centered]

    @user = current_user
    next redirect('/') unless @user
    next redirect('/') unless @user.disabled_reason
  end

  get '/' do
    @reason = @user.decrypt(:disabled_reason)

    @title = t :'auth/disabled/title'
    haml :'auth/layout', :layout => :layout_minimal do
      haml :'auth/user_disabled', :layout => false
    end
  end
end
