class EARMMS::ApiUserController < EARMMS::ApiController
  helpers EARMMS::UserTagHelpers

  post '/' do
    content_type 'application/json'
    next halt 404 unless @token = api_token?()

    begin
      if request.params['userid'] != nil
        user = EARMMS::User[request.params['userid']&.strip.to_i]
        raise "invalid user" unless user

        profile = EARMMS::Profile.for_user(user)
        raise "no profile" unless profile

      elsif request.params['usertag'] != nil
        usertag = request.params['usertag']&.strip&.upcase
        raise "no usertag" unless usertag

        user = tag_to_user(usertag)
        raise "invalid user" unless user

        profile = EARMMS::Profile.for_user(user)
        raise "no profile" unless profile

      else
        raise "no request"
      end
    rescue
      halt 400
    end

    # Begin constructing data, now that we have valid user/profile objects
    data = {}
    data[:success] = true
    data[:user] = {}
    data[:user][:id] = [user.id, profile.id].join(':')
    data[:user][:name] = nil
    data[:user][:status] = 'supporter'
    data[:user][:branch] = nil

    # User name
    full_name = profile.decrypt(:full_name)
    unless full_name.nil? || full_name&.empty?
      data[:user][:name] = full_name
    end

    # User membership status
    status = profile.decrypt(:membership_status)
    if %w[member supporter].include?(status)
      data[:user][:status] = status
    end

    # User branch
    branch = EARMMS::Branch[profile.decrypt(:branch).to_i]
    if branch
      data[:user][:branch] = {}
      data[:user][:branch][:id] = branch.id
      data[:user][:branch][:name] = branch.decrypt(:name)
    end

    JSON.generate(data)
  end
end
