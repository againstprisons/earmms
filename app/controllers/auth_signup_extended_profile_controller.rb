class EARMMS::AuthSignupExtendedProfileController < EARMMS::ApplicationController
  helpers EARMMS::AuthSignupHelpers

  before do
    unless logged_in?
      flash(:error, t(:'auth/login/must_log_in'))
      session[:after_login] = request.path
      next redirect('/auth')
    end

    @current_step = "profile"
    @user = current_user
    @profile = EARMMS::Profile.for_user(@user)

    extended_signup_verify!()
    @signup_data = extended_signup_get_data()

    @fields = field_desc().map do |f|
      next nil if f[:display_in_extended_signup] == false

      if %w[supporter].include?(@signup_data['status'])
        next nil unless f[:required]
      end

      if f[:display_only_if_membership_status]&.count&.positive?
        next nil unless f[:display_only_if_membership_status].include?(@signup_data['status'])
      end

      f
    end.compact

    @auth_header_hide_home = @auth_header_hide_links = true
    @auth_layout_classes = %w[auth-layout-medium auth-layout-centered]
    @title = t(:'auth/signup_extended/profile/title')
  end

  get '/' do
    haml :'auth/layout', :layout => :layout_minimal do
      haml :'auth/signup/extended/profile', :layout => false
    end
  end
  
  post '/' do
    required = @fields.reject{|f| !f[:required]}
    has_all_required = required.map do |f|
      next false unless request.params.key?(f[:name])
      next false unless request.params[f[:name]]
      true
    end.all?

    unless has_all_required
      flash(:error, t(:'auth/signup_extended/profile/errors/required_field'))
      next redirect request.path
    end

    cjson = @profile.decrypt(:custom_fields)
    if cjson.nil? || cjson.empty?
      cjson = {}
    else
      cjson = JSON.parse(cjson)
    end

    f_data = {}
    cf_data = {}
    @fields.each do |field|
      next unless request.params.key?(field[:name])
      if field[:is_custom]
        cf_data[field[:name]] = request.params[field[:name]]&.strip
      else
        f_data[field[:name]] = request.params[field[:name]]&.strip
      end
    end

    # Save data
    @signup_data["fields"] = {"db": f_data, "custom": cf_data}
    @user.encrypt(:signup_extended_status_data, JSON.generate(@signup_data))
    @user.signup_extended_status = "membership-info"
    @user.save

    redirect extended_signup_current_step_url()
  end
end

