class EARMMS::BranchAdminController < EARMMS::ApplicationController
  helpers EARMMS::BranchHelpers

  before do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'branch:access'
  end

  get '/' do
    @branches = get_branches_data()
    @title = t(:'branch/title')
    haml :"branch_admin/index"
  end

  get '/:branchid' do |branchid|
    branchid = branchid.to_i
    next halt 404 unless has_role? "branch:#{branchid}:access"

    @branch = EARMMS::Branch[branchid]
    next halt 404 unless @branch
    @branchname = @branch.decrypt(:name)
    @member_count = EARMMS::ProfileFilter.perform_filter(:branch, "#{@branch.id.to_s}:member").count

    now = DateTime.now()
    @yesterday = now - 1
    @yesterday = DateTime.civil(@yesterday.year, @yesterday.month, @yesterday.day, 23, 59, 59)
    @twoweeks = @yesterday + 14
    @upcoming = EARMMS::BranchMeeting.where(branch: @branch.id).order(Sequel.desc(:id)).limit(5).map do |m|
      dt = DateTime.parse(m.decrypt(:datetime))
      next nil unless dt.between?(@yesterday, @twoweeks)

      {
        :id => m.id,
        :datetime => dt,
      }
    end.compact

    @title = t(:'branch/view/title', :branch => @branchname)
    haml :"branch_admin/branch"
  end
end
