require 'base64'
require 'json'
require 'addressable'

class EARMMS::InitialSetupNewInstallCompleteController < EARMMS::InitialSetupController
  get '/' do
    haml :'initial_setup/layout', :layout => :layout_minimal do
      haml :'initial_setup/newinstall/layout', :layout => false do
        haml :'initial_setup/newinstall/complete', :layout => false
      end
    end
  end

  post '/' do
    # Save upgrade completion
    @this_upgrade.status = 'complete'
    @this_upgrade.data = nil
    @this_upgrade.save
    
    # Release the mutex
    @mutex[:mutex].release!

    # Enable maintenance mode
    cfg = EARMMS::ConfigModel.find_or_create(key: 'maintenance') do |e|
      e.type = 'bool'
      e.value = 'no'
    end
    cfg.value = 'yes'
    cfg.save

    # Trigger app config refresh
    EARMMS.app_config_refresh(:force => true)

    # Restart if we're running with multiple workers
    if EARMMS::ServerUtils.app_server_has_multiple_workers?
      EARMMS::ServerUtils.app_server_restart!
    end

    next redirect '/'
  end
end
