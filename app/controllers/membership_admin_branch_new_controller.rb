class EARMMS::MembershipAdminBranchNewController < EARMMS::MembershipAdminController
  helpers EARMMS::MembershipAdminBranchNewHelpers

  before do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:branch:create'
  end

  get '/' do
    @title = t(:'membership/branch/new/title')
    haml :'membership_admin/branch/new'
  end

  post '/' do
    name = request.params["name"]&.strip
    if name.nil? || name.empty?
      flash :error, t(:'membership/branch/new/errors/no_name')
      next redirect request.path
    end

    branch = EARMMS::Branch.new
    branch.save
    branch.encrypt(:name, name)
    branch.save

    flash :success, t(:'membership/branch/new/success', :name => name, :bid => branch.id)
    next redirect "/admin/branch/edit/#{branch.id}"
  end
end
