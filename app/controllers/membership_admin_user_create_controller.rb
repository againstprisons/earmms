class EARMMS::MembershipAdminUserCreateController < EARMMS::MembershipAdminController
  helpers EARMMS::FieldHelpers

  before do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:user:create'
  end

  get '/' do
    @title = t(:'membership/user/create/title')
    haml :'membership_admin/user/create'
  end

  post '/' do
    name = request.params["user-name"]&.strip
    if name.nil? || name.empty?
      flash :error, t(:'membership/user/create/errors/no_name')
      next redirect request.path
    end

    user = EARMMS::User.new({
      email: nil,
      creation: Time.now,
      last_login: nil,
      signup_extended_status: 'complete'
    }).save

    profile = EARMMS::Profile.new(user: user.id)
    profile.save
    profile.encrypt(:full_name, name)
    profile.encrypt(:branch, EARMMS.app_config['membership-default-branch'].to_s)
    profile.encrypt(:membership_status, 'supporter')
    profile.encrypt(:custom_fields, custom_field_default_json)
    profile.save

    EARMMS::ProfileFilter.clear_filters_for(profile)
    EARMMS::ProfileFilter.create_filters_for(profile)

    flash :success, t(:'membership/user/create/success', :uid => user.id)
    redirect "/admin/user/edit/#{user.id}"
  end
end
