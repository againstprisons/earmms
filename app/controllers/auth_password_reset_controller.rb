class EARMMS::AuthPasswordResetController < EARMMS::ApplicationController
  before do
    @auth_layout_classes = %w[auth-layout-small auth-layout-centered]
    next redirect('/') if logged_in?
  end

  get '/' do
    @title = t(:'auth/reset_password/title')

    haml :'auth/layout', :layout => :layout_minimal do
      haml :'auth/password_reset/index', :layout => false
    end
  end

  post '/' do
    email = request.params['email']&.strip&.downcase
    unless email
      flash :error, t(:'auth/reset_password/errors/no_email')
      next redirect request.path
    end

    # Get user for this email, and send a reset if the user exists
    user = EARMMS::User.where(email: email).first
    user.send_password_reset! if user

    @title = t(:'auth/reset_password/title')
    haml :'auth/layout', :layout => :layout_minimal do
      haml :'auth/password_reset/sent', :layout => false
    end
  end

  get '/:token' do |token|
    @auth_header_hide_home = @auth_header_hide_links = true

    token = token.strip.downcase
    @token = EARMMS::Token.where(token: token, use: 'password_reset').first
    next halt 404 unless @token
    next halt 404 unless @token.is_valid?

    @user = EARMMS::User[@token.user]
    next halt 404 unless @user

    @title = t(:'auth/reset_password/do/title')
    haml :'auth/layout', :layout => :layout_minimal do
      haml :'auth/password_reset/reset', :layout => false
    end
  end

  post '/:token' do |token|
    token = token.strip.downcase
    @token = EARMMS::Token.where(token: token, use: 'password_reset').first
    next halt 404 unless @token
    next halt 404 unless @token.is_valid?

    @user = EARMMS::User[@token.user]
    next halt 404 unless @user

    password = request.params['password']&.strip
    repeat = request.params['password-repeat']&.strip
    if password.nil? || password.empty? || repeat.nil? || repeat.empty?
      flash :error, t(:'auth/reset_password/do/errors/enter_password')
      next redirect request.path
    end

    unless password == repeat
      flash :error, t(:'auth/reset_password/do/errors/no_match')
      next redirect request.path
    end

    @user.password = request.params["password"]
    @user.save

    flash :success, t(:'auth/reset_password/do/success')
    next redirect('/auth')
  end
end
