class EARMMS::InitialSetupNewInstallRolesController < EARMMS::InitialSetupController
  helpers do
    def available_roles
      roles = [
        {
          :role => 'system:*',
          :form_id => 'role__system_admin',
          :desc => t(:'initial_setup/newinstall/roles/role/system_admin'),
          :default => true,
        },
        {
          :role => 'system:alert_emails',
          :form_id => 'role__system_alerts',
          :desc => t(:'initial_setup/newinstall/roles/role/system_alerts'),
          :default => true,
        },
        {
          :role => 'admin:*',
          :form_id => 'role__membership_admin',
          :desc => t(:'initial_setup/newinstall/roles/role/membership_admin'),
          :default => true,
        },
        {
          :role => 'admin:alert_emails',
          :form_id => 'role__membership_alerts',
          :desc => t(:'initial_setup/newinstall/roles/role/membership_alerts'),
          :default => true,
        },
        {
          :role => 'branch:*',
          :form_id => 'role__branch_all',
          :desc => t(:'initial_setup/newinstall/roles/role/branch_all'),
          :default => true,
        },
        {
          :role => 'email:*',
          :form_id => 'role__email_all',
          :desc => t(:'initial_setup/newinstall/roles/role/email_all'),
          :default => true,
        },
      ]

      roles.map do |role|
        role[:user_has] = EARMMS::UserHasRole.where(:user => current_user.id, :role => role[:role]).count.positive?
        role
      end
    end
  end

  get '/' do
    haml :'initial_setup/layout', :layout => :layout_minimal do
      haml :'initial_setup/newinstall/layout', :layout => false do
        haml :'initial_setup/newinstall/roles', :layout => false
      end
    end
  end

  post '/' do
    @user = current_user
    @data = JSON.parse(@this_upgrade.decrypt(:data))

    @add_roles = []
    @remove_roles = []
    available_roles.each do |role|
      next if role[:user_has]
      form_res = request.params[role[:form_id]]&.strip&.downcase == 'on'

      if !role[:user_has] && form_res
        @add_roles << role[:role]
      elsif role[:user_has] && !role[:form_res]
        @remove_roles << role[:role]
      end
    end

    if @remove_roles.include?('system:*')
      flash :error, t(:'initial_setup/newinstall/roles/errors/no_system_admin')
      @add_roles << 'system:*' unless @add_roles.include?('system:*')
      @remove_roles.delete('system:*')
    end

    # save role add/remove for the current user in the upgrade data
    @data['roles'] ||= {}
    @data['roles'][@user.id] ||= {}
    @data['roles'][@user.id][:add] ||= []
    @data['roles'][@user.id][:add].push(*@add_roles)
    @data['roles'][@user.id][:remove] ||= []
    @data['roles'][@user.id][:remove].push(*@remove_roles)

    # save upgrade data
    @this_upgrade.encrypt(:data, JSON.generate(@data))
    @this_upgrade.status = 'roles-ok'
    @this_upgrade.save

    next redirect '/initialsetup/newinstall/apply'
  end
end
