require 'rotp'
require 'rqrcode'
require 'base64'

class EARMMS::UserSettingsMfaKeyController < EARMMS::ApplicationController
  before do
    unless logged_in?
      flash(:error, t(:'auth/login/must_log_in'))
      session[:after_login] = request.path
      next redirect '/auth'
    end

    @user = current_user
    @profile = EARMMS::Profile.for_user(@user)
    @has_totp = @user.totp_enabled
    next redirect '/user/mfa' unless @has_totp

    @keys = EARMMS::U2FRegistration.where(user: @user.id).all
    @enhanced = @user.enhanced_security
  end

  get '/' do
    @title = t(:'userprofile/mfa/key/title')
    haml :'user_settings/mfa/key/index'
  end

  post '/register' do
    @auth_header_hide_home = @auth_header_hide_links = true
    @auth_layout_classes = %w[auth-layout-small auth-layout-centered]
    @name = request.params["keyname"]&.strip
    next halt 400 if @name.nil? || @name&.empty?
    @title = t(:'userprofile/mfa/key/register/do/title', name: @name)

    @u2f = U2F::U2F.new(request.base_url)
    @messages = {
      insert: t(:'mfa_key/status/insert'),
      error: t(:'mfa_key/status/error'),
      success: t(:'mfa_key/status/success'),
    }

    if request.params['confirm']&.strip.to_i != 1
      @reg_requests = @u2f.registration_requests
      session[:u2f_challenges] = @reg_requests.map(&:challenge)
      @sign_requests = @u2f.authentication_requests(@keys.map(&:key_handle))
      @app_id = @u2f.app_id

      next haml :'auth/layout', :layout => :layout_minimal do
        haml :'user_settings/mfa/key/register', :layout => false
      end
    end

    reg = begin
      response = U2F::RegisterResponse.load_from_json(request.params["response"]&.strip)
      @u2f.register!(session[:u2f_challenges], response)
    rescue U2F::Error => e
      flash :error, t(:'userprofile/mfa/key/register/errors/u2f_exception', ex: e.class.name)
      next redirect '/user/mfa/key'
    ensure
      session.delete(:u2f_challenges)
    end

    robj = EARMMS::U2FRegistration.new({
      user: @user.id,
      name: @name,
      certificate: reg.certificate,
      key_handle: reg.key_handle,
      public_key: reg.public_key,
      counter: reg.counter,
    }).save

    flash :success, t(:'userprofile/mfa/key/register/success')
    next redirect '/user/mfa/key'
  end

  post '/remove' do
    @auth_header_hide_home = @auth_header_hide_links = true
    @auth_layout_classes = %w[auth-layout-small auth-layout-centered]
    @title = t(:'userprofile/mfa/key/remove/title')

    @key = EARMMS::U2FRegistration[request.params['keyid']&.strip.to_i]
    next halt 404 unless @key
    next halt 404 unless @key.user == @user.id

    if @enhanced && @keys.count <= 1
      flash :error, t(:'userprofile/mfa/key/remove/errors/enhanced_security')
      next redirect '/user/mfa/key'
    end

    if request.params['confirm']&.strip.to_i != 1
      next haml :'auth/layout', :layout => :layout_minimal do
        haml :'user_settings/mfa/key/remove', :layout => false
      end
    end

    password = request.params['password']&.strip
    if password.nil? || password.empty?
      flash :error, t(:'userprofile/mfa/key/remove/errors/invalid_password')
      next haml :'auth/layout', :layout => :layout_minimal do
        haml :'user_settings/mfa/key/remove', :layout => false
      end
    end

    unless @user.verify_password(password)
      flash :error, t(:'userprofile/mfa/key/remove/errors/invalid_password')
      next haml :'auth/layout', :layout => :layout_minimal do
        haml :'user_settings/mfa/key/remove', :layout => false
      end
    end

    @key.delete
    flash :success, t(:'userprofile/mfa/key/remove/success')
    redirect '/user/mfa/key'
  end
end
