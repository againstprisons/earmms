class EARMMS::InitialSetupNewInstallController < EARMMS::InitialSetupController
  before do
    @this_upgrade.mode = 'newinstall'
    @this_upgrade.save
  end

  helpers do
    def render_view
      haml :'initial_setup/layout', :layout => :layout_minimal do
        haml :'initial_setup/newinstall/layout', :layout => false do
          haml :'initial_setup/newinstall/index', :layout => false
        end
      end
    end
  end

  get '/' do
    @n_base_url = "#{request.scheme || 'http'}://#{request.host}"
    render_view
  end

  post '/' do
    @data = @this_upgrade.decrypt(:data)
    @data = '{}' if @data.nil? || @data.empty?
    @data = JSON.parse(@data)

    # get form data
    @n_site_name = request.params['site_name']&.strip
    @n_site_name = nil if @n_site_name&.empty?
    @n_org_name = request.params['org_name']&.strip
    @n_org_name = nil if @n_org_name&.empty?
    @n_base_url = request.params['base_url']&.strip&.downcase
    @n_base_url = nil if @n_base_url&.empty?
    @n_email_from = request.params['email_from']&.strip
    @n_email_from = nil if @n_email_from&.empty?

    none_of = [
      @n_site_name.nil?,
      @n_org_name.nil?,
      @n_base_url.nil?,
      @n_email_from.nil?,
    ]

    if none_of.any?
      flash :error, t(:'initial_setup/newinstall/basecfg/errors/required_field_missing')
      @n_base_url ||= "#{request.scheme || 'http'}://#{request.host}"

      next render_view
    end

    # Create SMTP url
    begin
      smtp_sec = request.params['smtp_sec']&.strip&.downcase || 'none'
      smtp_auth = request.params['smtp_auth']&.strip&.downcase || 'plain'
      smtp_verify = request.params['smtp_verify']&.strip&.upcase || 'PEER'

      @n_smtp_url = Addressable::URI.new
      @n_smtp_url.host = request.params['smtp_host']&.strip&.downcase
      @n_smtp_url.port = request.params['smtp_port'].to_i
      @n_smtp_url.user = Addressable::URI.encode(request.params['smtp_user']&.strip || '')
      @n_smtp_url.password = Addressable::URI.encode(request.params['smtp_pass']&.strip || '')
      @n_smtp_url.query_values = {
        :starttls => smtp_sec == 'starttls' ? 'yes' : 'no',
        :tls => smtp_sec == 'tls' ? 'yes' : 'no',
        :ssl => smtp_sec == 'ssl' ? 'yes' : 'no',
        :openssl_verify_mode => smtp_verify,
        :authentication => smtp_auth,
      }
    rescue
      flash :error, t(:'initial_setup/newinstall/basecfg/errors/smtp_url_error')
      next render_view
    end

    # Store this data in @this_upgrade
    @data ||= {}
    @data['config-entries'] ||= {}
    @data['config-entries']['site-name'] = @n_site_name
    @data['config-entries']['org-name'] = @n_org_name
    @data['config-entries']['base-url'] = @n_base_url
    @data['config-entries']['email-from'] = @n_email_from
    @data['config-entries']['email-smtp-host'] = @n_smtp_url.to_s

    # Save upgrade data
    @this_upgrade.status = 'base-config-ok'
    @this_upgrade.encrypt(:data, JSON.generate(@data))
    @this_upgrade.save

    next redirect '/initialsetup/newinstall/create-branch'
  end
end
