class EARMMS::UserSettingsMfaController < EARMMS::ApplicationController
  helpers EARMMS::AuthMfaRecoveryHelpers

  before do
    unless logged_in?
      flash(:error, t(:'auth/login/must_log_in'))
      session[:after_login] = request.path
      next redirect '/auth'
    end

    @title = t(:'userprofile/mfa/title')

    @user = current_user
    @profile = EARMMS::Profile.for_user(@user)
    @has_totp = @user.totp_enabled
    @u2f_count = EARMMS::U2FRegistration.where(user: @user.id).count
    @has_roles = EARMMS::UserHasRole.where(user: @user.id).count.positive?
    @has_recovery = user_has_mfa_recovery?(@user)
    @enhanced = @user.enhanced_security
  end

  get '/' do
    if @has_totp
      haml :'user_settings/mfa/index_enabled'
    else
      haml :'user_settings/mfa/index_disabled'
    end
  end

  get '/disable' do
    if @has_roles
      flash :error, t(:'userprofile/mfa/disable/errors/have_roles')
      next redirect '/user/mfa'
    end

    @auth_header_hide_home = @auth_header_hide_links = true
    @auth_layout_classes = %w[auth-layout-small auth-layout-centered]
    @title = t(:'userprofile/mfa/disable/do/title')
    haml :'auth/layout', :layout => :layout_minimal do
      haml :'user_settings/mfa/disable', :layout => false
    end
  end

  post '/disable' do
    if @has_roles || @enhanced
      flash :error, t(:'userprofile/mfa/disable/errors/have_roles')
      next redirect '/user/mfa'
    end

    password = request.params['password']&.strip
    if password.nil? || password.empty?
      flash :error, t(:'userprofile/mfa/disable/errors/invalid_password')
      next redirect request.path
    end

    unless @user.verify_password(password)
      flash :error, t(:'userprofile/mfa/disable/errors/invalid_password')
      next redirect request.path
    end

    # destroy recovery codes
    EARMMS::Token.where(user: @user.id, use: 'mfa_recovery').delete

    # destroy u2f entries
    EARMMS::U2FRegistration.where(user: @user.id).delete

    # clear totp fields and save
    @user.totp_enabled = false
    @user.totp_secret = nil
    @user.save

    flash :success, t(:'userprofile/mfa/disable/success')
    next redirect '/user/mfa'
  end
end
