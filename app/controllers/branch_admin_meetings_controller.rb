require 'sanitize'

class EARMMS::BranchAdminMeetingsController < EARMMS::ApplicationController
  get '/:branchid' do |branchid|
    next halt 404 unless logged_in?
    @branch = EARMMS::Branch[branchid.to_i]
    next halt 404 unless @branch
    next halt 404 unless has_role? "branch:#{branchid}:meeting"
    @branchname = @branch.decrypt(:name)
    
    page = request.params["page"].to_i
    page = 1 if page.nil? || page.zero?
    @ds = EARMMS::BranchMeeting
      .where(branch: @branch.id)
      .order(Sequel.desc(:id))
      .paginate(page, 25)

    @meetings = @ds.map do |x|
      {
        :id => x.id,
        :datetime => DateTime.parse(x.decrypt(:datetime)),
        :location => x.decrypt(:location),
      }
    end

    @title = t(:'branch/meeting/list/title', :branch => @branchname)
    haml :'branch_admin/meetings/index'
  end

  get '/:branchid/new' do |branchid|
    next halt 404 unless logged_in?
    @branch = EARMMS::Branch[branchid.to_i]
    next halt 404 unless @branch
    next halt 404 unless has_role? "branch:#{branchid}:meeting"
    @branchname = @branch.decrypt(:name)
    @title = t(:'branch/meeting/create/title', :branch => @branchname)

    haml :'branch_admin/meetings/new'
  end

  post '/:branchid/new' do |branchid|
    next halt 404 unless logged_in?
    @branch = EARMMS::Branch[branchid.to_i]
    next halt 404 unless @branch
    next halt 404 unless has_role? "branch:#{branchid}:meeting"
    @branchname = @branch.decrypt(:name)
    @title = t(:'branch/meeting/create/title', :branch => @branchname)

    # fields
    @date = request.params["date"]&.strip&.downcase
    @time = request.params["time"]&.strip&.downcase
    @location = request.params["location"]&.strip
    @notes = request.params["notes"]&.strip
    @notes = Sanitize.fragment(@notes, Sanitize::Config::RELAXED) # Sanitize HTML

    errors = [
      @date.nil?,
      @date&.empty?,
      @time.nil?,
      @time&.empty?,
    ]
    if errors.any?
      flash :error, t(:'branch/meeting/create/errors/required_not_provided')
      next haml :'branch_admin/meetings/new'
    end

    datetime = DateTime.parse("#{@date} #{@time}")
    unless datetime
      flash :error, t(:'branch/meeting/create/errors/invalid_datetime')
      next haml :'branch_admin/meetings/new'
    end

    meeting = EARMMS::BranchMeeting.new
    meeting.branch = @branch.id
    meeting.save
    meeting.encrypt(:datetime, datetime.to_s)
    meeting.encrypt(:location, @location)
    meeting.encrypt(:notes, @notes)
    meeting.save

    if request.params["send-email"]&.strip&.downcase == "on"
      meeting.send_reminder!
    end

    flash :success, t(:'branch/meeting/create/success')
    next redirect "/branch/meetings/#{@branch.id}/#{meeting.id}"
  end

  get '/:branchid/:meetingid' do |branchid, meetingid|
    next halt 404 unless logged_in?
    @branch = EARMMS::Branch[branchid.to_i]
    next halt 404 unless @branch
    next halt 404 unless has_role? "branch:#{branchid}:meeting"
    @meeting = EARMMS::BranchMeeting[meetingid.to_i]
    next halt 404 unless @meeting
    next halt 404 unless @meeting.branch == @branch.id
    @branchname = @branch.decrypt(:name)
    @datetime = DateTime.parse(@meeting.decrypt(:datetime))
    @title = t(:'branch/meeting/view/title', :branch => @branchname, :datetime => @datetime)

    @date = @datetime.strftime("%Y-%m-%d")
    @time = @datetime.strftime("%H:%M")
    @location = @meeting.decrypt(:location)
    @notes = @meeting.decrypt(:notes)

    haml :'branch_admin/meetings/meeting'
  end

  post '/:branchid/:meetingid' do |branchid, meetingid|
    next halt 404 unless logged_in?
    @branch = EARMMS::Branch[branchid.to_i]
    next halt 404 unless @branch
    next halt 404 unless has_role? "branch:#{branchid}:meeting"
    @meeting = EARMMS::BranchMeeting[meetingid.to_i]
    next halt 404 unless @meeting
    next halt 404 unless @meeting.branch == @branch.id
    @branchname = @branch.decrypt(:name)
    @datetime = DateTime.parse(@meeting.decrypt(:datetime))
    @title = t(:'branch/meeting/view/title', :branch => @branchname, :datetime => @datetime)

    @date = @datetime.strftime("%Y-%m-%d")
    @time = @datetime.strftime("%H:%M")
    @location = @meeting.decrypt(:location)
    @notes = @meeting.decrypt(:notes)

    case request.params["action"]&.strip&.downcase
    when "edit"
      # fields
      @date = request.params["date"]&.strip&.downcase
      @time = request.params["time"]&.strip&.downcase
      @location = request.params["location"]&.strip
      @notes = request.params["notes"]&.strip
      @notes = Sanitize.fragment(@notes, Sanitize::Config::RELAXED) # Sanitize HTML

      errors = [
        @date.nil?,
        @date&.empty?,
        @time.nil?,
        @time&.empty?,
      ]
      if errors.any?
        flash :error, t(:'branch/meetings/view/edit/errors/required_not_provided')
        next haml :'branch_admin/meetings/new'
      end

      datetime = DateTime.parse("#{@date} #{@time}")
      unless datetime
        flash :error, t(:'branch/meetings/view/edit/errors/invalid_datetime')
        next haml :'branch_admin/meetings/new'
      end

      @meeting.encrypt(:datetime, @datetime)
      @meeting.encrypt(:location, @location)
      @meeting.encrypt(:notes, @notes)
      @meeting.save

      flash :success, t(:'branch/meeting/view/edit/success')

    when "send-reminder"
      if @datetime < DateTime.now
        flash :error, t(:'branch/meeting/view/send_reminder/errors/in_past')
        next haml :'branch_admin/meetings/meeting'
      end

      @meeting.send_reminder!
      flash :success, t(:'branch/meeting/view/send_reminder/success')

    when "delete"
      if !request.params.key?("confirm") || request.params["confirm"] != "on"
        flash :error, t(:'branch/meeting/view/delete/errors/confirm_not_checked')
        next haml :'branch_admin/meetings/meeting'
      end

      EARMMS::BranchMeetingAttendanceRecord.where(meeting: @meeting.id).each do |ar|
        EARMMS::BranchMeetingAttendanceRecordFilter.where(attendance_record: ar.id).delete
        ar.delete
      end

      @meeting.delete
      flash :success, t(:'branch/meeting/view/delete/success')
      next redirect "/branch/meetings/#{@branch.id}"
    end

    haml :'branch_admin/meetings/meeting'
  end
end
