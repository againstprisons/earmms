class EARMMS::AuthEmailVerifyController < EARMMS::ApplicationController
  before do
    @auth_layout_classes = %w[auth-layout-small auth-layout-centered]
    @auth_header_hide_home = @auth_header_hide_links = true

    @title = t(:'auth/email_verify/title')
  end

  get '/:token' do |token|
    token = token.strip.downcase
    @token = EARMMS::Token.where(token: token, use: 'email_verify').first
    @user = EARMMS::User[@token&.user] if @token&.user.to_i.positive?

    # check token validity, show invalid page if necessary
    unless @token&.is_valid? && @user
      next haml :'auth/layout', :layout => :layout_minimal do
        haml :'auth/email_verify/invalid', :layout => false
      end
    end

    # set verified flag
    @user.email_verified = true
    @user.save

    # invalidate the token
    @token.invalidate!

    # and render a success page
    haml :'auth/layout', :layout => :layout_minimal do
      haml :'auth/email_verify/success', :layout => false
    end
  end
end
