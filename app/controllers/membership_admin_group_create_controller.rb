class EARMMS::MembershipAdminGroupCreateController < EARMMS::MembershipAdminController
  post '/' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:group:create'

    name = request.params["name"]&.strip
    if name.nil? || name.empty?
      flash :error, t(:'membership/groups/create/errors/no_name')
      next redirect '/admin/group'
    end

    type = request.params["type"].strip
    unless %w[caucus working_group].include?(type)
      flash :error, t(:'membership/groups/create/errors/invalid_type')
      next redirect '/admin/group'
    end

    group = EARMMS::Group.new(type: type)
    group.save
    group.encrypt(:name, name)
    group.save

    flash :success, t(:'membership/groups/create/success')
    redirect "/admin/group/edit/#{group.id}"
  end
end
