class EARMMS::MembershipAdminConfigurationKaupapaController < EARMMS::MembershipAdminController
  before do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:config'

    @kaupapa_entry = EARMMS::ConfigModel.where(key: 'membership-kaupapa-prompt').first
    @info_entry = EARMMS::ConfigModel.where(key: 'membership-information').first
    
    if @kaupapa_entry.nil? || @info_entry.nil?
      flash :error, t(:'membership/configuration/kaupapa/errors/non_existent_entries')
      next redirect '/admin/config'
    end

    @title = t(:'membership/configuration/kaupapa/title')
  end

  get '/' do
    haml :'membership_admin/config/kaupapa'
  end

  post '/' do
    kaupapa_changed = false
    kaupapa_text = request.params["kaupapa"]&.strip
    kaupapa_fragment = Sanitize.fragment(kaupapa_text, Sanitize::Config::RESTRICTED).gsub("&nbsp;", " ").strip
    if kaupapa_text.nil? || kaupapa_text&.empty? || kaupapa_fragment.empty?
      # disable kaupapa prompt
      kaupapa_changed = true if @kaupapa_entry.value != ''
      @kaupapa_entry.value = ''
      @kaupapa_entry.save
    else
      kaupapa_changed = true if @kaupapa_entry.value != kaupapa_text
      @kaupapa_entry.value = kaupapa_text
      @kaupapa_entry.save
    end

    if kaupapa_changed
      unless EARMMS.app_config_refresh_pending.include?(@kaupapa_entry.key)
        EARMMS.app_config_refresh_pending << @kaupapa_entry.key
      end

      gen_alert(:system, :config_change, t(:'system/configuration/edit/change/alert', {
        :key => @kaupapa_entry.key,
        :value => @kaupapa_entry.value,
        :force_language => true,
      }))
    end

    info_changed = false
    info_text = request.params["info"]&.strip
    info_fragment = Sanitize.fragment(info_text, Sanitize::Config::RESTRICTED).gsub("&nbsp;", " ").strip
    if info_text.nil? || info_text&.empty? || info_fragment.empty?
      # disable info prompt
      info_changed = true if @info_entry.value != ''
      @info_entry.value = ''
      @info_entry.save
    else
      info_changed = true if @info_entry.value != info_text
      @info_entry.value = info_text
      @info_entry.save
    end

    if info_changed
      unless EARMMS.app_config_refresh_pending.include?(@info_entry.key)
        EARMMS.app_config_refresh_pending << @info_entry.key
      end

      gen_alert(:system, :config_change, t(:'system/configuration/edit/change/alert', {
        :key => @info_entry.key,
        :value => @info_entry.value,
        :force_language => true,
      }))
    end
    
    flash :success, t(:'membership/configuration/kaupapa/success')
  
    next redirect request.path
  end
end
