class EARMMS::AuthSignupExtendedMembershipController < EARMMS::ApplicationController
  helpers EARMMS::AuthSignupHelpers

  before do
    unless logged_in?
      flash(:error, t(:'auth/login/must_log_in'))
      session[:after_login] = request.path
      next redirect('/auth')
    end

    @current_step = "membership"
    @user = current_user
    @profile = EARMMS::Profile.for_user(@user)

    extended_signup_verify!()
    @signup_data = extended_signup_get_data()

    @info_member = EARMMS.app_config['extended-signup-member-requirements']
    @info_supporter = EARMMS.app_config['extended-signup-supporter-requirements']
    @kaupapa = EARMMS.app_config['membership-kaupapa-prompt']

    @auth_header_hide_home = @auth_header_hide_links = true
    @auth_layout_classes = %w[auth-layout-large auth-layout-centered]
    @title = t(:'auth/signup_extended/membership/title')
  end

  get '/' do
    haml :'auth/layout', :layout => :layout_minimal do
      haml :'auth/signup/extended/membership', :layout => false
    end
  end
  
  post '/' do
    status = request.params['status']&.strip&.downcase
    unless %w[member supporter].include?(status)
      flash :error, t(:'auth/signup_extended/membership/errors/invalid_status')
      next redirect request.path
    end

    if @kaupapa && %w[member].include?(status)
      accept = request.params['accept'].to_i == 1
      unless accept
        @title = t(:'auth/signup_extended/membership/accept_kaupapa/title')
        next haml :'auth/layout', :layout => :layout_minimal do
          haml :'auth/signup/extended/membership_kaupapa', :layout => false
        end
      end
    end

    # Save data
    @signup_data["status"] = status
    @user.encrypt(:signup_extended_status_data, JSON.generate(@signup_data))
    @user.signup_extended_status = "branch"
    @user.save

    redirect extended_signup_current_step_url()
  end
end

