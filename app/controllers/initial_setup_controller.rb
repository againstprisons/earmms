class EARMMS::InitialSetupController < EARMMS::ApplicationController
  helpers EARMMS::InitialSetupHelpers

  before do
    @title = t(:'initial_setup/title')

    next halt 404 unless logged_in?
    next halt 404 unless has_role?('system:access')

    @this_upgrade = EARMMS::VersionUpgradeModel.find_or_create(upgrade_id: EARMMS::VERSION_UPGRADE_ID) do |vu|
      vu.mode = nil
      vu.status = 'not-started'
      vu.data = nil
    end

    if @this_upgrade.status == 'complete'
      next redirect '/'
    end

    allowed_without_mutex = [
      current?("/initialsetup"),
      current?("/initialsetup/discard"),
      current?("/initialsetup/force-acquire"),
      current?("/initialsetup/debugging"),
    ].none?

    @mutex = EARMMS::MutexModel.acquire!(current_user.id, 'initial_setup', no_new: true)
    if !(@mutex[:mine]) && allowed_without_mutex
      if @mutex[:new] == :no_new
        @mutex = EARMMS::MutexModel.acquire!(current_user.id, 'initial_setup')
        if @mutex[:mine]
          if @mutex[:new] == :new
            flash :success, t(:'initial_setup/lock/acquired')
          end

        else
          flash :error, t(:'initial_setup/lock/errors/unsuccessful')

          @auth_header_hide_home = @auth_header_hide_links = true
          @auth_layout_classes = %w[auth-layout-small auth-layout-centered]
          out = haml(:'auth/layout', layout: :layout_minimal) { "" }
          halt out
        end

      else
        unless @mutex[:mine]
          @auth_header_hide_home = @auth_header_hide_links = true
          @auth_layout_classes = %w[auth-layout-medium auth-layout-centered]
          out = haml(:'auth/layout', :layout => :layout_minimal) do
            haml :'initial_setup/locked', :layout => false
          end

          halt out
        end
      end
    end

    if allowed_without_mutex
      if @this_upgrade.status.nil? || @this_upgrade.status == 'not-started'
        unless current?("/initialsetup/newinstall") || current?("/initialsetup/upgrade")
          next redirect '/initialsetup'
        end
      elsif !current?(current_step_url())
        next redirect current_step_url()
      end
    elsif current?('/initialsetup')
      unless @this_upgrade.status.nil? || @this_upgrade.status == 'not-started'
        next redirect current_step_url()
      end
    end

    @has_v1_keys = [
      EARMMS::ConfigModel.where(key: 'kaupapa-prompt').count.positive?,
      EARMMS::ConfigModel.where(key: 'meeting-reminder-secs').count.positive?,
      EARMMS::ConfigModel.where(key: 'prune-old-work-queue-entries').count.positive?,
      EARMMS::ConfigModel.where(key: 'email-to-self').count.positive?,
    ].select{|x| x}.count > 2
  end
end
