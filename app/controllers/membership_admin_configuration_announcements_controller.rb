class EARMMS::MembershipAdminConfigurationAnnouncementsController < EARMMS::MembershipAdminController
  before do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:config:announcements:access'

    @all_announcements = EARMMS::Announcement.all
    @title = t(:'membership/configuration/announcements/title')
  end

  get '/' do
    haml :'membership_admin/config/announcements'
  end

  post '/toggle' do
    next halt 404 unless has_role?("admin:config:announcements:modify")
    this_an = EARMMS::Announcement[request.params['id'].to_i]
    unless this_an
      flash :error, t(:'membership/configuration/announcements/errors/invalid_id')
      next redirect '/admin/config/announcements'
    end

    this_an.visible = !this_an.visible
    this_an.save

    flash :success, t("membership/configuration/announcements/#{this_an.visible ? 'enable' : 'disable'}/success".to_sym)
    next redirect '/admin/config/announcements'
  end

  post '/delete' do
    next halt 404 unless has_role?("admin:config:announcements:modify")
    this_an = EARMMS::Announcement[request.params['id'].to_i]
    unless this_an
      flash :error, t(:'membership/configuration/announcements/errors/invalid_id')
      next redirect '/admin/config/announcements'
    end

    this_an.delete

    flash :success, t(:'membership/configuration/announcements/delete/success', :id => this_an.id)
    next redirect '/admin/config/announcements'
  end

  post '/create' do
    next halt 404 unless has_role?("admin:config:announcements:create")

    text = request.params['message']&.strip
    if text.nil? || text.empty?
      flash :error, t(:'membership/configuration/announcements/create/errors/no_text')
      next redirect '/admin/config/announcements'
    end

    only_logged_in = request.params['only_logged_in']&.strip&.downcase == 'on'

    this_an = EARMMS::Announcement.new(message: text, only_logged_in: only_logged_in, visible: false)
    this_an.save

    flash :success, t(:'membership/configuration/announcements/create/success', :id => this_an.id)
    next redirect '/admin/config/announcements'
  end
end
