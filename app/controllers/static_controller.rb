require 'sassc'

class EARMMS::StaticController < EARMMS::ApplicationController
  VENDOR_WHITELIST = [
    "/purecss/build",
    "/font-awesome/css",
    "/font-awesome/fonts",
    "/zxcvbn/dist",
  ]

  configure do
    scsspath = File.join(EARMMS.root, "assets", "scss")
    viewspath = File.join(EARMMS.root, "app", "views")

    set(:views, {
      :scss => scsspath,
      :default => viewspath
    })
  end

  get '/css/:name.css' do |name|
    name = name.to_sym
    style = :compressed
    style = :nested if settings.development?

    content_type 'text/css'
    begin
      scss name, style: style
    rescue Errno::ENOENT
      "/* 404  */"
    end
  end

  get '/vendor/*' do
    fn = File.expand_path(params['splat'].first, "/")
    next 404 unless VENDOR_WHITELIST.map{|x| fn.start_with?(x)}.any?
    path = File.join(EARMMS.root, "node_modules", fn)
    next 404 unless File.file? path
    send_file path
  end

  get '/*' do
    fn = File.expand_path(params['splat'].first, "/")
    path = File.join(EARMMS.root, "public", fn)
    if EARMMS.theme_dir
      themepath = File.join(EARMMS.theme_dir, "public", fn)
      path = themepath if File.file? themepath
    end

    next 404 unless File.file? path
    send_file path
  end
end
