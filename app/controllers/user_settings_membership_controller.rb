class EARMMS::UserSettingsMembershipController < EARMMS::ApplicationController
  before do
    unless logged_in?
      flash(:error, t(:'auth/login/must_log_in'))
      session[:after_login] = request.path
      next redirect('/auth')
    end
    
    @kaupapa_prompt = EARMMS.app_config['membership-kaupapa-prompt']
    @membership_info = EARMMS.app_config['membership-information']
    @branches = EARMMS::Branch.all.map do |br|
      [
        br.id,
        {
          :id => br.id,
          :name => br.decrypt(:name),
        }
      ]
    end.to_h

    @user = current_user
    @profile = EARMMS::Profile.for_user(@user)
    @status = @profile.decrypt(:membership_status)
    @branch = EARMMS::Branch[@profile.decrypt(:branch).to_i]
    @branch_name = @branch&.decrypt(:name)

    @pending_status = EARMMS::Alert[@profile.decrypt(:status_change_alert).to_i]
    if @pending_status
      d = JSON.parse(@pending_status.decrypt(:data))
      @pending_status = {
        :obj => @pending_status,
        :from => d['from_status'],
        :to => d['to_status'],
      }
    end

    @pending_branch = EARMMS::Alert[@profile.decrypt(:branch_change_alert).to_i]
    if @pending_branch
      d = JSON.parse(@pending_branch.decrypt(:data))
      from_branch = EARMMS::Branch[d['from_branch_id'].to_i]
      to_branch = EARMMS::Branch[d['to_branch_id'].to_i]
      @pending_branch = {
        :obj => @pending_branch,
        :from => {
          :branch => from_branch,
          :name => @branches.key?(from_branch.id) ? @branches[from_branch.id][:name] : t(:'unknown'),
        },
        :to => {
          :branch => to_branch,
          :name => @branches.key?(to_branch.id) ? @branches[to_branch.id][:name] : t(:'unknown'),
        },
      }
    end
  end

  get '/' do
    @title = t(:'userprofile/membership/title')
    haml :'user_settings/membership/index'
  end
  
  get '/membership-info' do
    unless @status == 'member' || (@pending_status ? @pending_status[:to] == 'member' : false)
      next halt 404
    end

    @title = t(:'userprofile/membership/membership_info/title')
    @auth_layout_classes = %w[auth-layout-large auth-layout-centered]
    next haml :'auth/layout', :layout => :layout_minimal do
      haml :'user_settings/membership/information', :layout => false
    end
  end

  post '/change-status' do
    to_status = (@status == "member" ? "supporter" : "member")

    if @pending_status
      flash :error, t(:'userprofile/membership/change_status/errors/pending')
      next redirect "/user/membership"
    end

    if to_status == "member" && @kaupapa_prompt != nil
      unless request.params["kaupapa-agree"]&.strip&.downcase == "on"
        flash :error, t(:'userprofile/membership/change_status/errors/kaupapa')
        next redirect "/user/membership"
      end
    end

    @pending_status = gen_alert(:membership, :user_status_change_req, JSON.dump({
      "from_status" => @status,
      "to_status" => to_status,
    }))
    @profile.encrypt(:status_change_alert, @pending_status.id)
    @profile.save

    flash :success, t(:'userprofile/membership/change_status/success', :to_status => to_status)
    
    if to_status == 'member' && @membership_info
      next redirect '/user/membership/membership-info'
    end
    
    redirect "/user/membership"
  end

  post '/change-branch' do
    to_branch = EARMMS::Branch[request.params["branch"].to_i]
    next halt 400 unless to_branch
    to_branch_name = @branches[to_branch.id][:name]

    if @pending_branch
      flash :error, t(:'userprofile/membership/change_branch/errors/pending')
      next redirect "/user/membership"
    end

    if @branch.id == to_branch.id
      flash :error, t(:'userprofile/membership/change_branch/errors/same_branch')
      next redirect "/user/membership"
    end

    @pending_branch = gen_alert(:membership, :user_branch_change_req, JSON.dump({
      "from_branch" => @branch.decrypt(:name),
      "from_branch_id" => @branch.id,
      "to_branch" => to_branch_name,
      "to_branch_id" => to_branch.id,
    }))
    @profile.encrypt(:branch_change_alert, @pending_branch.id)
    @profile.save

    flash :success, t(:'userprofile/membership/change_branch/success', :branch => to_branch_name)
    next redirect "/user/membership"
  end
end
