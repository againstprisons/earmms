require 'chronic'

class EARMMS::InitialSetupNewInstallApplyController < EARMMS::InitialSetupController
  get '/' do
    @data = JSON.parse(@this_upgrade.decrypt(:data))
    @configs = newinstall_cfg_entries(@data['config-entries']).map{|k,v| v}.flatten
    @output = @configs.map do |desc|
      key = desc[:key]
      value = desc[:value]
      if desc[:type] == :bool
        value = (value ? 'yes' : 'no')
      end
      value = value.to_s

      parsed = value
      warnings = []
      stop = false
      EARMMS::Config.parsers.each do |parser|
        if !stop && parser.accept?(key, desc[:type])
          out = parser.parse(parsed)
          warnings << out[:warning] if out[:warning]
          parsed = out[:data]
          stop = out[:stop_processing_here]
        end
      end

      {
        :key => key,
        :warnings => warnings,
        :source => desc[:source],
      }
    end.compact

    # Do we have warnings?
    @has_warnings = @output.map{|x| x[:warnings].count.positive?}.any?
    @default_warnings = @output.map do |x|
      if x[:source] != :wizard && x[:warnings].count.positive?
        x
      else
        nil
      end
    end.compact

    # Generate a verification code and store it in the session if one
    # doesn't already exist there
    if session.key?(:initial_setup_newinstall_apply_code)
      @verify_code = session[:initial_setup_newinstall_apply_code]
    else
      @verify_code = Random.new.rand(100000000 .. 999999999).to_s
      session[:initial_setup_newinstall_apply_code] = @verify_code
    end

    # Render
    haml :'initial_setup/layout', :layout => :layout_minimal do
      haml :'initial_setup/newinstall/layout', :layout => false do
        haml :'initial_setup/newinstall/apply', :layout => false
      end
    end
  end

  post '/' do
    @data = JSON.parse(@this_upgrade.decrypt(:data))

    # Check verification code
    form_verify = request.params['verify']&.strip
    if form_verify
      form_verify = form_verify.split(' ').map{|x| x.split('-')}.flatten.join('')
    end
    if form_verify != session[:initial_setup_newinstall_apply_code]
      flash :error, t(:'initial_setup/newinstall/apply/errors/invalid_code')
      next redirect request.path
    end
    session.delete(:initial_setup_newinstall_apply_code)

    # apply user role changes
    @data['roles'].each do |uid, roledata|
      user = EARMMS::User[uid.to_i]
      if user
        roledata['remove'].each do |role|
          EARMMS::UserHasRole.where(:user => user.id, :role => role).delete
        end

        roledata['add'].each do |role|
          EARMMS::UserHasRole.new(:user => user.id, :role => role).save
        end
      end
    end

    # drop all existing config entries
    EARMMS::ConfigModel.select.delete

    # create new config entries
    @configs = newinstall_cfg_entries(@data['config-entries']).map{|k,v| v}.flatten
    @configs.each do |cfg|
      entry = EARMMS::ConfigModel.new({
        :key => cfg[:key].to_s,
        :type => cfg[:type].to_s,
        :value => cfg[:value].to_s,
      })

      entry.save
    end

    # create branch (if needed)
    if @data['first-branch']['create'] == true
      branch = EARMMS::Branch.new
      branch.save
      branch.encrypt(:name, @data['first-branch']['name'])
      branch.save
      
      cfgentry = EARMMS::ConfigModel.find_or_create(key: 'membership-default-branch') do |e|
        e.type = EARMMS::APP_CONFIG_ENTRIES['membership-default-branch'][:type]
      end
        
      cfgentry.value = branch.id.to_s
      cfgentry.save
    end

    # drop all existing work queue entries
    EARMMS::WorkQueue.select.delete

    # create new work queue entries
    @data['job-queue'].each do |qdata|
      ts = Chronic.parse(qdata['at'])
      entry = EARMMS::WorkQueue.new({
        :status => 'queued',
      }).save

      entry.encrypt(:task, qdata['name'])
      entry.encrypt(:data, '{}')
      entry.encrypt(:run_at, ts.to_s)
      entry.save
    end

    # save upgrade data
    @this_upgrade.encrypt(:data, JSON.generate(@data))
    @this_upgrade.status = 'apply-ok'
    @this_upgrade.save
    next redirect current_step_url
  end
end
