class EARMMS::AuthSignupExtendedApplyController < EARMMS::ApplicationController
  helpers EARMMS::AuthSignupHelpers

  before do
    unless logged_in?
      flash(:error, t(:'auth/login/must_log_in'))
      session[:after_login] = request.path
      next redirect('/auth')
    end

    @current_step = "apply"
    @user = current_user
    @profile = EARMMS::Profile.for_user(@user)

    extended_signup_verify!()
    @signup_data = extended_signup_get_data()

    @auth_header_hide_home = @auth_header_hide_links = true
    @auth_layout_classes = %w[auth-layout-medium auth-layout-centered]
    @title = t(:'auth/signup_extended/apply/title')
  end

  get '/' do
    @signup_full_name = @signup_data['fields']['db']['full_name']
    @signup_membership_status = @signup_data['status']
    @signup_branch = EARMMS::Branch[@signup_data['branch'].to_i]
    @signup_branch_name = @signup_branch&.decrypt(:name) ||  t(:'unknown')

    haml :'auth/layout', :layout => :layout_minimal do
      haml :'auth/signup/extended/apply', :layout => false
    end
  end
  
  post '/' do
    # Save membership status request
    if %w[member].include?(@signup_data['status'])
      pending_status = gen_alert(:membership, :user_status_change_req, JSON.dump({
        "from_status" => @profile.decrypt(:membership_status),
        "to_status" => @signup_data['status'],
      }))

      @profile.encrypt(:status_change_alert, pending_status.id)
    elsif %w[supporter].include?(@signup_data['status'])
      @profile.encrypt(:membership_status, @signup_data['status'])
    end

    # Save branch affiliation request
    current_branch = EARMMS::Branch[@profile.decrypt(:branch).to_i]
    current_branch = EARMMS::Branch[EARMMS.app_config['membership-default-branch'].to_i] if current_branch.nil? 
    if @signup_data['branch'].to_i == EARMMS.app_config['membership-default-branch'].to_i
      @profile.encrypt(:branch, EARMMS.app_config['membership-default-branch'].to_s)
    else
      to_branch = EARMMS::Branch[@signup_data['branch'].to_i]
      pending_branch = gen_alert(:membership, :user_branch_change_req, JSON.dump({
        "from_branch" => current_branch.decrypt(:name),
        "from_branch_id" => current_branch.id,
        "to_branch" => to_branch.decrypt(:name),
        "to_branch_id" => to_branch.id,
      }))

      @profile.encrypt(:branch_change_alert, pending_branch.id)
    end

    # Save fields
    @profile.encrypt(:custom_fields, JSON.generate(@signup_data['fields']['custom']))
    @signup_data['fields']['db'].each do |name, val|
      @profile.encrypt(name, val)
    end
    
    # Save profile object and refresh filters
    @profile.save
    EARMMS::ProfileFilter.clear_filters_for(@profile)
    EARMMS::ProfileFilter.create_filters_for(@profile)

    # Save user object
    @user.signup_extended_status = "complete"
    @user.signup_extended_status_data = nil
    @user.save

    # This will redirect to "/auth/signup/ex/complete" which just redirects
    # to the dashboard.
    redirect extended_signup_current_step_url()
  end
end


