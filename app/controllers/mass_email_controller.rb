require 'sanitize'

class EARMMS::MassEmailController < EARMMS::ApplicationController
  helpers EARMMS::MassEmailHelpers

  before do
    next halt 404 unless logged_in?
    next halt 404 unless has_role?('email:access') || is_group_admin?(:include_email_privs => true)

    @title = t(:'massemail/title')
    @targets = get_email_targets
    if @targets.empty?
      halt haml :'mass_email/no_targets'
    end
  end
  
  get '/' do
    @selected = @targets[0][:target]
    @text = @subject = ""

    query_target = request.params['target']&.strip&.downcase
    if !(query_target.nil? || query_target.empty?)
      if @targets.map{|x| x[:target]}.include?(query_target)
        @selected = query_target
      end
    end

    @subject = session.delete(:mass_email_subject) if session.key?(:mass_email_subject)
    @text = session.delete(:mass_email_text) if session.key?(:mass_email_text)

    haml :'mass_email/index'
  end

  post '/' do
    has_error = false

    @selected = request.params["target"]&.strip&.downcase
    if @targets.select{|x| x[:target] == @selected}.empty?
      has_error = true
      flash :error, t(:'massemail/errors/invalid_target')
    end

    @subject = request.params["subject"]&.strip
    if @subject.nil? || @subject.empty?
      has_error = true
      flash :error, t(:'massemail/errors/no_subject')
    end

    @text = request.params["text"]&.strip
    if @text.nil? || @text.empty?
      has_error = true
      flash :error, t(:'massemail/errors/no_text')
    end

    if has_error || request.params["reedit"]&.strip&.downcase == "on"
      next haml :'mass_email/index'
    end

    haml :'mass_email/confirm'
  end

  post '/send' do
    has_error = false

    @selected = request.params["target"]&.strip&.downcase
    if @targets.select{|x| x[:target] == @selected}.empty?
      has_error = true
      flash :error, t(:'massemail/errors/invalid_target')      
    end

    @subject = request.params["subject"]&.strip
    if @subject.nil? || @subject.empty?
      has_error = true
      flash :error, t(:'massemail/errors/no_subject')
    end

    @text = request.params["text"]&.strip
    if @text.nil? || @text.empty?
      has_error = true
      flash :error, t(:'massemail/errors/no_text')
    end

    if has_error
      next haml :'mass_email/index'
    end

    @footer = ["Please do not reply to this email."]

    # Sanitize HTML
    @text = [Sanitize.fragment(@text, Sanitize::Config::RELAXED)]
    @text_raw = @text.first

    # Gather list of emails to send to
    users = []
    t = @selected.split(":")
    case t[0]
    when "branch"
      status = t[2]
      branch = EARMMS::Branch[t[1].to_i]
      next halt 400 unless branch

      @footer << "You have been sent this email because you are a #{t("membership_status/#{status}".to_sym)} of the #{org_name} branch: #{ERB::Util.html_escape(branch.decrypt(:name).inspect)}."

    when "group"
      id = t[1]
      group = EARMMS::Group[id.to_i]
      next halt 400 unless group

      if has_role?("email:all_groups")
        has_perms = true

      else
        has_perms = EARMMS::GroupMemberFilter.perform_filter(:user, current_user.id.to_s).map do |cmf|
          cm = EARMMS::GroupMember[cmf.group_member]
          next nil unless cm

          group = EARMMS::Group[cm.group]
          next nil unless group

          roles = cm.decrypt(:roles).split(",")
          next nil unless roles.include?("email") || roles.include?("admin")

          next true
        end.compact.all?
      end

      next halt 400 unless has_perms
      @footer << "You have been sent this email because you are a member of the #{org_name} group: #{ERB::Util.html_escape(group.decrypt(:name).inspect)}."

    when "status"
      status = t[1].downcase
      @footer << "You have been sent this email because you are a #{t("membership_status/#{status}".to_sym)} of #{org_name}."

    when "all"
      @footer << "You have been sent this email because you have an account on #{site_name}."

    else
      next halt 400
    end

    # add global email header
    unless EARMMS.app_config['mass-email-header'].nil?
      val = Sanitize.fragment(EARMMS.app_config['mass-email-header'], Sanitize::Config::RESTRICTED)
      if !(val.gsub("&nbsp;", "").strip.empty?()) || EARMMS.app_config['mass-email-header'].include?('<div')
        @text.unshift(EARMMS.app_config['mass-email-header'])
      end
    end

    # add global email footer
    unless EARMMS.app_config['mass-email-footer'].nil?
      val = Sanitize.fragment(EARMMS.app_config['mass-email-footer'], Sanitize::Config::RESTRICTED)
      if !(val.gsub("&nbsp;", "").strip.empty?()) || EARMMS.app_config['mass-email-footer'].include?('<div')
        @text << "<hr>"
        @text << EARMMS.app_config['mass-email-footer']
      end
    end

    if @footer.count.positive?
      @text << "<hr>"
      @footer.each do |line|
        @text << "<p>#{line}</p>"
      end
    end

    @text = @text.join("\n")

    is_draft = request.params['save-draft']&.strip&.downcase == "on"
    d = EARMMS::MassEmail.new(creation: Time.now, user: current_user.id, is_draft: is_draft)
    d.save
    d.encrypt(:target, @selected)
    d.encrypt(:subject, @subject)
    d.encrypt(:content, @text)
    d.encrypt(:content_raw, @text_raw)
    d.save

    if is_draft
      @email = d
      next haml :'mass_email/saved_draft'
    end

    d.view_token_generate!
    d.save

    plain_text = (
      "Sorry, but this email can only be viewed in HTML mode. " +
      "To view this email, either allow HTML email in your email client, or " +
      "visit the following link in a browser: " +
      d.view_token_link
    )

    viewlink_header = (
      '<div style="display:block;width:100%;text-align:center;font-size:0.8em;">' +
      "<a style=\"color:grey !important;\" href=\"#{d.view_token_link}\">" +
      'To view this email in a browser, click here.' +
      '</a></div>'
    )

    account_delete_link = Addressable::URI.parse(EARMMS.app_config['base-url'])
    account_delete_link += '/auth/account-delete'
    account_delete_footer = (
      'To unsubscribe from these emails, ' +
      "<a href=\"#{account_delete_link}\">" +
      'you can delete your account by clicking here.' +
      '</a>'
    )

    content_html = [
      viewlink_header,
      @text,
      account_delete_footer
    ]

    qm = EARMMS::EmailQueue.new(creation: Time.now)
    qm.save
    qm.queue_status = 'queued'
    qm.encrypt(:recipients, JSON.generate({"type" => "mass_email_target", "target" => @selected}))
    qm.encrypt(:subject, @subject)
    qm.encrypt(:content, plain_text)
    qm.encrypt(:content_html, content_html.join("\n\n"))
    qm.save

    d.email_queue_id = qm.id
    d.save

    haml :'mass_email/success'
  end

  get '/existing' do
    @existing = get_existing_emails(current_user)
    @previous_emails = @existing.select{|x| !x[:is_draft]}
    @draft_emails = @existing.select{|x| x[:is_draft]}

    @title = t(:'massemail/existing/title')
    haml :'mass_email/existing'
  end

  get '/view/:id' do |id|
    @email = EARMMS::MassEmail[id.to_i]
    next halt 404 unless @email

    target = @email.decrypt(:target)
    target_h = @targets.select{|x| x[:target] == target}.first
    target_name = t(:'unknown')
    target_name = target_h[:name] if target_h && target_h.key?(:name)

    queued = EARMMS::EmailQueue[@email.email_queue_id]
    queue_status = 'unknown'
    queue_status = queued.queue_status if queued
    queue_status = 'draft' if @email.is_draft

    @data = {
      :id => id.to_i,
      :created => @email.creation,
      :target => target,
      :target_name => target_name,
      :subject => @email.decrypt(:subject),
      :content => @email.decrypt(:content),
      :is_draft => @email.is_draft,
      :queue_status => t("massemail/queue_status/#{queue_status}".to_sym),
    }

    haml :'mass_email/view'
  end

  post '/delete' do
    @email = EARMMS::MassEmail[request.params['email_id'].to_i]
    next halt 404 unless @email
    next halt 400 unless @email.is_draft

    @email.delete

    flash :success, t(:'massemail/delete/success', :email_id => @email.id)
    redirect '/massemail'
  end

  post '/from-params' do
    target = request.params['target']&.strip&.downcase
    session[:mass_email_subject] = request.params['subject']&.strip
    session[:mass_email_text] = request.params['text']&.strip

    uri = Addressable::URI.parse('/massemail')
    uri.query_values = {target: target}
    redirect uri.to_s
  end

  post '/load-existing' do
    @email = EARMMS::MassEmail[request.params['email_id'].to_i]
    next halt 404 unless @email

    target = @email.decrypt(:target)
    subject = @email.decrypt(:subject)
    unless @email.content_raw.nil? || @email.content_raw.empty?
      content = @email.decrypt(:content_raw)
    else
      content = @email.decrypt(:content)
    end

    session[:mass_email_subject] = subject
    session[:mass_email_text] = content

    uri = Addressable::URI.parse('/massemail')
    uri.query_values = {target: target}
    redirect uri.to_s
  end
end
