require 'email_validator'

class EARMMS::MembershipAdminUserEditController < EARMMS::MembershipAdminController
  helpers EARMMS::MembershipAdminUserEditHelpers

  before do
    @fields = field_desc()
  end

  get '/:uid' do |uid|
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:user:edit'

    @user = EARMMS::User[uid.to_i]
    next halt 404 unless @user
    @profile = EARMMS::Profile.for_user(@user)
    next halt 404 unless @profile

    @user_name = @profile.decrypt(:full_name)
    @user_is_nil = @user.email == nil
    @values = @profile.field_hash
    @title = t(:'membership/user/edit/title', :user => @user_name)

    @membership_status = @profile.decrypt(:membership_status).strip.downcase
    @membership_status = nil unless %w[member supporter].include?(@membership_status)
    @status_change_req = @profile.decrypt(:status_change_alert).strip.to_i
    @status_change_req = EARMMS::Alert[@status_change_req] unless @status_change_req == nil
    @branch_change_req = @profile.decrypt(:branch_change_alert).strip.to_i
    @branch_change_req = EARMMS::Alert[@branch_change_req] unless @branch_change_req == nil

    @branches = EARMMS::Branch.all.map do |b|
      [
        b.id.to_s,
        {
          :id => b.id,
          :name => b.decrypt(:name),
        }
      ]
    end.to_h

    @branch = @profile.decrypt(:branch).strip.to_i
    @branch = nil unless @branches.keys.include?(@branch.to_s)

    @summary = [
      {
        :name => :userid,
        :data => @user ? @user.id : 'unknown',
      },
      {
        :name => :created,
        :data => @user ? @user.creation.strftime("%Y-%m-%d %H:%M") : 'unknown',
      },
      {
        :name => :roles,
        :data => @user ? EARMMS::UserHasRole.where(:user => @user.id).count : 0,
      },
    ]

    haml :'membership_admin/user/edit'
  end

  post '/:uid' do |uid|
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:user:edit'
    next halt 400, "no action" unless request.params.key?("action")

    @user = EARMMS::User[uid.to_i]
    next halt 404 unless @user
    @profile = EARMMS::Profile.for_user(@user)
    next halt 404 unless @profile

    @user_name = @profile.decrypt(:full_name)
    @user_is_nil = @user.email == nil
    @values = @profile.field_hash

    @membership_status = @profile.decrypt(:membership_status).strip.downcase
    @membership_status = nil unless %w[member supporter].include?(@membership_status)
    @status_change_req = @profile.decrypt(:status_change_alert).strip.to_i
    @status_change_req = EARMMS::Alert[@status_change_req] unless @status_change_req == nil
    @branch_change_req = @profile.decrypt(:branch_change_alert).strip.to_i
    @branch_change_req = EARMMS::Alert[@branch_change_req] unless @branch_change_req == nil

    @branches = EARMMS::Branch.all.map do |b|
      [
        b.id.to_s,
        {
          :id => b.id,
          :name => b.decrypt(:name),
        }
      ]
    end.to_h

    case request.params["action"].strip.downcase
    when "profile_edit"
      required = @fields.reject{|f| !f[:required]}
      has_all_required = required.map do |f|
        next false unless request.params.key?(f[:name])
        next false unless request.params[f[:name]]
        true
      end.all?

      unless has_all_required
        flash(:error, t(:'membership/user/edit/errors/required_not_provided'))
        next redirect request.path
      end

      cjson = @profile.decrypt(:custom_fields)
      if cjson.nil? || cjson.empty?
        cjson = {}
      else
        cjson = JSON.parse(cjson)
      end

      @fields.each do |field|
        next unless request.params.key?(field[:name])
        if field[:is_custom]
          cjson[field[:name]] = request.params[field[:name]]
        else
          @profile.encrypt(field[:name], request.params[field[:name]])
        end
      end

      @profile.encrypt(:custom_fields, JSON.dump(cjson))
      @profile.save

      flash :success, t(:'membership/user/edit/profile/success')

    when "membership_edit"
      oldstatus = @profile.decrypt(:membership_status)
      status = request.params["status"]&.strip&.downcase

      # XXX: change this for multiple membership statuses?
      if status.nil? || status.empty? || !(%w[member supporter].include?(status))
        flash :error, t(:'membership/user/edit/membership_status/errors/invalid', :status => status)
        next redirect request.path
      end

      # Save new status to profile
      @profile.encrypt(:membership_status, status.to_s)
      @profile.save

      # Create changelog entry
      changelog = EARMMS::ProfileChangelog.new(profile: @profile.id, created: Time.now)
      changelog.save
      changelog.encrypt(:data, JSON.dump({
        :type => 'membership_status',
        :from => oldstatus.to_s,
        :to => status.to_s,
      }))
      changelog.save

      # If member, and the membership welcome email is enabled, queue sending that email
      # XXX: change this for multiple membership statuses?
      if EARMMS.app_config['membership-welcome-email-enabled'] && %w[member].include?(status)
        email_subject = EARMMS.app_config['membership-welcome-email-subject']
        email_content = EARMMS.app_config['membership-welcome-email-body']
        email_content_plain = Sanitize.fragment(email_content.gsub("</p>", "\n</p>\n"))

        qm = EARMMS::EmailQueue.new(creation: Time.now)
        qm.save
        qm.queue_status = 'queued'
        qm.encrypt(:recipients, JSON.generate({"type" => "list", "list" => [@user.email]}))
        qm.encrypt(:subject, email_subject)
        qm.encrypt(:content, email_content_plain)
        qm.encrypt(:content_html, email_content)
        qm.save

        flash :success, t(:'membership/user/edit/membership_status/success/welcome_email', queue_id: qm.id)
      end

      # Dismiss status change request if presen
      if @status_change_req
        @profile.status_change_alert = nil
        @profile.save
        @status_change_req.dismiss!
        @status_change_req = nil

        flash :success, t(:'membership/user/edit/membership_status/pending/dismissed')
      end

      flash :success, t(:'membership/user/edit/membership_status/success', :status => status, :oldstatus => oldstatus)

    when "branch_edit"
      oldbranch = @profile.decrypt(:branch)&.to_i
      newbranch = request.params["branch"]&.strip.to_i
      if newbranch.zero? || !(@branches.keys.include?(newbranch.to_s))
        flash :error, t(:'membership/user/edit/branch/errors/invalid_branch', :bid => newbranch)
        next redirect request.path
      end

      # Save new branch to profile
      @profile.encrypt(:branch, newbranch.to_s)
      @profile.save

      # Create changelog entry
      changelog = EARMMS::ProfileChangelog.new(profile: @profile.id, created: Time.now)
      changelog.save
      changelog.encrypt(:data, JSON.dump({
        :type => 'branch_change',
        :from => oldbranch.to_s,
        :to => newbranch.to_s,
      }))
      changelog.save

      # Alert branch admins about the user joining/leaving their branch
      alert_branch_admin_user_change(@profile, oldbranch, newbranch)

      # Dismiss branch change request if there is one
      if @branch_change_req
        @profile.branch_change_alert = nil
        @profile.save
        @branch_change_req.dismiss!
        @branch_change_req = nil

        flash :success, t(:'membership/user/edit/branch/pending/dismissed')
      end

      flash :success, t(:'membership/user/edit/branch/success', :branch => @branches[newbranch.to_s][:name], :branchid => newbranch)

    when "change_email"
      email = request.params["email"]&.strip&.downcase
      if email.nil? || email.empty?
        flash :error, t:(:'membership/user/edit/errors/required_not_provided')
        next redirect request.path
      end

      unless EmailValidator.valid?(email)
        flash :error, t(:'membership/user/edit/email_address/errors/invalid_email')
        next redirect request.path
      end

      if EARMMS::User.where(email: email).count.positive?
        flash :error, t(:'membership/user/edit/email_address/errors/exists')
        next redirect request.path
      end

      oldemail = @user.email
      @user.email = email
      @user.save

      gen_alert :membership, :admin_force_edit_user, t(:'membership/user/edit/email_address/alert', {
        :name => @user_name,
        :uid => @user.id,
        :email => email,
        :oldemail => oldemail,
        :force_language => true
      })

      flash :success, t(:'membership/user/edit/email_address/success', :email => email, :oldemail => oldemail)

    else
      flash :error, t(:'membership/user/edit/errors/invalid_action')
      next redirect request.path
    end

    EARMMS::ProfileFilter.clear_filters_for(@profile)
    EARMMS::ProfileFilter.create_filters_for(@profile)
    next redirect request.path
  end
end
