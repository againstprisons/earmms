require 'base64'
require 'json'
require 'addressable'

class EARMMS::InitialSetupUpgradeController < EARMMS::InitialSetupController
  CONFIG_MIGRATE_VERSION = 1

  before do
    if @has_v1_keys
      @this_upgrade.mode = 'upgrade'
      @this_upgrade.save
    else
      next halt 404
    end
  end

  helpers do
    def render_view
      haml :'initial_setup/layout', :layout => :layout_minimal do
        haml :'initial_setup/upgrade/layout', :layout => false do
          haml :'initial_setup/upgrade/index', :layout => false
        end
      end
    end
  end

  get '/' do
    render_view
  end

  post '/' do
    # Get blob from request
    @raw_blob = request.params['configblob']&.strip
    if @raw_blob.nil? || @raw_blob.empty?
      flash :error, t(:'initial_setup/upgrade/blob/errors/invalid_blob')
      next render_view
    end

    # Parse blob
    begin
      @blob = JSON.parse(Base64.decode64(@raw_blob))
    rescue
      flash :error, t(:'initial_setup/upgrade/blob/errors/invalid_blob')
      next render_view
    end

    # Verify blob. The parsed blob will have a 'generator' key, which is what
    # we want to look at here. 'generator' is a two-key array, the first value
    # should always be 'v1-config-migrate' and the second key is a version
    # number. We need to make sure that we know what to do with the rest of the
    # blob, and checking the version number here means that we know what the
    # migrator output looks like.
    #
    # Currently, the migrator version should be `1`. This is also defined in
    # the CONFIG_MIGRATE_VERSION constant at the top of this controller.
    if @blob.key?('generator') && @blob['generator'].respond_to?(:[])
      if @blob['generator'][0] != 'v1-config-migrate'
        flash :error, t(:'initial_setup/upgrade/blob/errors/invalid_blob')
        next render_view
      end

      if @blob['generator'][1] != 1
        flash :error, t(:'initial_setup/upgrade/blob/errors/bad_blob_version', {
          :got => @blob['generator'][1],
          :need => CONFIG_MIGRATE_VERSION,
        })

        next render_view
      end
    else
      flash :error, t(:'initial_setup/upgrade/blob/errors/invalid_blob')
      next render_view
    end

    # And finally, check that the blob's 'config' key actually... has config
    # data in it. For simplicity's sake, we'll just check that the config has
    # `site-name`, `org-name`, and `base-url`.
    has_keys = %w[site-name org-name base-url].map{|k| @blob['config'].key?(k)}
    unless has_keys.all?
      flash :error, t(:'initial_setup/upgrade/blob/errors/invalid_blob')
      next render_view
    end

    # If we get here, the blob was valid! Let's store the blob data in the
    # database on the upgrade object.
    @this_upgrade.status = 'blob-okay'
    @this_upgrade.encrypt(:data, JSON.generate(@blob))
    @this_upgrade.save

    # And redirect on to the change step
    next redirect current_step_url
  end
end
