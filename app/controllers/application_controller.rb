require 'sinatra/base'
require 'haml'

class EARMMS::ApplicationController < Sinatra::Base
  helpers EARMMS::ApplicationHelpers

  set :default_encoding, "UTF-8"
  set :views, File.expand_path('../../views', __FILE__)
  set :haml, :format => :html5, :default_encoding => "UTF-8"
  enable :sessions

  before do
    # Set CSRF
    session[:csrf] ||= EARMMS::Crypto.generate_token.encode(Encoding::UTF_8)
    response.set_cookie('authenticity_token', {
      value: session[:csrf],
      expired: Time.now + (60 * 60 * 24 * 30), # 30 days
      path: '/',
      httponly: true,
    })

    # Check CSRF
    if !request.safe?
      unless request.params.key?('_csrf')
        next halt 403, "CSRF failed"
      end

      unless session[:csrf] == request.params['_csrf']
        next halt 403, "CSRF failed"
      end

      unless request.cookies.key?('authenticity_token')
        next halt 403, "CSRF failed"
      end

      unless session[:csrf] == request.cookies['authenticity_token']
        next halt 403, "CSRF failed"
      end
    end

    if logged_in?
      @global_announcements = EARMMS::Announcement.where(visible: true).all
    else
      @global_announcements = EARMMS::Announcement.where(visible: true, only_logged_in: false).all
    end

    # Check if we're in upgrade flow
    vup = EARMMS::VersionUpgradeModel.where(upgrade_id: EARMMS::VERSION_UPGRADE_ID).first
    if vup.nil? || vup.status != 'complete'
      # If there are users, we're logged in, and we have system access,
      # let us continue
      if logged_in? && has_role?('system:access')
        allowed = [
          current_prefix?('/initialsetup'),
          current_prefix?('/auth'),
          current_prefix?('/static'),
        ].any?

        next if allowed
        next redirect '/initialsetup'
      end

      # Otherwise, only allow access to auth/static
      unless current_prefix?('/auth') || current_prefix?('/static')
        next halt 503
      end
    end

    # Check if maintenance mode
    if is_maintenance?
      if current_prefix?('/auth') && !current?('/auth/signup')
        next
      elsif current_prefix?('/static/')
        next
      else
        if logged_in? && has_role?('system:use_during_maintenance')
          next
        else
          halt haml :'errors/maintenance', :layout => :layout_minimal
        end
      end
    end

    # Check if user has been disabled
    if logged_in?
      u = current_user
      if u.disabled_reason != nil
        unless current_prefix?('/auth') || current_prefix?("/static")
          halt redirect("/auth/disabled")
        end
      end
    end

    # Check if user is still in the extended signup flow
    if logged_in? && EARMMS.app_config['extended-signup-enabled']
      allowed = [
        current_prefix?('/auth'),
        current_prefix?('/static'),
        current_prefix?('/language'),
        settings.development? && current_prefix?('/system/debug'),
      ].any?

      unless allowed
        u = current_user
        s = u.signup_extended_status
        if s != nil && s != 'complete'
          halt redirect("/auth/signup/ex/#{s}")
        end
      end
    end

    # Check for mutexes, and check whether the current user has not verified their email
    if logged_in?
      u = current_user

      @global_mutex_announce = EARMMS::MutexModel.where(user: u.id).count.positive?
      @global_email_needs_to_verify = true unless u.email_verified
    end
  end

  not_found do
    haml :'errors/not_found', :layout => :layout_minimal
  end

  error 503 do
    haml :'errors/unavailable', :layout => :layout_minimal
  end
  
  error 400 do
    haml :'errors/bad_request', :layout => :layout_minimal
  end
end
