class EARMMS::MembershipAdminGroupEditController < EARMMS::MembershipAdminController
  helpers EARMMS::GroupHelpers

  before do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:group:access'
  end

  get '/:id' do |id|
    @group = EARMMS::Group[id.to_i]
    next halt 404 unless @group
    @group_h = @group.to_h
    @summary = @group.summary
    @members = get_group_members(@group)

    @title = t(:'membership/groups/view/title', :group => @group_h[:name])
    haml :'membership_admin/group/edit'
  end

  post '/:id' do |id|
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:group:edit'

    @group = EARMMS::Group[id.to_i]
    next halt 404 unless @group

    g_type = request.params['type']&.strip&.downcase
    unless %w[caucus working_group].include?(g_type)
      flash :error, t(:'membership/groups/view/edit/errors/invalid_type')
      next redirect request.path
    end

    g_name = request.params['name']&.strip
    if g_name.nil? || g_name.empty?
      flash :error, t(:'membership/groups/view/edit/errors/no_name')
      next redirect request.path
    end

    g_desc = request.params['description']&.strip
    g_discourse = request.params['discourse_link']&.strip
    g_discourse = nil if g_discourse&.empty?
    g_joinable = request.params['is_joinable']&.strip&.downcase == 'on'
    g_restricted = request.params['is_restricted']&.strip&.downcase == 'on'
    g_agreement = request.params['agreement']&.strip
    g_agreement = nil if g_agreement&.empty?
    
    # Store all the new details
    @group.type = g_type
    @group.encrypt(:name, g_name)
    @group.encrypt(:description, g_desc)
    @group.encrypt(:discourse_link, g_discourse)
    @group.is_joinable = g_joinable
    @group.restrict_to_member = g_restricted
    if g_agreement
      @group.encrypt(:user_agreement, g_agreement)
    else
      @group.user_agreement = nil
    end
    
    # And save.
    @group.save

    flash :success, t(:'membership/groups/view/edit/success')
    redirect request.path
  end
end
