class EARMMS::MembershipAdminUserSearchController < EARMMS::MembershipAdminController
  helpers EARMMS::MembershipAdminUserHelpers

  before do
    @available_sorts = %w[uid creation name]
    @branches = EARMMS::Branch.all.map do |b|
      [
        b.id,
        {
          :id => b.id,
          :name => b.decrypt(:name),
        }
      ]
    end.to_h
  end

  get '/expanded' do
    searchtype = request.params["searchtype"].strip.downcase
    query = request.params[searchtype]

    new_uri = Addressable::URI.parse('/admin/user/search')
    new_uri.query_values = {
      "searchtype" => searchtype,
      "query" => query,
    }

    redirect(new_uri.to_s)
  end

  get '/' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:user_change'
    next halt 400, "no searchtype provided" unless request.params.key?("searchtype")
    next halt 400, "no query provided" unless request.params.key?("query")

    @searchtype = request.params["searchtype"].strip.downcase
    @query = request.params["query"].strip

    @current_uri = current_uri_with_query(@searchtype, @query)
    @custom_fields = custom_profile_fields()

    @sort = "uid"
    @sort = request.params["sort"].strip.downcase if request.params.key?("sort")
    @sort_reverse = @sort.start_with?('~')

    @results = do_search(@searchtype, @query)
    @results = do_search_get_data(@results)
    @results = do_search_sort(@results, @sort.start_with?('~') ? @sort[1..-1] : @sort) if @results
    @results.reverse! if @sort_reverse && @results

    @title = t :'membership/user/search/title'
    haml :'membership_admin/user/list'
  end

  post '/' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:user_change'
    next halt 400, "no searchtype provided" unless request.params.key?("searchtype")
    next halt 400, "no query provided" unless request.params.key?("query")
    next halt 400, "no action provided" unless request.params.key?("action")

    @searchtype = request.params["searchtype"].strip.downcase
    @query = request.params["query"].strip

    @current_uri = current_uri_with_query(@searchtype, @query)
    @custom_fields = custom_profile_fields()

    @sort = "uid"
    @sort = request.params["sort"].strip.downcase if request.params.key?("sort")
    @sort_reverse = @sort.start_with?('~')

    @results = do_search(@searchtype, @query)
    @results = do_search_get_data(@results)
    @results = do_search_sort(@results, @sort.start_with?('~') ? @sort[1..-1] : @sort) if @results
    @results.reverse! if @sort_reverse && @results

    action = request.params["action"].strip.downcase
    if action == "as_text_email"
      content_type 'text/plain'
      next @results.map do |m|
        "#{ERB::Util.html_escape(m[:user_name])} <#{m[:user].email}>"
      end.compact.join(", ")

    elsif action == "export_search"
      type = request.params["export_type"]
      type = "text" if type.nil? || type == ""
      type = "text" unless %w[text csv].include? type
      type = type.to_sym

      EARMMS::Workers::SearchExport.queue({
        :searchtype => @searchtype,
        :query => @query,
        :exporttype => type,
        :requesting_user => current_user.id,
      })

      message = :'membership/user/search/actions/email_export/success'
      if type == :text
        message = :'membership/user/search/actions/email_export_text/success'
      elsif type == :csv
        message = :'membership/user/search/actions/email_export_csv/success'
      end

      flash :success, t(message)

    elsif action == "perform_mass_action"
      mass_action = request.params["mass_action"]&.strip&.downcase
      if mass_action.nil? || mass_action.empty?
        next halt 400, "invalid mass action"
      end

      EARMMS::Workers::MassAction.queue({
        :searchtype => @searchtype,
        :query => @query,
        :action => mass_action,
      })

      flash :success, t(:'membership/user/search/massaction/success')

    else
      next halt 400, "invalid action"
    end

    @title = t :'membership/user/search/title'
    haml :'membership_admin/user/list'
  end
end
