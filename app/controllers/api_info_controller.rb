class EARMMS::ApiInfoController < EARMMS::ApiController
  get '/' do
    content_type 'application/json'

    data = {}
    data[:'success'] = true
    data[:'site-name'] = EARMMS.app_config['site-name']
    data[:'org-name'] = EARMMS.app_config['org-name']
    data[:'base-url'] = EARMMS.app_config['base-url']

    # Return early if we don't have a token
    next JSON.generate(data) unless @token = api_token?()

    data[:'authenticated'] = true

    # Version info, if enabled in the config
    if EARMMS.app_config['display-version']
      data[:'version'] = "EARMMS #{EARMMS.version}"
    else
      data[:'version'] = "EARMMS"
    end

    JSON.generate(data)
  end
end
