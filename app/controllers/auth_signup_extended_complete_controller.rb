class EARMMS::AuthSignupExtendedCompleteController < EARMMS::ApplicationController
  before do
    unless logged_in?
      flash(:error, t(:'auth/login/must_log_in'))
      session[:after_login] = request.path
      next redirect('/auth')
    end
  end

  get '/' do
    next redirect '/dashboard'
  end
end
