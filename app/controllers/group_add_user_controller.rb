class EARMMS::GroupAddUserController < EARMMS::ApplicationController
  helpers EARMMS::GroupHelpers

  before do
    @auth_layout_classes = %w[auth-layout-medium auth-layout-centered]

    next halt 404 unless logged_in?
    next halt 404 unless EARMMS.app_config["group-allow-admin-to-add-by-uid"]

    @user = current_user
    @profile = EARMMS::Profile.for_user(@user)
  end
  
  get '/:id' do |cid|
    next halt 404 unless cid
    @group = EARMMS::Group[cid]
    next halt 404 unless @group
    @group_h = @group.to_h
    @members = get_group_members(@group)
    next halt 404 unless @members.key?(@user.id)
    next halt 404 unless @members[@user.id][:admin]

    @title = t(:'groupadmin/adduser/title', :group => @group_h[:name])
    haml :'group_admin/add_user/index'
  end
  
  post '/:id' do |cid|
    @auth_header_hide_home = @auth_header_hide_links = true

    next halt 404 unless cid
    @group = EARMMS::Group[cid]
    next halt 404 unless @group
    @group_h = @group.to_h
    @members = get_group_members(@group)
    next halt 404 unless @members.key?(@user.id)
    next halt 404 unless @members[@user.id][:admin]

    @make_admin = request.params['make_admin']&.strip&.downcase == 'on'
    @user_adding = EARMMS::User[request.params['user_id'].to_i]
    if @user_adding.nil?
      flash :error, t(:'groupadmin/adduser/errors/invalid_uid')
      next redirect request.path
    end
    @user_adding_profile = EARMMS::Profile.for_user(@user_adding)
    @user_adding_name = @user_adding_profile.decrypt(:full_name)
    
    if @members.key?(@user_adding.id)
      flash :error, t(:'groupadmin/adduser/errors/already_exists')
      next redirect request.path
    end
    
    # Check group membership status restriction
    if @group_h[:member_only] && @user_adding_profile.decrypt(:membership_status)&.strip&.downcase != 'member'
      flash :error, t(:'groupadmin/adduser/errors/restricted_non_member')
      next redirect request.path
    end

    if request.params['confirm']&.strip&.downcase == 'on'
      # Add user to group
      gm = EARMMS::GroupMember.new(group: @group.id)
      gm.save
      gm.encrypt(:user, @user_adding.id.to_s)
      if @make_admin
        gm.encrypt(:roles, 'admin,email') 
      end
      gm.save

      # Create filters
      EARMMS::GroupMemberFilter.create_filters_for(gm)

      # Success!
      flash :success, t(:'groupadmin/adduser/success', :user => @user_adding_name)
      next redirect "/group/edit/#{@group.id}"
    end

    @title = t(:'groupadmin/adduser/confirm/title')
    haml :'auth/layout', :layout => :layout_minimal do
      haml :'group_admin/add_user/confirm', :layout => false
    end
  end
end
