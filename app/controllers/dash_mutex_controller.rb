class EARMMS::DashMutexController < EARMMS::ApplicationController
  before do
    redirect '/' unless logged_in?

    @user = current_user
    @mutexes = EARMMS::MutexModel.where(user: @user.id).map do |m|
      [
        m.type,
        {
          obj: m,
          id: m.id,
          creation: m.creation,
          type: EARMMS::MutexModel.type_data[m.type],
        }
      ]
    end.to_h
  end

  get '/' do
    @auth_layout_classes = %w[auth-layout-medium auth-layout-centered]
    @title = t(:'dash/mutex/title')

    haml :'auth/layout', :layout => :layout_minimal do
      haml :'dash/mutex', :layout => false
    end
  end

  post '/' do
    @type = request.params['mutex']&.strip
    @mutex = @mutexes[@type]
    next halt 400 unless @mutex

    @mutex[:obj].release!
    flash :success, t(:'dash/mutex/unlock/success', name: t(@mutex[:type][:friendly_tl]))

    next redirect '/-/mutex'
  end
end