class EARMMS::MembershipAdminController < EARMMS::ApplicationController
  before do
    @membership_alerts = EARMMS::Alert.where(section: "membership", actioned: false).count
  end
end
