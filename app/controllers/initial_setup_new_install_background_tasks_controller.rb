class EARMMS::InitialSetupNewInstallBackgroundTasksController < EARMMS::InitialSetupController
  helpers do
    def available_background_tasks
      [
        {
          :name => 'EmailQueueSender',
          :form_id => 'task__email_queue',
          :friendly => t(:'initial_setup/newinstall/background_tasks/tasks/email'),
          :enabled_default => true,
        },
        {
          :name => 'MembershipStatsGeneration',
          :form_id => 'task__stats',
          :friendly => t(:'initial_setup/newinstall/background_tasks/tasks/stats'),
          :enabled_default => true,
        },
      ]
    end
  end

  get '/' do
    @data = JSON.parse(@this_upgrade.decrypt(:data))
    @has_meeting_reminders = @data['config-entries']['worker-meeting-reminder-secs'].positive?

    haml :'initial_setup/layout', :layout => :layout_minimal do
      haml :'initial_setup/newinstall/layout', :layout => false do
        haml :'initial_setup/newinstall/background_tasks', :layout => false
      end
    end
  end

  post '/' do
    @data = JSON.parse(@this_upgrade.decrypt(:data))
    @data['job-queue'] ||= []

    available_background_tasks().each do |t|
      if request.params[t[:form_id]].to_i.positive?
        @data['job-queue'] << {
          'name' => t[:name],
          'at' => 'now'
        }
      end
    end

    @this_upgrade.encrypt(:data, JSON.generate(@data))
    @this_upgrade.status = 'background-tasks-ok'
    @this_upgrade.save

    next redirect '/initialsetup/newinstall/two-factor'
  end
end
