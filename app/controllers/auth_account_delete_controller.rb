class EARMMS::AuthAccountDeleteController < EARMMS::ApplicationController
  before do
    @auth_layout_classes = %w[auth-layout-medium auth-layout-centered]
    @title = t(:'auth/account_delete/title')
  end

  get '/' do
    next redirect '/auth/account-delete/confirm' if logged_in?

    haml :'auth/layout', :layout => :layout_minimal do
      haml :'auth/account_delete/index', :layout => false
    end
  end

  post '/' do
    next redirect '/auth/account-delete/confirm' if logged_in?

    @email = request.params['email']&.strip&.downcase
    @user = EARMMS::User.where(email: @email).first
    unless @user
      next haml :'auth/layout', :layout => :layout_minimal do
        haml :'auth/account_delete/email_sent'
      end
    end

    # if user has a password set, prompt to log in
    unless @user.password_hash.nil?
      flash :warning, t(:'auth/account_delete/start/must_login')
      session[:after_login] = request.path
      next redirect '/auth'
    end

    # otherwise, send a confirmation email
    @user.send_delete_confirm_email!

    haml :'auth/layout', :layout => :layout_minimal do
      haml :'auth/account_delete/email_sent'
    end
  end

  get '/confirm' do
    @auth_header_hide_home = @auth_header_hide_links = true

    if logged_in?
      @user = current_user
    else
      token = request.params['token']&.strip&.downcase
      @token = EARMMS::Token.where(token: token, use: 'delete_confirm').first
      @user = EARMMS::User[@token&.user]
      next halt 404 unless @token&.is_valid?
    end

    next halt 404 unless @user

    haml :'auth/layout', :layout => :layout_minimal do
      haml :'auth/account_delete/confirm'
    end
  end

  post '/confirm' do
    if logged_in?
      @user = current_user
    else
      token = request.params['token']&.strip&.downcase
      @token = EARMMS::Token.where(token: token, use: 'delete_confirm').first
      @user = EARMMS::User[@token&.user]
      next halt 404 unless @token&.is_valid?
    end

    next halt 404 unless @user
    @profile = EARMMS::Profile.for_user(@user)

    unless @user.password_hash.nil?
      password = request.params['password']&.strip
      unless @user.verify_password(password)
        flash :error, t(:'auth/account_delete/confirm/errors/invalid_password')
        next redirect request.path
      end
    end

    # Generate account deletion alert
    gen_alert :membership, :user_deletion, t(:'auth/account_delete/alert', {
      :user => @profile.decrypt(:full_name),
      :uid => @user.id,
      :force_language => true,
    })

    # Delete account
    @token&.invalidate!
    @user.delete_account!

    # say goodbye
    flash :success, t(:'auth/account_delete/confirm/success')
    redirect '/'
  end
end
