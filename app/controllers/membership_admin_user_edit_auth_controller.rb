class EARMMS::MembershipAdminUserEditAuthController < EARMMS::MembershipAdminController
  helpers EARMMS::MembershipAdminUserEditHelpers

  get '/:uid' do |uid|
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:user:auth'

    @user = EARMMS::User[uid.to_i]
    next halt 404 unless @user
    @profile = EARMMS::Profile.for_user(@user)
    next halt 404 unless @profile

    @user_name = @profile.decrypt(:full_name)
    @disable = @user.disabled_reason ? @user.decrypt(:disabled_reason) : ''
    @title = t(:'membership/user/auth/title', :user => @user_name)

    haml :'membership_admin/user/auth'
  end

  post '/:uid' do |uid|
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:user:auth'
    next halt 400, "no action" unless request.params.key?("action")

    @user = EARMMS::User[uid.to_i]
    next halt 404 unless @user
    @profile = EARMMS::Profile.for_user(@user)
    next halt 404 unless @profile

    @user_name = @profile.decrypt(:full_name)

    @regen = false
    case request.params["action"].strip.downcase
    when "disable_account"
      reason = request.params['reason']&.strip
      reason = nil if reason&.empty?
      if reason
        @user.encrypt(:disabled_reason, reason)
        flash :success, t(:'membership/user/auth/disable/success/disable')
      else
        @user.disabled_reason = nil
        flash :success, t(:'membership/user/auth/disable/success/enable')
      end

      gen_alert :membership, :admin_force_edit_user, t(:'membership/user/auth/disable/alert', :name => @user_name, :uid => @user.id, :reason => reason, :force_language => true)
      @user.save

    when "delete_account"
      if request.params['confirm']&.strip != 'DELETEME'
        flash :error, t(:'membership/user/auth/delete/errors/invalid_confirm')
        next redirect request.path
      end

      userid = @user.id
      @user.delete_account!

      gen_alert :membership, :user_deletion, t(:'membership/user/auth/delete/alert', :name => @user_name, :uid => userid, :force_language => true)
      flash :success, t(:'membership/user/auth/delete/success')
      next redirect '/admin/user'

    when "pwreset"
      @user.password_hash = nil
      @user.save
      @user.send_password_reset!

      gen_alert :membership, :admin_force_edit_user, t(:'membership/user/auth/pwreset/alert', :name => @user_name, :uid => @user.id, :force_language => true)
      flash :success, t(:'membership/user/auth/pwreset/success')

      # do this after the success message and alert generation so if the current
      # user decides to use this to force a reset on themself, the alert gen
      # works properly (as `current_user` will check the session token, which
      # we're invalidating)
      @user.invalidate_tokens(nil)

    else
      flash :error, t(:'membership/user/auth/errors/invalid_action')
      next redirect request.path
    end

    if @regen
      EARMMS::ProfileFilter.clear_filters_for(@profile)
      EARMMS::ProfileFilter.create_filters_for(@profile)
    end

    next redirect request.path
  end
end
