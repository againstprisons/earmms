require 'email_validator'

class EARMMS::UserSettingsAccountController < EARMMS::ApplicationController
  helpers EARMMS::UserSettingsAccountHelpers

  before do
    unless logged_in?
      flash(:error, t(:'auth/login/must_log_in'))
      session[:after_login] = request.path
      next redirect '/auth'
    end

    @user = current_user
    @profile = EARMMS::Profile.for_user(@user)
  end

  get '/' do
    @title = t(:'userprofile/account/title')
    haml :'user_settings/account'
  end

  post '/' do
    case request.params['action']&.strip&.downcase
    when "change-email"
      email = request.params["email"]&.strip&.downcase
      password = request.params["password"]
      if email.nil? || email.empty? || password.nil? || password.empty?
        flash :error, t(:'userprofile/account/errors/required_not_provided')
        next redirect request.path
      end

      # verify password
      unless @user.verify_password(password)
        flash :error, t(:'userprofile/account/errors/invalid_password')
        next redirect request.path
      end

      unless EmailValidator.valid?(email)
        flash :error, t(:'userprofile/account/change_email/errors/invalid_email')
        next redirect request.path
      end

      # check there's no users in the db with that email
      if EARMMS::User.where(email: email).count.positive?
        flash :error, t(:'userprofile/account/change_email/errors/already_exists')
        next redirect request.path
      end

      @user.email = email
      @user.save

      flash :success, t(:'userprofile/account/change_email/success')

    when "change-password"
      unless @user.verify_password(request.params["password"] || '')
        flash :error, t(:'userprofile/account/errors/invalid_password')
        next redirect request.path
      end

      newpass = request.params["newpass"]
      confirm = request.params["newpass-confirm"]
      if newpass.nil? || newpass.empty? || confirm.nil? || confirm.empty?
        flash :error, t(:'userprofile/account/errors/required_not_provided')
        next redirect request.path
      end

      if newpass != confirm
        flash :error, t(:'userprofile/account/change_password/errors/no_match')
        next redirect request.path
      end

      @user.password = request.params["newpass"]
      @user.invalidate_tokens(session[:token])
      @user.save

      flash :success, t(:'userprofile/account/change_password/success')

    else
      flash :error, t(:'userprofile/account/errors/invalid_action')
    end

    redirect request.path
  end
end
