class EARMMS::GroupEditController < EARMMS::ApplicationController
  helpers EARMMS::GroupHelpers

  before do
    next halt 404 unless logged_in?
    @user = current_user
    @profile = EARMMS::Profile.for_user(@user)
  end
  
  get '/:id' do |cid|
    next halt 404 unless cid
    @group = EARMMS::Group[cid]
    next halt 404 unless @group
    @group_h = @group.to_h
    @members = get_group_members(@group)
    next halt 404 unless @members.key?(@user.id)
    next halt 404 unless @members[@user.id][:admin]

    @title = t(:'groupadmin/edit/title', :group => @group_h[:name])
    haml :'group_admin/edit'
  end

  post '/:id' do |cid|
    next halt 404 unless cid
    @group = EARMMS::Group[cid]
    next halt 404 unless @group
    @group_h = @group.to_h
    @members = get_group_members(@group)
    next halt 404 unless @members.key?(@user.id)
    next halt 404 unless @members[@user.id][:admin]

    case request.params['action']&.strip&.downcase
    when 'remove'
      user = EARMMS::User[request.params['uid'].to_i]
      next halt 400 unless user
      next halt 400 unless @members.keys.include?(user.id)
      if user.id == @user.id
        flash :error, t(:'groupadmin/edit/members/remove/errors/self')
        next redirect request.path
      end
      
      if @members[user.id][:admin]
        flash :error, t(:'groupadmin/edit/members/remove/errors/is_admin')
        next redirect request.path
      end
      
      gm = @members[user.id][:group_member]
      EARMMS::GroupMemberFilter.clear_filters_for(gm)
      gm.delete
      
      flash :success, t(:'groupadmin/edit/members/remove/success')

    when 'toggle-admin'
      user = EARMMS::User[request.params['uid'].to_i]
      next halt 400 unless user
      next halt 400 unless @members.keys.include?(user.id)
      if user.id == @user.id
        flash :error, t(:'groupadmin/edit/members/demote/errors/self')
        next redirect request.path
      end
      
      gm = @members[user.id][:group_member]
      if @members[user.id][:admin]
        gm.encrypt(:roles, '')
        flash :success, t(:'groupadmin/edit/members/demote/success')
      else
        gm.encrypt(:roles, 'admin,email')
        flash :success, t(:'groupadmin/edit/members/promote/success')
      end
      gm.save

      EARMMS::GroupMemberFilter.clear_filters_for(gm)
      EARMMS::GroupMemberFilter.create_filters_for(gm)

    else
      flash :error, t(:'groupadmin/edit/errors/invalid_action')
    end
    
    redirect request.path
  end
end
