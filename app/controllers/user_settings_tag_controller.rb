class EARMMS::UserSettingsTagController < EARMMS::ApplicationController
  helpers EARMMS::UserTagHelpers

  get '/' do
    @auth_layout_classes = %w[auth-layout-medium auth-layout-centered]
    @tl_namespace = "userprofile/usertag"

    # Discord marker: If non-zero, Discord display mode is enabled.
    #
    # If greater than or equal to 8, an error is shown if the user requesting
    # their tag is not an organisation member.
    @discord_marker = request.params["d"]&.strip.to_i
    if @discord_marker > 0
      @tl_namespace = "userprofile/usertag/discord"
      @only_member = @discord_marker >= 8
    end

    unless logged_in?
      flash(:error, "You must be logged in to access this page.")
      session[:after_login] = "/user/tag?d=#{@discord_marker}"
      next redirect '/auth'
    end

    @user = current_user
    @user_is_member = %w[member].include?(EARMMS::Profile.for_user(@user)&.decrypt(:membership_status))
    @usertag = user_to_tag(@user)
    @usertaguser = tag_to_user(@usertag) if settings.development?

    @title = t("#{@tl_namespace}/title".to_sym)
    haml :'auth/layout', :layout => :layout_minimal do
      haml :'user_settings/tag', :layout => false
    end
  end
end
